my_runner_2017 - The Pitfall! Clone

------------------------------------------------------------------------

DESCRIPTION:

This project is a side-scrolling endless game.

It's like a clone of the famous 'Pitfall! The mayan adventure',
a side-scrolling plateformer, released in 1982 on ATARI 2600
by ActiVison.

Unlike the original game, mine is an infinite runner.

All the graphics are new but the theme and the ambiance are the same.
------------------------------------------------------------------------

INSTALLATION (Fedora ONLY):

You'll need to install the CSFML library with the '.sh' script in 'bonus'
folder.

After that, do (in the root of the folder 'my_runner_2017'):
	$make

Then it's OK
------------------------------------------------------------------------

RUNNING THE GAME:

After the 'INTALLATION' step, open a terminal in the root of the folder
'my_runner_2017', then do:
	$./my_runner <$path to a correct map>

To get a correct map refer to the 'MAP DEFINITION' section.
------------------------------------------------------------------------

CONTROLS:

The charcater will run automatically.

You can jump by pressing the 'SPACE' key.
------------------------------------------------------------------------

MAP DEFINITION:

First here is template of a correct map
('#' represent the boder of your terminal or text editor,
don't take care about):

	TEMPLATE:

. ########################################################################
9 #                                                    8                 #
8 #                                       4                              #
7 #                  2                                             3     #
6 #                                                                      #
5 #                                 9                                    #
4 #       6           0                             7                    #
3 #                                                                      #
2 #       X                   1                                          #
1 #                                                     5                #
0 #                     X              X      6                          #
. ########################################################################
   0123456789.........

	AXIS:

            y (row, '#' =~ 23 pixels)
            ^
            |
            |
            0------> x (time in seconds)

	EXPLAINATION:

The x axis had a variable size from 0 to infinite.
The y axis had a fixed size '10' rows.

The '0' row is where the player walk
The '2' row is where the player's head is (when walking).

All the chars in the map represents the probability of the presence of an
obstacle, so:

			Numbers or ' ' or 'X' => spears
' ' or '0' = 0%
'1' = 10%
'2' = 20%
'3' = 30%
 .
 .
 .
 .
'9' = 90%
'X' {Force present} = 100%

	STORAGE:

The map must be store in a text file with or without extention |<map.txt>
							       |<map>

	SPECIFICATIONS:

The map don't need to be rectangular (unlike the exemple),
but the x coordinate matter !

!!! IF ONE OF THE PREVIOUS RULES IS VIOLATED THE GAME WONT RUN !!!
------------------------------------------------------------------------


MY_RUNNER_2017

BY Pierre MARTY

! EPITECH PROJECT !
