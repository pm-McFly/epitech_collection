/*
** EPITECH PROJECT, 2017
** runner.h
** File description:
** Desc
*/

#ifndef RUNNER_H_
#define RUNNER_H_	1

/* Include */
#include <SFML/Audio.h>
#include <SFML/Graphics.h>
#include <SFML/System.h>
#include <SFML/Window.h>

/* objects */
typedef struct v_entity_s {
	sfTexture *t;
	sfSprite *s;
	sfVector2f p;
	sfVector2f o;
	sfIntRect r;
} v_entity_t;

typedef struct paral_s {
	v_entity_t *s1;
	v_entity_t *s2;
	sfVector2f speed;
} layer_t;

typedef struct options_s {
	sfRenderWindow *w;
	sfVideoMode mode;
	sfMusic *bgs;
	float volume;
	int lives;
} options_t;

typedef struct bg_s {
	layer_t *l1;
	layer_t *l2;
	layer_t *l3;
	layer_t *l4;
	layer_t *l5;
} bg_t;

typedef struct moving_s {
	v_entity_t *visual;
	sfVector2f speed;
} moving_t;

typedef struct animated_s {
	v_entity_t *visual;
	sfVector2f speed;
	int n_steps;
	int set_c;
	sfMusic *sound;
} animated_t;

typedef struct character_s {
	animated_t *looklike;
	sfBool jumping;
	sfBool death;
	int score;
} character_t;

/* Prototypes */
options_t *game_creator(void);
void game_destructor(options_t *opts);
sfRenderWindow *window_creator(sfVideoMode);
sfMusic *music_creator(char *, float, sfBool);
void e_control_center(sfRenderWindow *);
void game_loop(options_t *, bg_t *, moving_t **);
bg_t *background_creator(void);
void background_destructor(bg_t *);
layer_t *layer_creator(char *, float, float);
void layer_destructor(layer_t *);
v_entity_t *v_entity_creator(char *, int, int);
v_entity_t *v_entity_copy(v_entity_t *, int, int);
void v_entity_destructor(v_entity_t *);
void background_display(sfRenderWindow *, bg_t *);
void parallax_center(sfRenderWindow *, layer_t *);
void splash_screen(sfRenderWindow *);
void menu(sfRenderWindow *, bg_t *);
int m_control_center(sfRenderWindow *, v_entity_t *, sfClock *);
animated_t *animated_creator(char *, sfVector2f, int, char *);
character_t *character_creator(char *, float);
void character_destructor(character_t *);
void animated_destructor(animated_t *);
void character_display(sfRenderWindow *, character_t *, sfClock *);
void animated_run(animated_t *);
void animated_jump(character_t *);
char *score2str(int);
char *file2str(char *);
moving_t **enemy_generator(int);
moving_t *enemy_creator(int, int);
void enemy_pool_display(moving_t **, sfRenderWindow *);
float my_rand(float, float);
void enemy_pool_destructor(moving_t **);

#endif /* !RUNNER_H_ */
