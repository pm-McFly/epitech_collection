/*
** EPIETCH PROJECT, 2017
** menu.c
** File description:
** display menu
*/

#include "runner.h"

void menu(sfRenderWindow *w, bg_t *bg)
{
	v_entity_t *menu = v_entity_creator("./assets/img/panel/splash.png", 1920, 1080);

	while (sfRenderWindow_isOpen(w)) {
		e_control_center(w);
		background_display(w, bg);
		sfRenderWindow_drawSprite(w, menu->s, NULL);
	}
	v_entity_destructor(menu);
}

	

/*
	v_entity_t *menu = v_entity_creator("./assets/img/panel/splash.png", 1067, 6000);
	v_entity_t *selector = v_entity_creator("./assets/img/panel/frame.png", 242, 129);
	sfClock *move = sfClock_create();
	selector->p = (sfVector2f){10, 24};
	sfRenderWindow_drawSprite(opts->w, menu->s, NULL);
	m_control_center(opts->w, selector, move);
	v_entity_destructor(selector);
	v_entity_destructor(menu);
	sfClock_destroy(move);

*/
