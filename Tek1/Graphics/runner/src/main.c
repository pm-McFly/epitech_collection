/*
** EPITECH PROJECT, 2017
** main.c
** File description:
** launcher
*/

#include "runner.h"
#include <stdio.h>

int main (void)
{
	options_t *opts;

	if ((opts = game_creator()) == NULL)
		return (0);
	splash_screen(opts->w);
	if (sfRenderWindow_isOpen(opts->w) == sfTrue)
		game_loop(opts, background_creator(), enemy_generator(4));
	game_destructor(opts);
	return (0);
}
