/*
** EPITECH PROJECT, 2017
** splash.c
** File description:
** display logo of the game
*/

#include "runner.h"

void splash_screen(sfRenderWindow *w)
{
	sfClock *limit = sfClock_create();
	sfMusic *intro_m = music_creator("./assets/audio/intro.ogg",
	100, sfFalse);
	v_entity_t *intro_ve = v_entity_creator("./assets/img/intro.png", 1920, 1080);

	sfMusic_play(intro_m);
	while (sfRenderWindow_isOpen(w) &&
	sfClock_getElapsedTime(limit).microseconds <= 4500000) {
		e_control_center(w);
		sfRenderWindow_clear(w, sfBlack);
		sfRenderWindow_drawSprite(w, intro_ve->s, NULL);
		sfRenderWindow_display(w);
	}
	v_entity_destructor(intro_ve);
	sfMusic_stop(intro_m);
	sfMusic_destroy(intro_m);
}
