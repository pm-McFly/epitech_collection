/*
** EPITECH PROJECT, 2017
** window.c
** File description:
** global window manager
*/

#include "runner.h"

sfRenderWindow *window_creator(sfVideoMode mode)
{
	sfRenderWindow *w;

	w = sfRenderWindow_create(mode, "my_runner", sfDefaultStyle, NULL);
	if (!w)
		return (NULL);
	sfRenderWindow_setFramerateLimit(w, 30);
	sfRenderWindow_setVerticalSyncEnabled(w, sfTrue);
	return (w);
}
