/*
** EPITECH PROJECT, 2017
** my_rand.c
** File description:
** get rand num into limits
*/

#include <stdlib.h>

float my_rand(float min, float max)
{
 	int randnum = rand();
	float out = randnum % (int)(max - min) + (int)min;
	return (out);
}
