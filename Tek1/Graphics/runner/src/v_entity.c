/*
** EPITECH PROJECT, 2017
** v_entity.c
** File description:
** general v_entity manager
*/

#include "runner.h"
#include <stdlib.h>

v_entity_t *v_entity_creator(char *path, int width, int height)
{
	v_entity_t *entity = malloc(sizeof(v_entity_t));

	if (!entity)
		return (NULL);
	entity->t = sfTexture_createFromFile(path, NULL);
	if (!entity) {
		free(entity);
		return (NULL);
	}
	entity->p = (sfVector2f){0, 0};
	entity->o = (sfVector2f){0, 0};
	entity->r = (sfIntRect){0, 0, width, height};
	entity->s = sfSprite_create();
	sfSprite_setTexture(entity->s, entity->t, sfTrue);
	sfSprite_setTextureRect(entity->s, entity->r);
	sfSprite_setPosition(entity->s, entity->o);
	return (entity);
}

v_entity_t *v_entity_copy(v_entity_t *src, int x_offset, int y_offset)
{
	v_entity_t *entity_c = malloc(sizeof(v_entity_t));

	if (!entity_c)
		return (NULL);
	entity_c->t = sfTexture_copy(src->t);
	entity_c->s = sfSprite_copy(src->s);
	entity_c->o.x = src->o.x + x_offset;
	entity_c->o.y = src->o.y + y_offset;
	entity_c->p.x = src->p.x + x_offset;
	entity_c->p.y = src->p.y + y_offset;
	sfSprite_setTexture(entity_c->s, entity_c->t, sfTrue);
	sfSprite_setTextureRect(entity_c->s, src->r);
	sfSprite_setPosition(entity_c->s, entity_c->o);
	return (entity_c);
}

void v_entity_destructor(v_entity_t *entity)
{
	sfTexture_destroy(entity->t);
	sfSprite_destroy(entity->s);
	free(entity);
	return;
}
