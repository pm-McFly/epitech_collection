/*
** EPITECH PROJECT, 2017
** background.c
** File description:
** general manger of the background
*/

#include "runner.h"
#include <stdlib.h>

bg_t *background_creator(void)
{
	bg_t *parl_x = malloc(sizeof(bg_t));

	if (!(parl_x->l1 = layer_creator("./assets/img/bg/plx-1.png", 0, 0))) {
		free(parl_x);
		return (NULL);
	}
	if (!(parl_x->l2 = layer_creator("./assets/img/bg/plx-2.png", -1.74, 0))) {
		layer_destructor(parl_x->l1);
		free(parl_x);
		return (NULL);
	}
	if (!(parl_x->l3 = layer_creator("./assets/img/bg/plx-3.png", -2, 0))) {
		layer_destructor(parl_x->l1);
		layer_destructor(parl_x->l2);
		free(parl_x);
		return (NULL);
	}
	if (!(parl_x->l4 = layer_creator("./assets/img/bg/plx-4.png", -2.3, 0))) {
		layer_destructor(parl_x->l1);
		layer_destructor(parl_x->l2);
		layer_destructor(parl_x->l3);
		free(parl_x);
		return (NULL);
	}
	if (!(parl_x->l5 = layer_creator("./assets/img/bg/plx-5.png", -2.8, 0))) {
		layer_destructor(parl_x->l1);
		layer_destructor(parl_x->l2);
		layer_destructor(parl_x->l3);
		layer_destructor(parl_x->l4);
		free(parl_x);
		return (NULL);
	}
	return (parl_x);
}

void background_destructor(bg_t *plx)
{
	layer_destructor(plx->l1);
	layer_destructor(plx->l2);
	layer_destructor(plx->l3);
	layer_destructor(plx->l4);
	layer_destructor(plx->l5);
	free(plx);
}

void background_display(sfRenderWindow *w, bg_t *plx)
{
	parallax_center(w, plx->l1);
	parallax_center(w, plx->l2);
	parallax_center(w, plx->l3);
	parallax_center(w, plx->l4);
	parallax_center(w, plx->l5);
}
