/*
** EPITECH PROJECT, 2017
** score.c
** File description:
** all score functions
*/

#include "runner.h"
#include <stdlib.h>

char *score2str(int i)
{
	char const digit[10] = "0123456789";
	char *p = malloc(sizeof(char) * 10);
	int shifter = i;

	do{
		++p;
		shifter = shifter / 10;
	} while(shifter);
	*p = '\0';
	do{
		*--p = digit[i % 10];
		i = i / 10;
	} while(i);
	return p;
}
