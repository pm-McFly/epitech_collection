/*
** EPITECH PROJECT, 2017
** layer.c
** File description:
** general layer manager
*/

#include "runner.h"
#include <stdlib.h>

layer_t *layer_creator(char *path, float x_spd, float y_spd)
{
	layer_t *layer = malloc(sizeof(layer_t));

	if (!(layer->s1 = v_entity_creator(path, 1067, 600))) {
		free(layer);
		return (NULL);
	}
	layer->s2 = v_entity_copy(layer->s1, 1067, 0);
	layer->speed = (sfVector2f){x_spd, y_spd};
	return (layer);
}

void layer_destructor(layer_t *lyr)
{
	v_entity_destructor(lyr->s1);
	v_entity_destructor(lyr->s2);
	free(lyr);
}
