/*
** EPITECH PROJECT, 2017
** file_tools.c
** File description:
** Desc
*/

#include <stdlib.h>
#include <sys/types.h>
#include <sys/stat.h>
#include <fcntl.h>
#include <unistd.h>

static int open_file(char *file_path, int flag)
{
	int fd;

	if ((fd = open(file_path, flag)) == -1) {
		return (-1);
	}
	else
		return (fd);
}

static int get_file_size(char *path)
{
	int size;
	int tmp;
	int fd;
	char *buff;

	if ((buff = malloc(sizeof(char) * 10)) == NULL)  {
		return (-1);
	}
	if ((fd = open(path, O_RDONLY)) == -1)
		return (-1);
	size = 10;
	tmp = read(fd, buff, 10);
	while (tmp == 10) {
		tmp = read(fd, buff, 10);
		size += 10;
	}
	close(fd);
	free(buff);
	return (size);
}

char *file2str(char *path)
{
	int fd;
	char *buff;
	int size = get_file_size(path);

	if ((buff = malloc(sizeof(char) * (size + 1))) == NULL) {
		return (NULL);
	}
	if ((fd = open_file(path, O_RDONLY)) == -1)
		return (NULL);
	size = read(fd, buff, size);
	buff[size] = '\0';
	close(fd);
	return (buff);
}
