/*
** EPITECH PROJECT, 2017
** music.c
** File description:
** global music manager
*/

#include "runner.h"

sfMusic *music_creator(char *path, float volume, sfBool loop)
{
	sfMusic *m;
	m = sfMusic_createFromFile(path);
	if (!m)
		return (NULL);
	sfMusic_setVolume(m, volume);
	sfMusic_setLoop(m, loop);
	return (m);
}
