/*
** EPIETCH PROJECT, 2017
** parallax.c
** File description:
** general parallax manager
*/

#include "runner.h"

void parallax_center(sfRenderWindow *w, layer_t *l)
{
	l->s1->p = sfSprite_getPosition(l->s1->s);
	l->s2->p = sfSprite_getPosition(l->s2->s);
	if (l->s1->p.x <= -1067) {
		l->s1->p.x = 1067;
		sfSprite_setPosition(l->s1->s, l->s1->p);
	}
	if (l->s2->p.x <= -1067) {
		l->s2->p.x = 1067;
		sfSprite_setPosition(l->s2->s, l->s2->p);
	}
	sfRenderWindow_drawSprite(w, l->s1->s, NULL);
	sfRenderWindow_drawSprite(w, l->s2->s, NULL);
	sfSprite_move(l->s1->s, l->speed);
	sfSprite_move(l->s2->s, l->speed);
}
