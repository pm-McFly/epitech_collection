/*
** EPITECH PROJECT, 2017
** enemy.c
** File description:
** general enemy manager
*/

#include "runner.h"
#include <stdlib.h>

moving_t **enemy_generator(int n)
{
	int i = 0;
	moving_t **map_obj;

	map_obj = malloc(sizeof(moving_t *) * (n + 1));
	map_obj[n] = NULL;
	while (i != n) {
		map_obj[i] = enemy_creator(((1167 * (i + 1)) / 3), (int)my_rand(218, 470));
		++i;
	}
	map_obj[i] = NULL;
	return (map_obj);
}

void enemy_pool_display(moving_t **pool, sfRenderWindow *w)
{
	int i = 0;

	while (pool[i] != NULL) {
		if (sfSprite_getPosition(pool[i]->visual->s).x <= -23)
			sfSprite_setPosition(pool[i]->visual->s, (sfVector2f){(1167), (int)my_rand(218, 470)});
		sfRenderWindow_drawSprite(w, pool[i]->visual->s, NULL);
		sfSprite_move(pool[i]->visual->s, pool[i]->speed);
		++i;
	}
}

void enemy_pool_destructor(moving_t **pool)
{
	int i = 0;

	while (pool[i] != NULL) {
		v_entity_destructor(pool[i]->visual);
		free(pool[i]);
		++i;
	}
	free(pool);
}


moving_t *enemy_creator(int column, int row)
{
	moving_t *obj = malloc(sizeof(moving_t));
	char *s;
	int m;
	obj->speed = (sfVector2f){(my_rand(8, 15) * -1), 0};

	m = (int)my_rand(1, 4);
	if (m == 1)
		s = "./assets/img/obstacles/spear1.png";
	else if (m == 2)
		s = "./assets/img/obstacles/spear2.png";
	else
		s = "./assets/img/obstacles/spear3.png";
	obj->visual = v_entity_creator(s, 84, 23);
	obj->visual->p = (sfVector2f){column, row};
	sfSprite_setPosition(obj->visual->s, obj->visual->p);
	return (obj);
}
