/*
** EPITECH PROJECT, 2017
** game.c
** File description:
** global game manager
*/

#include "runner.h"
#include <stdlib.h>

options_t *game_creator(void)
{
	options_t *opts;

	opts = malloc(sizeof(options_t));
	opts->mode = (sfVideoMode){1067, 600, 8};
	opts->w = window_creator(opts->mode);
	opts->volume = 20;
	opts->bgs = music_creator("./assets/audio/surround.ogg",
	opts->volume, sfTrue);
	opts->lives = 3;
	if ((!opts) || (!opts->w) || (!opts->bgs))
		return (NULL);
	return (opts);
}

void game_destructor(options_t *opts)
{
	sfRenderWindow_destroy(opts->w);
	sfMusic_destroy(opts->bgs);
	free(opts);
}

static void loop_content(sfRenderWindow *w, bg_t *bg, character_t *johny,
sfClock *mover, moving_t **obj_pool)
{
	sfFont *font = sfFont_createFromFile("./assets/Doctor.ttf");
	sfText *text = sfText_create();

	sfText_setFont(text, font);
	e_control_center(w);
	sfRenderWindow_clear(w, sfBlack);
	background_display(w, bg);
	enemy_pool_display(obj_pool, w);
	character_display(w, johny, mover);
	sfText_setString(text, score2str(johny->score));
	sfRenderWindow_drawText(w, text, NULL);
	sfRenderWindow_display(w);
	sfFont_destroy(font);
	sfText_destroy(text);
}

void game_loop(options_t *opts, bg_t *bg, moving_t **obj_pool)
{
	character_t *johny = character_creator("./assets/img/char/char.png", 0);
	sfClock *mover = sfClock_create();
	if (!bg) {
		sfRenderWindow_close(opts->w);
		return;
	}
	sfMusic_play(opts->bgs);
	while (sfRenderWindow_isOpen(opts->w)) {
		loop_content(opts->w, bg, johny, mover, obj_pool);
	}
	background_destructor(bg);
	character_destructor(johny);
	sfMusic_stop(opts->bgs);
	sfClock_destroy(mover);
	enemy_pool_destructor(obj_pool);
}
