/*
** EPITECH PROJECT, 2017
** character.c
** File description:
** general character manager
*/

#include "runner.h"
#include <stdlib.h>

character_t *character_creator(char *path, float x_speed)
{
	character_t *perso = malloc(sizeof(character_t));

	if (!perso)
		return (NULL);
	sfVector2f speed = (sfVector2f){x_speed, 0};
	perso->looklike = animated_creator(path, speed, 7,
	"./assets/audio/death.ogg");
	perso->death = sfFalse;
	perso->jumping = sfFalse;
	if (!perso->looklike) {
		free(perso);
		return (NULL);
	}
	perso->looklike->visual->o = (sfVector2f){100, 415};
	sfSprite_setPosition(perso->looklike->visual->s,
	perso->looklike->visual->o);
	perso->score = 0;
	return (perso);
}

void character_destructor(character_t *perso)
{
	animated_destructor(perso->looklike);
	free(perso);
}

void character_display(sfRenderWindow *w, character_t *ch, sfClock *c)
{
	if (sfClock_getElapsedTime(c).microseconds >= 100000) {
		if ((sfKeyboard_isKeyPressed(sfKeySpace) == sfTrue) ||
		(ch->jumping == sfTrue))
			animated_jump(ch);
		else
			animated_run(ch->looklike);
		++ch->score;
		sfClock_restart(c);
	}
	sfSprite_move(ch->looklike->visual->s, ch->looklike->speed);
	sfRenderWindow_drawSprite(w, ch->looklike->visual->s, NULL);
	return;
}
