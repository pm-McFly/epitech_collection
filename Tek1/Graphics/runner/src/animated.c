/*
** EPITECH PROJECT, 2017
** animated.c
** File description:
** general animations manger
*/

#include "runner.h"
#include <stdlib.h>
#include <stdio.h>

animated_t *animated_creator(char *path_s, sfVector2f speed,
int nb_steps, char *path_m)
{
	animated_t *animation = malloc(sizeof(animated_t));

	if (!animation)
		return (NULL);
	animation->visual = v_entity_creator(path_s, 387 / 8, 71);
	if (!animation->visual) {
		free(animation);
		return (NULL);
	}
	animation->sound = music_creator(path_m, 100, sfFalse);
	if (!animation->sound) {
		free(animation->visual);
		free(animation);
		return (NULL);
	}
	animation->n_steps = nb_steps;
	animation->set_c = 0;
	animation->speed = speed;
	return (animation);
}

void animated_destructor(animated_t *animation)
{
	v_entity_destructor(animation->visual);
	sfMusic_destroy(animation->sound);
	free(animation);
}

void animated_run(animated_t *an)
{
	if (an->set_c == an->n_steps) {
		an->set_c = 1;
		an->visual->r.left = 0;
	}
	else {
		an->visual->r.left += an->visual->r.width;
		an->set_c += 1;
	}
	sfSprite_setTextureRect(an->visual->s, an->visual->r);
}

void animated_jump(character_t *ch)
{
	if (ch->looklike->visual->r.top == 0) {
		ch->looklike->visual->r.left = 0;
		ch->looklike->visual->r.top += ch->looklike->visual->r.height;
		ch->looklike->set_c = 0;
		ch->looklike->n_steps = 4;
		ch->looklike->speed.y = -7;
		ch->jumping = sfTrue;
	}
	else if (ch->looklike->set_c == 5) {
		ch->looklike->speed.y = 7;
	}
	else if (ch->looklike->set_c == 10) {
		ch->looklike->visual->r.left = 0;
		ch->looklike->set_c = 0;
		ch->looklike->n_steps = 7;
		ch->jumping = sfFalse;
		ch->looklike->speed.y = 0;
		ch->looklike->visual->r.top = 0;
		sfSprite_setPosition(ch->looklike->visual->s, ch->looklike->visual->o);
		return;
	}
	++ch->looklike->set_c;
	if (((ch->looklike->visual->r.left + ch->looklike->visual->r.width) >= ((387 / 2) - ch->looklike->visual->r.width)))
		;
	else
		ch->looklike->visual->r.left += ch->looklike->visual->r.width;
	sfSprite_setTextureRect(ch->looklike->visual->s,
	ch->looklike->visual->r);
}
