/*
** EPITECH PROJECT, 2017
** event.c
** File description:
** general event manager
*/

#include "runner.h"

void e_control_center(sfRenderWindow *w)
{
	sfEvent event;

	while (sfRenderWindow_pollEvent(w, &event)) {
		if(event.type == sfEvtClosed)
			sfRenderWindow_close(w);
	}
}

int m_control_center(sfRenderWindow *w, v_entity_t *select, sfClock *move)
{
	static int choice = 1;

	if (sfClock_getElapsedTime(move).microseconds >= 75000) {
		if (sfKeyboard_isKeyPressed(sfKeyDown) == sfTrue && choice != 2) {
			select->p.y += 125;
			++choice;
		}
		else if (sfKeyboard_isKeyPressed(sfKeyUp) == sfTrue && choice != 1) {
			select->p.y -= 125;
			--choice;
		}
	sfClock_restart(move);
	}
	sfSprite_setPosition(select->s, select->p);
	if (sfKeyboard_isKeyPressed(sfKeyReturn) == sfTrue) {
		if (choice == 2)
			sfRenderWindow_close(w);
		else
			return (1);
	}
	return (0);
}
