
# this message will be displayed each time the program is executed incorrectly #

USAGE: ./my_hunter {-h} [width | height | bpp]

{}: optional
[]: mandatory

Options:
	-h	if present display this

Arguments:
	width:	size of the window's width (MIN = 800 | MAX = 1920)
	height:	size of the window's height (MIN = 600 | MAX = 1080)
	bpp:	number of bytes to discribe a pixel (MIN = 8 | MAX = 32)


EXAMPLE:

	./my_hunter 800 600 32
