/*
** EPITECH PROJECT, 2017
** my_isneg
** File description:
** isneg
*/

void	my_putchar(char c);

int	my_isneg(int n)
{
	char c = 'P';

	if (n < 0)
		c = 'N';
	my_putchar(c);
	return (0);
}
