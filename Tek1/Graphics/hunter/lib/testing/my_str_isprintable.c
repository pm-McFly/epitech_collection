/*
** EPITECH PROJECT, 2017
** my_str_isprintable
** File description:
** is_printable
*/

int	my_str_isprintable(char const *str)
{
	int i = 0;
	int p = 1;

	while (str[i] != '\0' && p == 1) {
		if (str[i] < 32)
			p = 0;
		i++;
	}
	return (p);
}
