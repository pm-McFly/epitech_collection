/*
** EPITECH PROJECT, 2017
** my_str_isupper
** File description:
** is_upper
*/

int	info_char4 (char c);

int	my_str_isupper(char const *str)
{
	int i = 0;
	int alpha = 1;

	while (alpha == 1 && str[i] != '\0') {
		alpha -= info_char4(str[i]);
		i++;
	}
	return (alpha);
}

int	info_char4 (char c)
{
	if (c > 96 && c < 123)
		return (1);
	if (c > 65 && c < 91)
		return (0);
	else
		return (1);
}
