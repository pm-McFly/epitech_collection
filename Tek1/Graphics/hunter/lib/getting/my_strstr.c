/*
** EPITECH PROJECT, 2017
** my_strstr
** File description:
** strstr
*/

#include "my.h"
#include <stddef.h>

char *my_strstr (const char *s1, const char *s2)
{
	const char *p = s1;
	const int len = my_strlen (s2);

	for ( ; (p = my_strchr (p, *s2)) != 0; p++) {
		if (my_strncmp (p, s2, len) == 0)
			return (char *)p;
	}
	return (0);
}
