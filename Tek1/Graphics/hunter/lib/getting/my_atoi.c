/*
** EPITECH PROJECT, 2017
** my_getnbr
** File description:
** find in a given string a number
*/

int my_atoi(char *str)
{
	int sign = 1;
	int nbr = 0;

	while (*str == '-' || *str == '+') {
		if (*str == '-')
			sign = - sign;
		str++;
	}
	while (*str != '\0' && *str >= '0' && *str <= '9') {
		if (nbr > 214748364)
			return (0);
		if (nbr == 214748364 && *str > '7' && sign == 1)
			return (0);
		if (nbr == 214748364 && *str > '8' && sign == -1)
			return (0);
		nbr = nbr * 10 + *str - 48;
		str++;
	}
	if (sign == -1)
		nbr = - nbr;
	return (nbr);
}
