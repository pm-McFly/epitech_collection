/*
** EPITECH PROJECT, 2017
** my_print_ptr.c
** File description:
** my_printf
*/

#include "my.h"

int my_print_nbr(va_list ap)
{
	return (my_put_nbr(va_arg(ap, int)));
}

int my_print_unbr(va_list ap)
{
	return (my_put_unbr(va_arg(ap, unsigned int)));
}

int my_print_ptr(va_list ap)
{
	return (my_putstr("0x") + my_l_base_changer(va_arg(ap, char *), 16));
}
