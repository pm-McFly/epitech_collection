/*
** EPITECH PROJECT, 2017
** flag_str.c
** File description:
** Desc
*/

#include "my.h"

static const printf_action_t registers_str[] = {
	{ 'c', my_print_char },
	{ 'C', my_print_char },
	{ 's', my_print_string },
	{ 'S', my_print_ostring },
	{ 0, NULL }
};

int flag_str(char flag, va_list ap)
{
	int i = 0;

	while (registers_str[i].flag != 0) {
		if (registers_str[i].flag == flag) {
			return (registers_str[i].function(ap));
		}
		++i;
	}
	return (0);
}
