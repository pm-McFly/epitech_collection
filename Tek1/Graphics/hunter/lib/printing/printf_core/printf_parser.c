/*
** EPITECH PROJECT, 2017
** printf_parser.c
** File description:
** my_printf core
*/

#include "my.h"

int parser(char flag, va_list ap)
{
	int printed = 0;

	if (flag == '%')
		return (my_putchar('%'));
	printed += flag_nbr(flag, ap);
	printed += flag_str(flag, ap);
	return (printed);
}
