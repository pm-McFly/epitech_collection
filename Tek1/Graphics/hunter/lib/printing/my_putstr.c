/*
** EPITECH PROJECT, 2017
** my_putstr
** File description:
** put_str
*/

#include "my.h"

int	my_putstr(char const *str)
{
	return (write(1, str, my_strlen(str)));
}

int throw_error (char *error)
{
	return (write(2, error, my_strlen(error)));
}
