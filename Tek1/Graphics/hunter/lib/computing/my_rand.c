/*
** EPITECH PROJECT, 2017
** my_compute_power_rec
** File description:
** power_rec
*/

#include "my.h"

float my_rand(float min, float max)
{
 	int randnum = rand();
	float out = randnum % (int)(max - min) + (int)min;
	return (out);
}
