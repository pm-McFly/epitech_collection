/*
** EPITECH PROJECT, 2017
** my_strncpy.c
** File description:
**Reproduce the behavior of the strncmp function
*/

int my_strncmp(char const *s1, char const *s2, int nb)
{
	while (nb--)
		if (*s1++ != *s2++)
			return (*(char*)(s1 - 1) - *(char*)(s2 - 1));
	return (0);
}
