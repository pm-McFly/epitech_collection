/*
** EPITECH PROJECT, 2017
** my_compute_square_root
** File description:
** Desc
*/
int my_compute_square_root(int nb)
{
	int k = 1;

	if (nb == 0)
		k = 0;
	while (((k * k) - nb) != 0) {
		k = ((k + nb / k) / 2);
		if (((k * k) - nb) < 0)
			return (0);
	}
	return (k);
}
