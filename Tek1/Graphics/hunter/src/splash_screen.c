/*
** EPITECH PROJECT, 2017
** splash_screen.c
** File description:
** Desc
*/

#include "hunter.h"
#include "my.h"

static int check_event(p_settings_t *settings)
{
	if (check_wclose(settings->w) == EXIT_FCTN_WARNING)
		return (EXIT_FCTN_WARNING);
	if (sfMouse_isButtonPressed(sfMouseLeft) == sfTrue)
		if (game_loop(settings) == EXIT_FCTN_WARNING)
			return (EXIT_FCTN_WARNING);
	return (EXIT_FCTN_NORMALY);
}

static int display_splash(sfRenderWindow *w, sfSprite *title, sfSprite *bg)
{
	sfRenderWindow_clear(w, sfBlack);
	sfRenderWindow_drawSprite(w, bg, NULL);
	sfRenderWindow_drawSprite(w, title, NULL);
	sfRenderWindow_display(w);
	return (EXIT_FCTN_NORMALY);
}

int splash_screen(p_settings_t *settings)
{
	v_entity_t *title;

	if ((title = entity_create_fix("./assets/img/title.png", settings))
	== EXIT_FCTN_DANGER)
		return (EXIT_FCTN_WARNING);
	while (sfRenderWindow_isOpen(settings->w)) {
		if (check_event(settings) == EXIT_FCTN_WARNING) {
			sfSprite_destroy(title->sprite);
			sfTexture_destroy(title->texture);
			free (title);
			return (EXIT_FCTN_WARNING);
		}
		display_splash(settings->w, title->sprite, settings->sprite);
	}
	sfSprite_destroy(title->sprite);
	sfTexture_destroy(title->texture);
	free (title);
	return (EXIT_FCTN_NORMALY);
}
