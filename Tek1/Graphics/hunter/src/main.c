/*
** EPITECH PROJECT, 2017
** main.c
** File description:
** launch the game
*/

#include "my.h"
#include "hunter.h"

int main(int argc, char **argv)
{
	p_settings_t *settings;

	if (check_help_display(argc, argv) == EXIT_FCTN_WARNING)
		return (EXIT_PRGM_NORMALY);
	if ((settings = init_game(argv)) == EXIT_FCTN_DANGER)
		return (EXIT_PRGM_NORMALY);
	if (intro(settings) == EXIT_FCTN_WARNING) {
		sfRenderWindow_destroy(settings->w);
		free (settings);
		return (EXIT_PRGM_NORMALY);
	}
	if (game_launcher(settings) == EXIT_FCTN_WARNING) {
		sfRenderWindow_destroy(settings->w);
		free (settings);
		return (EXIT_PRGM_NORMALY);
	}
	sfRenderWindow_destroy(settings->w);
	free (settings);
	return (EXIT_PRGM_NORMALY);
}
