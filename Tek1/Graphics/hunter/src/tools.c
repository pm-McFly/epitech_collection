/*
** EPITECH PROJECT, 2017
** tools.c
** File description:
** other useful tools
*/

#include "my.h"
#include "hunter.h"

sfVector2f set_scale(sfVideoMode mode, sfVector2u size)
{
	sfVector2f scale;

	scale.x = ((float)mode.width / size.x);
	scale.y = ((float)mode.height / size.y);
	return (scale);
}

sfIntRect set_rect(int left, int top, int width, int height)
{
	sfIntRect rect;

	rect.left = left;
	rect.top = top;
	rect.width = width;
	rect.height = height;
	return (rect);
}

sfVector2f set_mvt(float mvt_x, float mvt_y)
{
	sfVector2f mv;

	mv.x = mvt_x;
	mv.y = mvt_y;
	return (mv);
}
