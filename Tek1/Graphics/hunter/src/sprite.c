/*
** EPITECH PROJECT, 2017
** sprite.c
** File description:
** Desc
*/

#include "hunter.h"
#include "my.h"

sfSprite *set_sprite(sfTexture *texture, float cscale, sfIntRect rect)
{
	sfVector2f scale;
	sfSprite *sprite;

	sprite = sfSprite_create();
	sfSprite_setTexture(sprite, texture, sfTrue);
	scale.x = cscale;
	scale.y = cscale;
	sfSprite_setScale(sprite, scale);
	sfSprite_setTextureRect(sprite, rect);
	return (sprite);
}

static int move_rect(v_entity_t *pony)
{
	if (pony->rect.left >= pony->columns)
		pony->rect.left = 0;
	pony->rect.left += pony->rect.width;
	sfSprite_setTextureRect(pony->sprite, pony->rect);
	return (EXIT_FCTN_NORMALY);
}

int sprite_animate(v_entity_t *pony, sfClock *clock, int w_sizex)
{
	int time = ABS(50000 / pony->mvt.x);

	if (pony->dead == sfTrue)
		time = 50000;
	if ((sfClock_getElapsedTime(clock)).microseconds >= time) {
		move_rect(pony);
		if (pony->dead == sfTrue &&
		pony->rect.left == (pony->columns - pony->rect.width)) {
			pony->dead = sfFalse;
			pony->rect = set_rect(0, 0, 100, 100);
			pony->columns = 15 * pony->rect.width;
			pony->origin.x = w_sizex;
			sfSprite_setPosition(pony->sprite, pony->origin);
		}
		sfClock_restart(clock);
	}
	return (EXIT_FCTN_NORMALY);
}
