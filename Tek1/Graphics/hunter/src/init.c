/*
** EPITECH PROJECT, 2017
** game.c
** File description:
** Desc
*/

#include "hunter.h"
#include "my.h"

p_settings_t *init_game(char **set)
{
	p_settings_t *settings;

	if ((settings = malloc(sizeof(p_settings_t))) == NULL) {
		throw_error("BAD MALLOC RETURN\n");
		return (NULL);
	}
	settings->score = 0;
	settings->nb_monster = 3;
	settings->life = 10;
	settings->mode.width = my_getnbr(set[1]);
	settings->mode.height = my_getnbr(set[2]);
	settings->mode.bitsPerPixel = my_getnbr(set[3]);
	if ((settings->w = window_init(settings->mode, "My_hunter")) == NULL)
		return (NULL);
	settings->cscale = (float)(settings->mode.width / MIN_WIDTH);
	return (settings);
}
