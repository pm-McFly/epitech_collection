/*
** EPITECH PROJECT, 2017
** cat_help.c
** File description:
** manage helper displaying
*/

#include "my.h"

static int display_help(void)
{
	int fd;
	int size;
	char buffer[32768];

	fd = open("./help/help.txt", O_RDONLY);
	if (fd == -1) {
		return (throw_error("ERROR WHILE OPENING\n"));
	}
	size = read(fd, buffer, 32768);
	buffer[size] = '\0';
	my_putstr(buffer);
	return (EXIT_FCTN_WARNING);
}

int check_help_display(int ac, char **av)
{
	if (ac > 4 || ac < 4)
		return (display_help());
	while (ac-- != 0) {
		if (my_strcmp(av[ac], "-h") == 0)
			return (display_help());
	}
	if (my_getnbr(av[1]) < 800 || my_getnbr(av[1]) > 1920)
		return (display_help());
	if (my_getnbr(av[2]) < 600 || my_getnbr(av[2]) > 1080)
		return (display_help());
	if (my_getnbr(av[3]) < 8 || my_getnbr(av[3]) > 32)
		return (display_help());
	return (0);
}
