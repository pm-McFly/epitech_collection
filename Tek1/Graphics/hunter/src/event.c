/*
** EPITECH PROJECT, 2017
** event.c
** File description:
** Desc
*/

#include "hunter.h"
#include "my.h"

static void pony_check_init(v_entity_t *pony, p_settings_t *set)
{
	pony->pos = sfSprite_getPosition(pony->sprite);
	set->cur_pos = sfMouse_getPositionRenderWindow(set->w);
	pony->hitbox = sfSprite_getGlobalBounds(pony->sprite);
	pony->origin.y = my_rand(((set->mode.height - (pony->rect.height *
		set->cscale)) - (set->mode.height / GRASS_SIZE_COEF)),
		(set->mode.height - (pony->rect.height * set->cscale)));
}

static void pony_kill(v_entity_t *pony)
{
	pony->dead = sfTrue;
	pony->rect.top = pony->rect.height;
	pony->columns = 24 * pony->rect.width;
	pony->mvt.x -= 0.02;
	pony->rect.left = 0;
	pony->value = my_rand(1, 10);
}

int pony_action(v_entity_t *pony, p_settings_t *set)
{
	pony_check_init(pony, set);
	if (sfKeyboard_isKeyPressed(sfKeySpace) == sfTrue)
		pony->mvt = set_mvt(-((float)set->mode.width / 5000.0), -0.00);
	if (sfKeyboard_isKeyPressed(sfKeyL) == sfTrue)
		pony->mvt = set_mvt(-((float)set->mode.width / 300.0), -0.00);
	if (sfMouse_isButtonPressed(sfMouseLeft) == sfTrue &&
	sfFloatRect_contains(&pony->hitbox, (float)set->cur_pos.x,
	(float)set->cur_pos.y) && pony->dead == sfFalse) {
		sfMusic_play(pony->sound);
		pony_kill(pony);
		set->score += pony->value;
		my_printf("score %u\n", set->score);
		my_printf("lives left %i\n", set->life);
	}
	if (pony->pos.x <= 0) {
		sfSprite_setPosition(pony->sprite, pony->origin);
		--set->life;
		my_printf("lives left %i\n", set->life);
	}
	if (set->life <= 0) {
		sfRenderWindow_close(set->w);
		return (EXIT_FCTN_WARNING);
	}
	return (EXIT_FCTN_NORMALY);
}

int check_wclose(sfRenderWindow *w)
{
	sfEvent event;

	while (sfRenderWindow_pollEvent(w, &event)) {
		if (event.type == sfEvtClosed) {
			sfRenderWindow_close(w);
			return (EXIT_FCTN_WARNING);
		}
	}
	return (EXIT_FCTN_NORMALY);
}
