/*
** EPITECH PROJECT, 2017
** intro.c
** File description:
** Desc
*/

#include "my.h"
#include "hunter.h"

static int clean_intro(sfTexture *texture, sfSprite *sprite, sfMusic *space,
	sfClock *timer)
{
	sfClock_destroy(timer);
	sfSprite_destroy(sprite);
	sfTexture_destroy(texture);
	sfMusic_destroy(space);
	return (EXIT_PRGM_NORMALY);
}

static int display_intro(sfRenderWindow *w, sfSprite *sprite)
{
	sfRenderWindow_clear(w, sfBlack);
	sfRenderWindow_drawSprite(w, sprite, NULL);
	sfRenderWindow_display(w);
	return (EXIT_FCTN_NORMALY);
}

int intro(p_settings_t *settings)
{
	sfTexture *texture = sfTexture_createFromFile("./assets/img/jacket.png",
	NULL);
	sfSprite *sprite = sfSprite_create();
	sfMusic *space = sfMusic_createFromFile("./assets/audio/space.ogg");
	sfClock *timer = sfClock_create();

	if (!texture || !space || !sprite || !timer) {
		clean_intro(texture, sprite, space, timer);
		return (EXIT_FCTN_WARNING);
	}
	sfSprite_setTexture(sprite, texture, sfTrue);
	sfSprite_setScale(sprite, set_scale(settings->mode,
		sfTexture_getSize(texture)));
	sfMusic_play(space);
	while ((sfClock_getElapsedTime(timer)).microseconds <= 4000000 &&
	sfRenderWindow_isOpen(settings->w)) {
		if (check_wclose(settings->w) == EXIT_FCTN_WARNING) {
			clean_intro(texture, sprite, space, timer);
			return (EXIT_FCTN_WARNING);
		}
		display_intro(settings->w, sprite);
	}
	clean_intro(texture, sprite, space, timer);
	return (EXIT_FCTN_NORMALY);
}
