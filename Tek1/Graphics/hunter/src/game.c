/*
** EPITECH PROJECT, 2017
** game.c
** File description:
** Desc
*/

#include "hunter.h"
#include "my.h"

static void destroy(v_entity_t *pony)
{
	sfMusic_destroy(pony->sound);
	sfTexture_destroy(pony->texture);
	sfSprite_destroy(pony->sprite);
	free (pony);
}

static void display_window(v_entity_t *pony, p_settings_t *pset, sfClock *clock)
{
	sfRenderWindow_clear(pset->w, sfBlack);
	sfRenderWindow_drawSprite(pset->w, pset->sprite, NULL);
	if (pony->dead != sfTrue)
		sfSprite_move(pony->sprite, pony->mvt);
	sfRenderWindow_drawSprite(pset->w, pony->sprite, NULL);
	sprite_animate(pony, clock, pset->mode.width);
	sfRenderWindow_display(pset->w);
}

static int check_event(p_settings_t *settings, v_entity_t *pony)
{
	if (check_wclose(settings->w) == EXIT_FCTN_WARNING)
		return (EXIT_FCTN_WARNING);
	pony_action(pony, settings);
	return (EXIT_FCTN_NORMALY);
}

int game_launcher(p_settings_t *pset)
{
	pset->sound = sfMusic_createFromFile("./assets/audio/surround.ogg");
	pset->texture = sfTexture_createFromFile("./assets/img/background.png",
	NULL);
	pset->sprite = sfSprite_create();
	if (!pset->sound || !pset->texture || !pset->sprite)
		return (EXIT_FCTN_WARNING);
	sfSprite_setTexture(pset->sprite, pset->texture, sfTrue);
	sfSprite_setScale(pset->sprite, set_scale(pset->mode,
		sfTexture_getSize(pset->texture)));
	sfMusic_setLoop(pset->sound, sfTrue);
	sfMusic_play(pset->sound);
	if ((splash_screen(pset)) == EXIT_FCTN_WARNING) {
		sfMusic_destroy(pset->sound);
		sfTexture_destroy(pset->texture);
		sfSprite_destroy(pset->sprite);
		return (EXIT_FCTN_WARNING);
	}
	sfMusic_destroy(pset->sound);
	sfTexture_destroy(pset->texture);
	sfSprite_destroy(pset->sprite);
	return (EXIT_FCTN_NORMALY);
}

int game_loop(p_settings_t *pset)
{
	v_entity_t *pony;
	sfClock *timer = sfClock_create();

	if ((pony = entity_create("./assets/img/enemy.png", pset,
"./assets/audio/explosion.wav"))
	== EXIT_FCTN_DANGER)
		return (EXIT_FCTN_WARNING);
	while(sfRenderWindow_isOpen(pset->w)) {
		if (check_event(pset, pony) == EXIT_FCTN_WARNING) {
			destroy(pony);
			return (EXIT_FCTN_WARNING);
		}
		display_window(pony, pset, timer);
	}
	destroy(pony);
	return (EXIT_FCTN_NORMALY);
}
