/*
** EPITECH PROJECT, 2017
** entity.c
** File description:
** Desc
*/

#include "hunter.h"
#include "my.h"

v_entity_t *entity_create_fix(char *file, p_settings_t *set)
{
	v_entity_t *entity;

	if ((entity = malloc(sizeof(v_entity_t))) == NULL) {
		throw_error("BAD MALLOC\n");
		return (EXIT_FCTN_DANGER);
	}
	entity->texture = sfTexture_createFromFile(file, NULL);
	entity->sprite = sfSprite_create();
	if (!entity->texture || !entity->sprite)
		return (EXIT_FCTN_DANGER);
	sfSprite_setTexture(entity->sprite, entity->texture, sfTrue);
	entity->scale.x = set->cscale;
	entity->scale.y = set->cscale;
	sfSprite_setScale(entity->sprite, entity->scale);
	entity->hitbox = sfSprite_getGlobalBounds(entity->sprite);
	entity->origin.x = ((set->mode.width / 2) - (entity->hitbox.width / 2));
	entity->origin.y = ((set->mode.height / 2) - (entity->hitbox.height / 2));
	sfSprite_setPosition(entity->sprite, entity->origin);
	return (entity);
}

v_entity_t *entity_create(char *file_i, p_settings_t *set, char *file_m)
{
	v_entity_t *entity;
	float max;

	if ((entity = malloc(sizeof(v_entity_t))) == NULL) {
		throw_error("BAD MALLOC\n");
		return (EXIT_FCTN_DANGER);
	}
	entity->texture = sfTexture_createFromFile(file_i, NULL);
	entity->rect = set_rect(0, 0, 100, 100);
	entity->sound = sfMusic_createFromFile(file_m);
	if (!entity->texture || !entity->sound)
		return (EXIT_FCTN_DANGER);
	max = (set->mode.height - (entity->rect.height * set->cscale));
	entity->origin.x = (set->mode.width - (entity->rect.width * set->cscale));
	entity->origin.y = max;
	entity->sprite = sfSprite_copy(set_sprite(entity->texture, set->cscale, entity->rect));
	entity->hitbox = sfSprite_getGlobalBounds(entity->sprite);
	sfSprite_setPosition(entity->sprite, entity->origin);
	entity->columns = 15 * entity->rect.width;
	entity->value = my_rand(1, 10);
	entity->lp = my_rand(1, 5);
	entity->dead = sfFalse;
	entity->mvt = set_mvt(-((float)set->mode.width / 1200.0), -0.00);
	return (entity);
}
