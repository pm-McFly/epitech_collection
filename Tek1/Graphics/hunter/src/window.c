/*
** EPITECH PROJECT, 2017
** window.c
** File description:
** Desc
*/

#include "hunter.h"
#include "my.h"

sfRenderWindow *window_init(sfVideoMode mode, char *name)
{
	sfRenderWindow *window;

	window = sfRenderWindow_create(mode, name, sfResize | sfClose, NULL);
	if (!window) {
		throw_error ("UNABLE TO CREATE WINDOW\n");
		return (NULL);
	}
	return (window);
}
