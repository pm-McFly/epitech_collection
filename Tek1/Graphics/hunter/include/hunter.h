/*
** EPITECH PROJECT, 2017
** ls.c
** File description:
** Desc
*/

#ifndef HUNTER_H_
#define HUNTER_H_	1

/* Include */
#include <SFML/Audio.h>
#include <SFML/Graphics.h>
#include <SFML/System.h>
#include <SFML/Window.h>

/* defines */
#define MIN_WIDTH	800
#define MAX_WIDTH	1920
#define MIN_HEIGHT	600
#define MAX_HEIGHT	1080
#define MIN_BPP		8
#define MAX_BPP		32
#define GRASS_SIZE_COEF 3.6044

/* objects */
typedef struct v_entity_s {
	sfTexture *texture;
	sfSprite *sprite;
	sfFloatRect hitbox;
	sfIntRect rect;
	sfVector2f mvt;
	sfVector2f scale;
	sfVector2f pos;
	sfVector2f origin;
	sfMusic *sound;
	sfBool dead;
	int columns;
	int value;
	int lp;
} v_entity_t;

typedef struct enemy_pool_s {
	v_entity_t *enemy;
	v_entity_t **next;
} enemy_pool_t;

typedef struct p_settings_s {
	sfVector2i cur_pos;
	sfTexture *texture;
	sfRenderWindow *w;
	sfVideoMode mode;
	sfSprite *sprite;
	sfMusic *sound;
	unsigned int score;
	int nb_monster;
	float cscale;
	int life;
} p_settings_t;

/* Prototypes */
int check_help_display(int, char **);
p_settings_t *init_game(char **);
sfRenderWindow *window_init(sfVideoMode, char *);
int intro(p_settings_t *);
sfVector2f set_scale(sfVideoMode, sfVector2u);
int check_wclose(sfRenderWindow *);
int game_launcher(p_settings_t *);
int splash_screen(p_settings_t *);
v_entity_t *entity_create_fix(char *, p_settings_t *);
sfIntRect set_rect(int, int, int, int);
sfVector2f set_mvt(float, float);
int sprite_animate(v_entity_t *, sfClock *, int);
int pony_action(v_entity_t *, p_settings_t *);
v_entity_t *entity_create(char *, p_settings_t *, char *);
sfSprite *set_sprite(sfTexture *, float, sfIntRect);
int game_loop(p_settings_t *);

#endif
