/*
** EPITECH PROJECT, 2018
** dirt.c
** File description:
** function
*/

#include "../include/iso.h"
#include "../include/map.h"
#include "../include/more_color.h"

static void draw_light_dirt(sfRenderWindow *w, map_t m)
{
	sfVertexArray *dirt = sfVertexArray_create();
	sfVertex c1;
	sfVertex c2;

	for (int i = 0; i < (MAP_Y - 1); ++i) {
		c1.color = DIRT_D;
		c2.color = DIRT_D;
		c1.position = m[i][MAP_X - 2].left.vertx.position;
		c2.position = m[i][MAP_X - 2].down.vertx.position;
		sfVertexArray_append(dirt, c2);
		sfVertexArray_append(dirt, c1);
		c1.position.y += (SCALE_Y * 2) + m[i][MAP_X - 2].left.height * 10;
		c2.position.y += (SCALE_Y * 2) + m[i][MAP_X - 2].down.height * 10;
		sfVertexArray_append(dirt, c1);
		sfVertexArray_append(dirt, c2);
		sfVertexArray_setPrimitiveType(dirt, PLAIN);
		sfRenderWindow_drawVertexArray(w, dirt, NULL);
		sfVertexArray_clear(dirt);
	}
}

static void draw_dark_dirt(sfRenderWindow *w, tile_t *border)
{
	sfVertexArray *dirt = sfVertexArray_create();
	sfVertex c1;
	sfVertex c2;

	for (int i = 0; i < (MAP_X); ++i) {
		c1.color = DIRT_C;
		c2.color = DIRT_C;
		c1.position = border[i].right.vertx.position;
		c2.position = border[i].down.vertx.position;
		sfVertexArray_append(dirt, c2);
		sfVertexArray_append(dirt, c1);
		c1.position.y += SCALE_Y * 2 + border[i].right.height * 10;
		c2.position.y += SCALE_Y * 2 + border[i].down.height * 10;
		sfVertexArray_append(dirt, c1);
		sfVertexArray_append(dirt, c2);
		sfVertexArray_setPrimitiveType(dirt, PLAIN);
		sfRenderWindow_drawVertexArray(w, dirt, NULL);
		sfVertexArray_clear(dirt);
	}
}

void win_display_map_dirt(sfRenderWindow *w, map_t m)
{
	draw_dark_dirt(w, m[MAP_Y - 2]);
	draw_light_dirt(w, m);
}
