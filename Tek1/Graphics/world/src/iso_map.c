/*
** EPITECH PROJECT, 2018
** map.c
** File description:
** function
*/

#include <stdlib.h>
#include "../include/more_color.h"
#include "../include/iso.h"
#include "../include/map.h"

static corner_t set_corner(int x, int y, int z)
{
	corner_t corner;

	corner.height = z;
	corner.xy = project_iso_point((x * SCALE_X), \
	(y * SCALE_Y), (z * SCALE_Z));
	corner.vertx.position = corner.xy;
	corner.vertx.color = GRASS_C;
	return (corner);
}

static map_t convert_2d_map(int **map_3d)
{
	map_t map_2d = NULL;

	if (map_3d == NULL) {
		return (NULL);
	}
	map_2d = malloc(sizeof(tile_t *) * MAP_Y);
	for (int i = 0; i < (MAP_Y - 1); ++i) {
		map_2d[i] = malloc(sizeof(tile_t) * MAP_X);
		for (int j = 0; j < (MAP_X - 1); ++j) {
			map_2d[i][j].active = false;
			map_2d[i][j].up = set_corner(j, i, map_3d[i][j]);
			map_2d[i][j].down = set_corner((j + 1), (i + 1), \
			map_3d[(i + 1)][(j + 1)]);
			map_2d[i][j].left = set_corner((j + 1), i, \
			map_3d[i][(j + 1)]);
			map_2d[i][j].right = set_corner(j, (i + 1), \
			map_3d[(i + 1)][j]);
		}
	}
	map_3d_i_delete(map_3d, (MAP_Y - 1));
	return (map_2d);
}

map_t get_map(char *path)
{
	int **raw_map;
	map_t map;

	if (path == NULL)
		return (NULL);
	raw_map = get_map_from_file(path);
	map = convert_2d_map(raw_map);
	return (map);
}
