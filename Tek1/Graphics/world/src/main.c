/*
** EPITECH PROJECT, 2018
** main.c
** File description:
** The program handler
*/

#include "../include/world.h"
#include "../include/window_funct.h"
#include "../include/tools.h"
#include "../include/help.h"
#include "../include/title.h"

int main(int ac, char **av, char **ev)
{
	world_t global;

	if (!print_help(ac, av))
		return (84);
	if (*ev == NULL) {
		throw_error(ENV_FAIL);
		return (84);
	}
	if (create_game_win(&global.w) == -1)
		return (84);
	if (title(global.w) == EXIT_GAME)
		return (0);
	game_loop(&global, av[1]);
	sfRenderWindow_destroy(global.w);
	return (0);
}
