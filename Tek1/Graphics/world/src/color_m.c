/*
** EPITECH PROJECT, 2018
** color_m.c
** File description:
** function
*/

#include "../include/more_color.h"

int ma(corner_t *c1, corner_t *c2, corner_t *c3, corner_t *c4)
{
	if (c2->height == c4->height && \
		c2->height > c3->height && \
		c2->height > c1->height) {
			c1->vertx.color = GRASS_D;
			c2->vertx.color = GRASS_D;
			c3->vertx.color = GRASS_D;
			c4->vertx.color = GRASS_D;
			return (1);
		}
	return (0);
}

int mb(corner_t *c1, corner_t *c2, corner_t *c3, corner_t *c4)
{
	if (c2->height > c4->height && \
		c2->height == c3->height && \
		c1->height < c2->height) {
			c1->vertx.color = GRASS_WC;
			c2->vertx.color = GRASS_WC;
			c3->vertx.color = GRASS_WC;
			c4->vertx.color = GRASS_WC;
			return (1);
		}
	return (0);
}

int mc(corner_t *c1, corner_t *c2, corner_t *c3, corner_t *c4)
{
	if (c1->height > c4->height && \
		c1->height == c3->height && \
		c1->height > c2->height) {
			c1->vertx.color = GRASS_WC;
			c2->vertx.color = GRASS_WC;
			c3->vertx.color = GRASS_WC;
			c4->vertx.color = GRASS_WC;
			return (1);
		}
	return (0);
}

int md(corner_t *c1, corner_t *c2, corner_t *c3, corner_t *c4)
{
	if (c1->height == c4->height && \
		c1->height > c3->height && \
		c1->height > c2->height) {
			c1->vertx.color = GRASS_D;
			c2->vertx.color = GRASS_D;
			c3->vertx.color = GRASS_D;
			c4->vertx.color = GRASS_D;
			return (1);
		}
	return (0);
}
