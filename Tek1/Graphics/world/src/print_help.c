/*
** EPITECH PROJECT, 2018
** print_help.c
** File description:
** Default
*/

#include "../include/help.h"
#include "../include/tools.h"

int	print_help(int ac, char **av)
{
	if (ac == 1)
		return (1);
	if (ac != 2)
		return (0);
	if (my_strcmp(av[1], "-h") == 0) {
		my_putstr("USAGE:\t./my_world [map]\n");
		my_putstr("[map]:\t(optional) opens a map file\n");
		return (0);
	}
	return (1);
}
