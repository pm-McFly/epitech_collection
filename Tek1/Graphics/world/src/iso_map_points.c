/*
** EPITECH PROJECT, 2018
** map_points.c
** File description:
** function
*/

#include <math.h>
#include "../include/my_maths.h"
#include "../include/iso.h"

sfVector2f project_iso_point(int x, int y, int z)
{
	sfVector2f iso_point;

	iso_point.x = ((ABS(cos(ANGLE_X)) * x) - \
	(ABS(cos(ANGLE_X)) * y)) + OFFSET_X;
	iso_point.y = ((ABS(sin(ANGLE_Y)) * y) + \
	(ABS(sin(ANGLE_Y)) * x) - z) + OFFSET_Y;
	return (iso_point);
}
