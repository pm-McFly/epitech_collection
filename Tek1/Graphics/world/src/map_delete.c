/*
** EPITECH PROJECT, 2018
** map_delete.c
** File description:
** function
*/

#include <stdlib.h>
#include "../include/iso.h"

void map_3d_i_delete(int **map, int len)
{
	if (map == NULL)
		return;
	for (int i = 0; i <= len; ++i) {
		free(map[i]);
	}
	free(map);
}

void map_3d_c_delete(char **map, int len)
{
	if  (map == NULL)
		return;
	for (int i = 0; i <= len; ++i) {
		free(map[i]);
	}
	free(map);
}

void map_delete(map_t map, int len)
{
	if (map == NULL)
		return;
	for (int i = 0; i <= len; ++i) {
		free(map[i]);
	}
	free(map);
}
