/*
** EPITECH PROJECT, 2018
** vertex.c
** File description:
** function
*/

#include <SFML/Graphics.h>
#include "../include/tools.h"
#include "../include/more_color.h"
#include "../include/iso.h"
#include "../include/vertex.h"

static sfVertexArray *get_line_vertex(sfVertex vertex1, sfVertex vertex2, \
	bool b)
{
	sfVertexArray *vertex_array = sfVertexArray_create();

	if (b == true) {
		vertex1.color = WIRE_S;
		vertex2.color = WIRE_S;
	} else {
		vertex1.color = WIRE;
		vertex2.color = WIRE;
	}
	sfVertexArray_append(vertex_array, vertex1);
	sfVertexArray_append(vertex_array, vertex2);
	sfVertexArray_setPrimitiveType(vertex_array, WIREF);
	return (vertex_array);
}

static sfVertexArray *get_quad_vertex(corner_t c1, corner_t c2, \
	corner_t c3, corner_t c4)
{
	sfVertexArray *vertex_array = sfVertexArray_create();

	set_color(&c1, &c2, &c3, &c4);
	sfVertexArray_append(vertex_array, c1.vertx);
	sfVertexArray_append(vertex_array, c3.vertx);
	sfVertexArray_append(vertex_array, c2.vertx);
	sfVertexArray_append(vertex_array, c4.vertx);
	sfVertexArray_setPrimitiveType(vertex_array, PLAIN);
	return (vertex_array);
}

static void render_plain_map(sfRenderWindow *w, map_t m)
{
	sfVertexArray *quad;

	for (int i = 0; i < (MAP_Y - 1); ++i) {
		for (int j = 0; j < (MAP_X - 1); ++j) {
			quad = get_quad_vertex(m[i][j].up, m[i][j].down, \
				m[i][j].left, m[i][j].right);
			sfRenderWindow_drawVertexArray(w, quad, NULL);
			sfVertexArray_clear(quad);
		}
	}
	sfVertexArray_destroy(quad);
}

static void render_wire_map(sfRenderWindow *w, map_t m)
{
	sfVertexArray *line;

	for (int i = 0; i < (MAP_Y - 1); ++i) {
		for (int j = 0; j < (MAP_X - 1); ++j) {
			line = get_line_vertex(m[i][j].up.vertx, \
				m[i][j].left.vertx, m[i][j].active);
			sfRenderWindow_drawVertexArray(w, line, NULL);
			sfVertexArray_clear(line);
			line = get_line_vertex(m[i][j].up.vertx, \
				m[i][j].right.vertx, m[i][j].active);
			sfRenderWindow_drawVertexArray(w, line, NULL);
			sfVertexArray_clear(line);
		}
	}
	sfVertexArray_destroy(line);
}

void win_display_map(sfRenderWindow *w, map_t m, sfPrimitiveType type)
{
	if (m == NULL) {
		sfRenderWindow_close(w);
		throw_error(MAP_FAIL);
		return;
	}
	if (type == PLAIN) {
		render_plain_map(w, m);
	}
	else {
		render_wire_map(w, m);
	}
}
