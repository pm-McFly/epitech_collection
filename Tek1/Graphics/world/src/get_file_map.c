/*
** EPITECH PROJECT, 2018
** file_map.c
** File description:
** function
*/

#include <stdlib.h>
#include <stdio.h>
#include "../include/get_next_line.h"
#include "../include/tools.h"
#include "../include/iso.h"
#include "../include/my_maths.h"

static int **convert_raw_map(char **raw_map)
{
	int z = 0;
	int **map = NULL;

	if (raw_map == NULL)
		return (NULL);
	map = malloc(sizeof(int *) * MAP_Y);
	if (map == NULL)
		return (NULL);
	for (int i = 0; i < MAP_Y; ++i) {
		map[i] = malloc(sizeof(int) * MAP_X);
		if (map[i] == NULL) {
			map_3d_i_delete(map, i);
			map_3d_c_delete(raw_map, MAP_Y);
			return (NULL);
		}
		for (int j = 0; j < MAP_X; ++j) {
			map[i][j] = my_getnbr(raw_map[z]);
			++z;
		}
	}
	map_3d_c_delete(raw_map, MAP_Y);
	return (map);
}

static char **get_raw_map(int fd)
{
	int i = 0;
	char *buff = NULL;
	char *line = get_next_line(fd);

	while (i < MAP_Y && *line) {
		buff = my_stradd(buff, line);
		buff = my_stradd(buff, ":");
		line = get_next_line(fd);
		++i;
	}
	if (line == NULL) {
		free(buff);
		return (NULL);
	}
	return (my_str_to_word_array(buff, ':'));
}

int **get_map_from_file(char *path)
{
	char **raw_map;
	int **map = NULL;
	int fd;

	fd = open(path, O_RDONLY);
	if (fd == -1)
		return (NULL);
	raw_map = get_raw_map(fd);
	map = convert_raw_map(raw_map);
	fd = close(fd);
	if (fd == -1)
		return (NULL);
	return (map);
}
