/*
** EPITECH PROJECT, 2018
** window.c
** File description:
** function
*/

#include <SFML/Window.h>
#include <SFML/Graphics.h>
#include "../include/window_const.h"
#include "../include/tools.h"

int create_game_win(sfRenderWindow **wdw)
{
	sfVideoMode mode;

	mode = (sfVideoMode){WIDTH, HEIGHT, BPP};
	*wdw = sfRenderWindow_create(mode, TITLE, sfDefaultStyle, NULL);
	if (!*wdw) {
		throw_error(WIN_FAIL);
		return (-1);
	}
	sfRenderWindow_setFramerateLimit(*wdw, FPS);
	sfRenderWindow_setVerticalSyncEnabled(*wdw, VSYNC);
	sfRenderWindow_setKeyRepeatEnabled(*wdw, KEYREPEAT);
	return (0);
}
