/*
** EPITECH PROJECT, 2018
** game_loop.c
** File description:
** function
*/

#include <stdio.h>
#include "../include/music.h"
#include "../include/map.h"
#include "../include/iso.h"
#include "../include/world.h"
#include "../include/buttons.h"
#include "../include/tools.h"
#include "../include/more_color.h"

static void check_map(char *path, map_t *map, sfRenderWindow *w)
{
	if (path != NULL)
		*map = get_map(path);
	else
		*map = get_map(PATH_MAP);
	if (map == NULL) {
		throw_error(MAP_FAIL);
		sfRenderWindow_close(w);
	}
}

void	event_handler(sfRenderWindow *w, obj_list list, unsigned char *t)
{
	sfVector2i	MousePos = sfMouse_getPositionRenderWindow(w);

	for (unsigned char i = 0; i < 3; ++i) {
		if (sfMouse_isButtonPressed(sfMouseLeft) &&
		sfFloatRect_contains(&list.btn[i].hb, MousePos.x, MousePos.y))
			*t = i;
	}
}

int game_loop(world_t *global, char *path)
{
	map_t		map;
	obj_list	list;
	unsigned char	current_tool = 2;

	check_map(path, &map, global->w);
	global->bgs = music_load("./assets/bgs1.ogg", sfTrue, 100);
	sfMusic_play(global->bgs);
	if (global->bgs == NULL)
		sfRenderWindow_close(global->w);
	map[3][4].active = true;
	load_sprites(&list);
	while (sfRenderWindow_isOpen(global->w)) {
		win_close_button(global->w);
		event_handler(global->w, list, &current_tool);
		sfRenderWindow_clear(global->w, VOID_BG);
		win_display_map(global->w, map, PLAIN);
		win_display_map(global->w, map, WIREF);
		win_display_map_dirt(global->w, map);
		render_sprites(global->w, &list, current_tool);
		sfRenderWindow_display(global->w);
	}
	map_delete(map, (MAP_Y - 2));
	sfMusic_destroy(global->bgs);
	return (0);
}
