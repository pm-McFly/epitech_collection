/*
** EPITECH PROJECT, 2018
** vertex_color.c
** File description:
** function
*/

#include <SFML/Graphics.h>
#include "../include/more_color.h"
#include "../include/map.h"

static void set_water(corner_t *c)
{
	if (c->height <= WATER_LVL)
		c->vertx.color = WATER;
}

static int pd_test(corner_t *c1, corner_t *c2, corner_t *c3, corner_t *c4)
{
	if (pdf(c1, c2, c3, c4) == 1)
		return (1);
	else if (pdr(c1, c2, c3, c4) == 1)
		return (1);
	else if (pdl(c1, c2, c3, c4) == 1)
		return (1);
	else if (pdb(c1, c2, c3, c4) == 1)
		return (1);
	return (0);
}

static int m_test(corner_t *c1, corner_t *c2, corner_t *c3, corner_t *c4)
{
	if (mc(c1, c2, c3, c4) == 1)
		return (1);
	else if (md(c1, c2, c3, c4) == 1)
		return (1);
	else if (ma(c1, c2, c3, c4) == 1)
		return (1);
	else if (mb(c1, c2, c3, c4) == 1)
		return (1);
	return (0);
}

static int pm_test(corner_t *c1, corner_t *c2, corner_t *c3, corner_t *c4)
{
	if (pmf(c1, c2, c3, c4) == 1)
		return (1);
	else if (pmr(c1, c2, c3, c4) == 1)
		return (1);
	else if (pml(c1, c2, c3, c4) == 1)
		return (1);
	else if (pmb(c1, c2, c3, c4) == 1)
		return (1);
	return (0);
}

void set_color(corner_t *c1, corner_t *c2, corner_t *c3, corner_t *c4)
{
	pm_test(c1, c2, c3, c4);
	m_test(c1, c2, c3, c4);
	pd_test(c1, c2, c3, c4);
	set_water(c1);
	set_water(c2);
	set_water(c3);
	set_water(c4);
}
