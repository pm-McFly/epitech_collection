/*
** EPITECH PROJECT, 2018
** title.c
** File description:
** Title screen for my_world
*/

#include <stdlib.h>
#include <stdio.h>
#include <SFML/Graphics.h>
#include <SFML/Audio.h>
#include <SFML/System/Export.h>
#include <SFML/System/Time.h>
#include <SFML/System/Types.h>
#include "../include/title.h"
#include "../include/world.h"

static void free_title(title_t *list)
{
	sfMusic_stop(list->bgm);
	sfMusic_destroy(list->bgm);
	sfSprite_destroy(list->bg);
	sfTexture_destroy(list->tmp);
}

static int	event_handler(sfRenderWindow *window, title_t *list)
{
	sfEvent		event;
	sfVector2i	MousePos = sfMouse_getPositionRenderWindow(window);

	while (sfRenderWindow_pollEvent(window, &event)) {
		if (event.type == sfEvtClosed) {
			sfRenderWindow_close(window);
			return (EXIT_GAME);
		}
		if (event.type == sfEvtKeyPressed &&
			event.key.code == sfKeyReturn) {
				return (RETURN_PRESSED);
			}
		}
		if (sfMouse_isButtonPressed(sfMouseLeft) &&
		    sfFloatRect_contains(&list->start_hitbox, MousePos.x,
					 MousePos.y)) {
				return (RETURN_PRESSED);
		}
		return (0);
}

static void	init_assets(title_t *list)
{
	list->bgm = sfMusic_createFromFile("./assets/title_theme.ogg");
	sfMusic_setLoop(list->bgm, sfTrue);
	sfMusic_play(list->bgm);
	list->tmp = sfTexture_createFromFile("./assets/title_img.png", NULL);
	list->bg = sfSprite_create();
	sfSprite_setTexture(list->bg, list->tmp, sfTrue);
	list->tmp = sfTexture_createFromFile("./assets/start_btn.png", NULL);
	list->btn_start = sfSprite_create();
	sfSprite_setTexture(list->btn_start, list->tmp, sfTrue);
	sfSprite_setScale(list->btn_start, (sfVector2f){2.0, 2.0});
	sfSprite_setPosition(list->btn_start, (sfVector2f){330.0, 450.0});
	list->start_hitbox = sfSprite_getGlobalBounds(list->btn_start);
}

static void render_room(sfRenderWindow *window, title_t list)
{
	sfRenderWindow_drawSprite(window, list.bg, NULL);
	sfRenderWindow_drawSprite(window, list.btn_start, NULL);
}

int	title(sfRenderWindow *window)
{
	int	input = 0;
	title_t	list;

	init_assets(&list);
	while (sfRenderWindow_isOpen(window)) {
		input = event_handler(window, &list);
		if (input == EXIT_GAME) {
			free_title(&list);
			return (EXIT_GAME);
		}
		if (input == RETURN_PRESSED) {
			free_title(&list);
			return (0);
		}
		render_room(window, list);
		sfRenderWindow_display(window);
	}
	return (0);
}
