/*
** EPITECH PROJECT, 2018
** music.c
** File description:
** function
*/

#include "../include/music.h"
#include "../include/tools.h"

sfMusic *music_load(char *path, sfBool loop, float volume)
{
	sfMusic *music;

	if (path == NULL) {
		throw_error(MUSIC_FAIL);
		return (NULL);
	}
	music = sfMusic_createFromFile(path);
	if (!music) {
		throw_error(MUSIC_FAIL);
		return (NULL);
	}

	sfMusic_setLoop(music, loop);
	sfMusic_setVolume(music, volume);
	return (music);
}
