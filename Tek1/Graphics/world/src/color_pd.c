/*
** EPITECH PROJECT, 2018
** color_pd.c
** File description:
** function
*/

#include "../include/more_color.h"

int pdf(corner_t *c1, corner_t *c2, corner_t *c3, corner_t *c4)
{
	if (c1->height == c4->height && \
		c1->height == c3->height && \
		c1->height > c2->height) {
			c1->vertx.color = GRASS_D;
			c2->vertx.color = GRASS_C;
			c3->vertx.color = GRASS_D;
			c4->vertx.color = GRASS_D;
			return (1);
		}
	return (0);
}

int pdr(corner_t *c1, corner_t *c2, corner_t *c3, corner_t *c4)
{
	if (c1->height > c4->height && \
		c1->height == c3->height && \
		c1->height == c2->height) {
			c1->vertx.color = GRASS_C;
			c2->vertx.color = GRASS_C;
			c3->vertx.color = GRASS_C;
			c4->vertx.color = GRASS_WC;
			return (1);
		}
	return (0);
}

int pdl(corner_t *c1, corner_t *c2, corner_t *c3, corner_t *c4)
{
	if (c1->height == c4->height && \
		c1->height > c3->height && \
		c1->height == c2->height) {
			c1->vertx.color = GRASS_C;
			c2->vertx.color = GRASS_C;
			c3->vertx.color = GRASS_D;
			c4->vertx.color = GRASS_C;
			return (1);
		}
	return (0);
}

int pdb(corner_t *c1, corner_t *c2, corner_t *c3, corner_t *c4)
{
	if (c2->height == c4->height && \
		c2->height == c3->height && \
		c1->height < c2->height) {
			c1->vertx.color = GRASS_WC;
			c2->vertx.color = GRASS_C;
			c3->vertx.color = GRASS_C;
			c4->vertx.color = GRASS_C;
			return (1);
		}
	return (0);
}
