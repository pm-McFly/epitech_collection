/*
** EPITECH PROJECT, 2018
** init_sprites.c
** File description:
** Default
*/

#include "../include/world.h"

static void	load_buttons(obj_list *list)
{
	sfTexture	*tmp;
	float		ypos = 40.0;

	tmp = sfTexture_createFromFile("./assets/btn1.png", NULL);
	sfSprite_setTexture(list->btn[0].s, tmp, sfTrue);
	tmp = sfTexture_createFromFile("./assets/btn2.png", NULL);
	sfSprite_setTexture(list->btn[1].s, tmp, sfTrue);
	tmp = sfTexture_createFromFile("./assets/btn3.png", NULL);
	sfSprite_setTexture(list->btn[2].s, tmp, sfTrue);
	tmp = sfTexture_createFromFile("./assets/btn4.png", NULL);
	sfSprite_setTexture(list->btn[3].s, tmp, sfTrue);
	for (int i = 0; i < 3; ++i) {
		sfSprite_setPosition(list->btn[i].s, (sfVector2f){21.0, ypos});
		list->btn[i].hb = sfSprite_getGlobalBounds(list->btn[i].s);
		ypos += 24;
	}
}

void load_sprites(obj_list *list)
{
	sfTexture	*tmp;

	tmp = sfTexture_createFromFile("./assets/tools.png", NULL);
	list->toolbox = sfSprite_create();
	sfSprite_setTexture(list->toolbox, tmp, sfTrue);
	sfSprite_setPosition(list->toolbox, (sfVector2f){5.0, 5.0});
	tmp = sfTexture_createFromFile("./assets/current.png", NULL);
	list->current_box = sfSprite_create();
	sfSprite_setTexture(list->current_box, tmp, sfTrue);
	sfSprite_setPosition(list->current_box, (sfVector2f){65.0, 5.0});
	for (int i = 0; i < 4; ++i)
		list->btn[i].s = sfSprite_create();
	load_buttons(list);
}

void render_sprites(sfRenderWindow *w, obj_list *list, unsigned char tool)
{
	sfSprite	*copy = sfSprite_copy(list->btn[tool].s);

	sfRenderWindow_drawSprite(w, list->toolbox, NULL);
	sfRenderWindow_drawSprite(w, list->current_box, NULL);
	for (unsigned char i = 0; i < 3; ++i)
		sfRenderWindow_drawSprite(w, list->btn[i].s, NULL);
	sfSprite_setPosition(copy, (sfVector2f){81.0, 36.0});
	sfRenderWindow_drawSprite(w, copy, NULL);
}
