/*
** EPITECH PROJECT, 2018
** button_close.c
** File description:
** function
*/

#include <SFML/Graphics.h>
#include <SFML/Window.h>

void win_close_button(sfRenderWindow *w)
{
	sfEvent event;

	while (sfRenderWindow_pollEvent(w, &event)) {
		if (event.type == sfEvtClosed)
			sfRenderWindow_close(w);
	}
}
