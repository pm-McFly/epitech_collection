/*
** EPITECH PROJECT, 2018
** my_putchar
** File description:
** putchar
*/

#include <unistd.h>

int my_putchar(char chr)
{
	return (write(1, &chr, 1));
}
