/*
** EPITECH PROJECT, 2018
** my_putstr
** File description:
** put_str
*/

#include <unistd.h>
#include "../include/tools.h"

int my_putstr(char const *str)
{
	return (write(1, str, my_strlen(str)));
}

int throw_error(char const *error)
{
	return (write(2, error, my_strlen(error)));
}
