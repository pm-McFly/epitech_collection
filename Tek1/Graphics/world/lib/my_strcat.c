/*
** EPITECH PROJECT, 2018
** my_strcat.c
** File description:
** cat str
*/

#include <stdlib.h>
#include "../include/tools.h"

char *my_stradd(char *dest, char *src)
{
	char *out;
	int idx;
	int jdx;

	if (src == NULL)
		return (NULL);
	if (dest == NULL)
		return (my_strdup(src));
	idx = my_strlen(src);
	jdx = my_strlen(dest);
	out = malloc(sizeof(char) * ((idx + jdx) + 1));
	if (out == NULL)
		return (NULL);
	out[(idx + jdx)] = 0;
	idx = 0;
	jdx = 0;
	while ((out[idx++] = dest[jdx++]));
	--idx;
	jdx = 0;
	while ((out[idx++] = src[jdx++]));
	return (out);
}

char *my_strcat(char *dest, char const *src)
{
	char *ptr = dest;

	while (*ptr)
		ptr++;
	while ((*ptr++ = *src++))
		;
	return (dest);
}
