/*
** EPITECH PROJECT, 2018
** get_next_line.c
** File description:
** clone function getline
*/

#include <stdlib.h>
#include <unistd.h>
#include "../include/tools.h"

int my_str_contain(char *str, char chr)
{
	for (int idx = 0; str[idx]; ++idx) {
		if (str[idx] == chr)
			return (1);
	}
	return (0);
}

static char *get_read_size(int fd)
{
	int idx = 0;
	char *buff = malloc(sizeof(char) * 2);

	if (buff == NULL) {
		return (NULL);
	} idx = read(fd, buff, 1);
	if (buff == NULL) {
		return (NULL);
	}
	if (idx <= 0) {
		return (NULL);
	}
	buff[idx] = '\0';
	return (buff);
}

char *get_next_line(int fd)
{
	char *buff;

	if (fd == -1)
		return (NULL);
	buff = get_read_size(fd);
	if (buff == NULL) {
		return (NULL);
	}
	while (my_str_contain(buff, '\n') != 1) {
		buff = my_stradd(buff, get_read_size(fd));
	}
	return (buff);
}
