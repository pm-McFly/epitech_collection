/*
** EPITECH PROJECT, 2018
** my_str_to_word_array.c
** File description:
** splits a string into words
*/

#include <stdlib.h>
#include "../include/tools.h"

static int count_word(char *str, char chr)
{
	int nb = 0;

	for (int idx = 0; str[idx]; ++idx) {
		if (str[idx] == chr)
			++nb;
	}
	++nb;
	return (nb);
}

char **my_str_to_word_array(char *str, char chr)
{
	char **tab;
	char *ptr = str;
	int cw = count_word(str, chr);
	int wn = 0;

	if (str == NULL)
		return (NULL);
	tab = malloc(sizeof(char *) * (cw + 2));
	if (tab == NULL)
		return (NULL);
	for (int idx = 0; ptr[idx]; ++idx) {
		if (ptr[idx] == chr) {
			ptr[idx] = '\0';
			tab[wn] = my_strdup(str);
			++wn;
			str = (ptr + (idx + 1));
		}
	}
	tab[wn] = my_strdup(str);
	tab[cw] = NULL;
	return (tab);
}
