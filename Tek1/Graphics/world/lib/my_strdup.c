/*
** EPITECH PROJECT, 2018
** my_strdup
** File description:
** strdup
*/

#include <stdlib.h>
#include "../include/tools.h"

char *my_strdup(const char *str)
{
	char *dup;

	dup = malloc(sizeof(char) * my_strlen(str) + 1);
	if (dup == NULL)
		return (NULL);
	else
		return (my_strcpy(dup, str));
}
