/*
** EPITECH PROJECT, 2018
** my_show_word_array
** File description:
** show word array
*/

#include <stddef.h>
#include "../include/tools.h"

int my_show_word_array(char * const *tab)
{
	int xn = 0;

	if (tab == NULL)
		return (0);
	while (tab[xn] != 0) {
		my_putstr(tab[xn]);
		my_putchar('\n');
		xn++;
	}
	return (0);
}
