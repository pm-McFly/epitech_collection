/*
** EPITECH PROJECT, 2018
** my_strlen
** File description:
** strlen
*/

#include <stddef.h>

int my_strlen(const char *str)
{
	if (str == NULL)
		return (0);
	int idx = 0;

	while (str[idx] != 0)
		idx++;
	return (idx);
}
