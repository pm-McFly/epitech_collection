/*
** EPITECH PROJECT, 2018
** my_getnbr
** File description:
** getnbr
*/

static int my_char_isnum(char chr)
{
	if (chr >= 48 && chr <= 57)
		return (1);
	else
		return (0);
}

int my_getnbr(char *str)
{
	int res = 0;
	int counter = 0;
	int sign = 1;

	while (str[counter]) {
		if (str[counter] == '-')
			sign *= -1;
		else if (my_char_isnum(str[counter]) == 1)
			res = res * 10 + (str[counter] - 48);
		else if (str[counter] != '+')
			return (res * sign);
		counter += 1;
		}
	return (res * sign);
}
