/*
** EPITECH PROJECT, 2018
** world.h
** File description:
** Header for the my_world project
*/

#ifndef WORLD_H_
# define WORLD_H_

# include <SFML/Audio.h>
# include <SFML/Window.h>
# include <SFML/Graphics.h>
# include "./map.h"

# define PATH_MAP "./assets/map"
# define ENV_FAIL "ERROR : No environment variable set!\n"

typedef struct world_s {
	sfRenderWindow *w;
	sfMusic *bgs;
} world_t;

typedef struct obj_struct {
	sfSprite	*s;
	sfFloatRect	hb;
} obj;

typedef	struct list_struct {
	sfSprite	*toolbox;
	sfSprite	*current_box;
	obj		btn[4];
} obj_list;

int game_loop(world_t *global, char *path);
void win_display_map(sfRenderWindow *w, map_t m, sfPrimitiveType type);
void win_display_map_dirt(sfRenderWindow *w, map_t m);
void render_sprites(sfRenderWindow *w, obj_list *list, unsigned char tool);
void load_sprites(obj_list *list);

#endif /* !WORLD_H_ */
