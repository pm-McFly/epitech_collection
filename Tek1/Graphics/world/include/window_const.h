/*
** EPITECH PROJECT, 2018
** window.h
** File description:
** header
*/

#ifndef WINDOW_H_
# define WINDOW_H_

# include <SFML/Config.h>
# include "./tools.h"

# define WIDTH 800
# define HEIGHT 600
# define BPP 32
# define NAME "My_world"
# define VERSION " | ALPHA_v0.7"
# define TITLE my_stradd(NAME, VERSION)
# define FPS 25
# define WIN_FAIL "ERROR : Window creation impossible!\n"
# define VSYNC sfTrue
# define KEYREPEAT sfFalse

#endif /* !WINDOW_H_ */
