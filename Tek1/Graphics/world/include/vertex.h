/*
** EPITECH PROJECT, 2018
** vertex.h
** File description:
** header
*/

#ifndef VERTEX_H_
# define VERTEX_H_

# include "../include/iso.h"

void set_color(corner_t *c1, corner_t *c2, corner_t *c3, corner_t *c4);

#endif /* !VERTEX_H_ */
