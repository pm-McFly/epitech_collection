/*
** EPITECH PROJECT, 2018
** get_next_line
** File description:
** .h
*/

#ifndef GET_NEXT_LINE_
# define GET_NEXT_LINE_

# include <stdlib.h>
# include <unistd.h>
# include <fcntl.h>

char *get_next_line(int fd);

#endif /* !GET_NEXT_LINE_ */
