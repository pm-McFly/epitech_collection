/*
** EPITECH PROJECT, 2018
** iso.h
** File description:
** header
*/

#ifndef ISO_H_
# define ISO_H_

# include <SFML/System/Vector2.h>
# include <SFML/Graphics/PrimitiveType.h>
# include "./map.h"

# define MAP_X 10
# define MAP_Y 10
# define ANGLE_X 41
# define ANGLE_Y 31
# define SCALE_X 40
# define SCALE_Y 40
# define SCALE_Z 10
# define OFFSET_X 400
# define OFFSET_Y 100
# define MAP_FAIL "ERROR : Unable to create map!\n"
# define PLAIN sfQuads
# define WIREF sfLinesStrip

int **get_map_from_file(char *path);
sfVector2f project_iso_point(int, int, int);
map_t get_map(char *path);
void map_3d_i_delete(int **map, int len);
void map_3d_c_delete(char **map, int len);
void map_delete(map_t map, int len);

#endif /* !ISO_H_ */
