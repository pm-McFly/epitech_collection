/*
** EPITECH PROJECT, 2018
** window_funct.h
** File description:
** header
*/

#ifndef WINDOW_FUNCT_H_
# define WINDOW_FUNCT_H_

# include <SFML/Graphics.h>

int create_game_win(sfRenderWindow **wdw);

#endif /* !WINDOW_FUNCT_H_ */
