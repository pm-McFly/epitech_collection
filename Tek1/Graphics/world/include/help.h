/*
** EPITECH PROJECT, 2018
** help.h
** File description:
** header
*/

#ifndef HELP_H_
# define HELP_H_

int print_help(int ac, char **av);

#endif /* !HELP_H_ */
