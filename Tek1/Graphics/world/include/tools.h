/*
** EPITECH PROJECT, 2018
** tools.h
** File description:
** header
*/

#ifndef TOOLS_H_
# define TOOLS_H_

int my_getnbr(char *str);
char *my_stradd(char *dest, char *src);
char *my_strcat(char *dest, char const *src);
int my_strlen(const char *str);
char **my_str_to_word_array(char *str, char chr);
char *my_strcpy(char *dest, char const *src);
char *my_strdup(const char *str);
int my_show_word_array(char * const *tab);
int my_putchar(char chr);
int my_putstr(char const *str);
int throw_error(char const *error);
int my_strcmp(char const *s1, char const *s2);

#endif /* !TOOLS_H_ */
