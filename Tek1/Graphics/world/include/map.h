/*
** EPITECH PROJECT, 2018
** map.h
** File description:
** header
*/

#ifndef MAP_H_
# define MAP_H_

# include <SFML/Graphics.h>
# include <stdbool.h>

# define WATER_LVL 0

typedef struct corner_s {
	sfVector2f xy;
	int height;
	sfVertex vertx;
} corner_t;

typedef struct tile_s {
	corner_t up;
	corner_t down;
	corner_t left;
	corner_t right;
	bool active;
} tile_t;

typedef tile_t ** map_t;

#endif /* !MAP_H_ */
