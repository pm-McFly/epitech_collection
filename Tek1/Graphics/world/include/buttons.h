/*
** EPITECH PROJECT, 2018
** buttons.h
** File description:
** header
*/

#ifndef BUTTONS_H_
# define BUTTONS_H_

# include <SFML/Graphics.h>

void win_close_button(sfRenderWindow *window);

#endif /* !BUTTONS_H_ */
