/*
** EPITECH PROJECT, 2018
** music.h
** File description:
** header
*/

#ifndef MUSIC_H_
# define MUSIC_H_

# include <SFML/Audio.h>

# define MUSIC_FAIL "ERROR : No audio file\n"

sfMusic *music_load(char *path, sfBool loop, float volume);

#endif /* !MUSIC_H_ */
