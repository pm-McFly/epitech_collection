/*
** EPITECH PROJECT, 2018
** title.h
** File description:
** header file for the title screen
*/

#ifndef TITLE_H_
# define TITLE_H_

#include <SFML/Audio.h>
#include <SFML/Graphics.h>

#define EXIT_GAME	1
#define RETURN_PRESSED	2
#define	START_GAME	3

int	title(sfRenderWindow *window);

typedef struct	title_obj_list_s {
	sfSprite	*bg;
	sfSprite	*btn_start;
	sfFloatRect	start_hitbox;
	sfTexture	*tmp;
	sfMusic		*bgm;
} title_t;

#endif /* !TITLE_H_ */
