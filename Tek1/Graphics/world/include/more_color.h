/*
** EPITECH PROJECT, 2018
** more_color.h
** File description:
** header
*/

#ifndef MORE_COLOR_H_
# define MORE_COLOR_H_

# include <SFML/Graphics/Color.h>
# include "./iso.h"

# define DIRT_C sfColor_fromRGB(141, 126, 71)
# define DIRT_D sfColor_fromRGB(101, 83, 45)
# define GRASS_WC sfColor_fromRGB(179, 196, 120)
# define GRASS_C sfColor_fromRGB(153, 188, 96)
# define GRASS_D sfColor_fromRGB(120, 173, 55)
# define WATER sfColor_fromRGBA(80, 142, 220, 200)
# define VOID_BG sfColor_fromRGB(34, 47, 56)
# define WIRE_S sfColor_fromRGBA(206, 49, 80, 200)
# define WIRE sfColor_fromRGBA(206, 249, 80, 130)

int pmf(corner_t *c1, corner_t *c2, corner_t *c3, corner_t *c4);
int pmr(corner_t *c1, corner_t *c2, corner_t *c3, corner_t *c4);
int pml(corner_t *c1, corner_t *c2, corner_t *c3, corner_t *c4);
int pmb(corner_t *c1, corner_t *c2, corner_t *c3, corner_t *c4);
int ma(corner_t *c1, corner_t *c2, corner_t *c3, corner_t *c4);
int mb(corner_t *c1, corner_t *c2, corner_t *c3, corner_t *c4);
int mc(corner_t *c1, corner_t *c2, corner_t *c3, corner_t *c4);
int md(corner_t *c1, corner_t *c2, corner_t *c3, corner_t *c4);
int pdf(corner_t *c1, corner_t *c2, corner_t *c3, corner_t *c4);
int pdr(corner_t *c1, corner_t *c2, corner_t *c3, corner_t *c4);
int pdl(corner_t *c1, corner_t *c2, corner_t *c3, corner_t *c4);
int pdb(corner_t *c1, corner_t *c2, corner_t *c3, corner_t *c4);

#endif /* !MORE_COLOR_H_ */
