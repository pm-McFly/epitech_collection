/*
** EPITECH PROJECT, 2017
** test.c
** File description:
** DESC
*/

#include "my.h"

void redirect_all_std(void)
{
	cr_redirect_stdout();
	cr_redirect_stderr();
}

Test(my_printf, simple_string, .init = redirect_all_std)
{
	my_printf("hello world");
	cr_assert_stdout_eq_str("hello world");
}

Test(my_printf, nothing_given, .init = redirect_all_std)
{
	cr_assert_eq(my_printf("\n"), printf("\n"));
	cr_assert_stdout_eq_str("\n");
}

Test(my_printf, simple_sentence, .init = redirect_all_std)
{
	cr_assert_eq(my_printf("Hello World !\n"), printf("Hello World !\n"));
	cr_assert_stdout_eq_str("Hello World !\n");
}

Test(my_printf, percent_flag, .init = redirect_all_std)
{
	cr_assert_eq(my_printf("percent_flag : %% test \n"), printf("percent_flag : %% test \n"));
	cr_assert_stdout_eq_str("percent_flag : % test \n");
}

Test(my_printf, d_i_flag, .init = redirect_all_std)
{
	cr_assert_eq(my_printf("i_flag : %%[%i] test \n", 42), printf("i_flag : %%[%i] test \n", 42));
	cr_assert_eq(my_printf("i_flag : %%%i test \n", -567), printf("i_flag : %%%i test \n", -567));
	cr_assert_eq(my_printf("d_flag : %%%d test \n", -567), printf("d_flag : %%%d test \n", -567));
	cr_assert_eq(my_printf("d_flag : %%%d test \n", 56745), printf("d_flag : %%%d test \n", 56745));
}

Test(my_printf, p_flag, .init = redirect_all_std)
{
	cr_assert_eq(my_printf("p_flag : [%p] test \n", "Xop"), printf("p_flag : [%p] test \n", "Xop"));
}

Test(my_printf, u_flag, .init = redirect_all_std)
{
	cr_assert_eq(my_printf("u_flag : %u test \n", -67), printf("u_flag : %u test \n", -67));
	cr_assert_eq(my_printf("u_flag : %u test \n", 67), printf("u_flag : %u test \n", 67));
}

Test(my_printf, b_flag, .init = redirect_all_std)
{
	cr_assert_eq(my_printf("b_flag : %b test \n", 2147483648), printf("b_flag : 10000000000000000000000000000000 test \n"));
	cr_assert_eq(my_printf("b_flag : %b test \n", 4294967295), printf("b_flag : 11111111111111111111111111111111 test \n"));
}

Test(my_printf, o_flag, .init = redirect_all_std)
{
	cr_assert_eq(my_printf("o_flag : %o test \n", -67), printf("o_flag : %o test \n", -67));
	cr_assert_eq(my_printf("o_flag : %o test \n", 67), printf("o_flag : %o test \n", 67));
}

Test(my_printf, x_flag, .init = redirect_all_std)
{
	cr_assert_eq(my_printf("x_flag : %x test \n", 15), printf("x_flag : %x test \n", 15));
	cr_assert_eq(my_printf("x_flag : %x test \n", 16), printf("x_flag : %x test \n", 16));
	cr_assert_eq(my_printf("x_flag : %x test \n", 123456789), printf("x_flag : %x test \n", 123456789));
}

Test(my_printf, X_flag, .init = redirect_all_std)
{
	cr_assert_eq(my_printf("X_flag : %X test \n", 15), printf("X_flag : %X test \n", 15));
	cr_assert_eq(my_printf("X_flag : %X test \n", 16), printf("X_flag : %X test \n", 16));
	cr_assert_eq(my_printf("X_flag : %X test \n", 123456789), printf("X_flag : %X test \n", 123456789));
}

Test(my_printf, c_flag, .init = redirect_all_std)
{
	cr_assert_eq(my_printf("c_flag : %c test \n", 'x'), printf("c_flag : %c test \n", 'x'));
	cr_assert_eq(my_printf("c_flag : %c test \n", 7), printf("c_flag : %c test \n", 7));
}

Test(my_printf, s_flag, .init = redirect_all_std)
{
	cr_assert_eq(my_printf("s_flag : %s test \n", "Xop"), printf("s_flag : %s test \n", "Xop"));
	cr_assert_eq(my_printf("s_flag : %s test \n", "7"), printf("s_flag : %s test \n", "7"));
	cr_assert_eq(my_printf("s_flag : %s test\n", "7\n"), printf("s_flag : %s test\n", "7\n"));
}

Test(my_printf, all_flags_only, .init = redirect_all_std)
{
	char *ptr = "hi I'm a pointer on 'h'\n";
	cr_assert_eq(my_printf("%% %i %d %p %u %o %x %X %c %s\n", 123, -6765433, ptr, -5432456, 54, -6789, 3452, 65, ptr), printf("%% %i %d %p %u %o %x %X %c %s\n", 123, -6765433, ptr, -5432456, 54, -6789, 3452, 65, ptr));
}

Test(my_printf, sentence_flags_combine, .init = redirect_all_std)
{
	cr_assert_eq(my_printf("asteks[%%%s", "moulinette]\n"), printf("asteks[%%%s", "moulinette]\n"));
	cr_assert_eq(my_printf("asteks%p\n", "moulinette"), printf("asteks%p\n", "moulinette"));
	cr_assert_eq(my_printf("asteks%i\n", -654322), printf("asteks%i\n", -654322));
	cr_assert_eq(my_printf("asteks%b\n", 4294967295), printf("asteks11111111111111111111111111111111\n"));
	cr_assert_eq(my_printf("asteks%b\n", 2147483648), printf("asteks10000000000000000000000000000000\n"));
}

Test(my_printf, S_flag,.init = redirect_all_std)
{
	char str[] = "toto";
	str[1] = 6;

	my_printf("S_flag : %S test \n", str);
	cr_assert_stdout_eq_str("S_flag : t\\006to test \n");
}
