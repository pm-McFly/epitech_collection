/*
** EPITECH PROJECT, 2017
** my_compute_power_rec
** File description:
** power_rec
*/

#include<stdio.h>

int	compute_rec(int nb, int p)
{
	long test = nb;

	if (test > 2147483647)
		return (0);
	if (p == 1)
		return (nb);
	if (p > 1 && (test * test < 2147483647))
		return (nb * compute_rec(nb, p - 1));
	else
		return (0);
}

int	my_compute_power_rec(int nb, int p)
{
	if (p < 0)
		return (0);
	else {
		if (p == 0)
			return (1);
		else
			return (compute_rec(nb, p));
	}
}
