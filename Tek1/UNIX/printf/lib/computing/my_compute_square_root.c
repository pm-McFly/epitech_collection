/*
** EPITECH PROJECT, 2017
** my_compute_square_root
** File description:
** square_root
*/

int	my_compute_square_root(int nb)
{
	int sqrt = nb / 2;

	if (nb == 1)
		return (1);
	else if (nb < 4)
		return (0);
	while (sqrt > nb / sqrt)
		sqrt--;
	if (sqrt * sqrt == nb)
		return (sqrt);
	else
		return (0);
}
