/*
** EPITECH PROJECT, 2017
** my_find_prime_sup
** File description:
** findprime_sup
*/

int	my_is_prime (int nb);

int	my_find_prime_sup (int nb)
{
	if (my_is_prime(nb) == 1)
		return (nb);
	if (nb % 2 == 0)
		nb = nb + 1;
	return (my_find_prime_sup(nb + 2));
}
