/*
** EPITECH PROJECT, 2017
** my_memcpy.c
** File description:
** My_printf
*/

void *my_memcpy (void *dest, const void *src)
{
        const char *srcp;
        char *dstp;
        int n = sizeof(src);

        srcp = src;
        dstp = dest;
        while (n--)
                *dstp++ = *srcp++;
        return (dstp);
}