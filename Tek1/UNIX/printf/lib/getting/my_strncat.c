/*
** EPITECH PROJECT, 2017
** my_strncat
** File description:
** str n cat
*/

char	*my_strncat (char *dest, char const *src, int n)
{
	int i = 0;
	int k = 0;

	while (dest[i] != '\0')
		i++;
	while (k < n && src[k] != '\0') {
		dest[i + k] = src[k];
		k += 1;
	}
	return (dest);
}
