/*
** EPITECH PROJECT, 2017
** my_strstr
** File description:
** strstr
*/

#include<stddef.h>
int     is_end (char *point, int i, char const *str);
char    *search_in (char const *str, char const *to_find, int start);

char	*my_strstr (char const *str, char const *to_find)
{
	char *point = NULL;
	int i = 0;

	while (str[i] != '\0' && point == NULL) {
		if (str[i] != to_find[0])
			i++;
		else {
			point = search_in(str, to_find, i);
			i = is_end (point, i, str);
		}
	}
	return (point);
}

char	*search_in (char const *str, char const *to_find, int start)
{
	char *point = NULL;
	int j = 0;
	int i = start;

	while (str[i] == to_find[j] && to_find[j] != '\0') {
		i++;
		j++;
	}
	if (to_find[j] == '\0') {
		point = (char*) str;
		point += start;
	}
	return (point);
}

int	is_end (char *point, int i, char const *str)
{
	int len = 0;

	while (str[len] != '\0')
		len++;
	if (point != NULL)
		i++;
	else
		i = len;
	return (i);
}
