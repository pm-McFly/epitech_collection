/*
** EPITECH PROJECT, 2017
** my_strcat
** File description:
** cat str
*/

char	*my_strcat (char *dest, char const *src)
{
	int i = 0;
	int j = 0;
	int k = 0;

	while (src[j] != '\0')
		j++;
	while (dest[i] != '\0')
		i++;
	while (k < j) {
		dest[i + k] = src[k];
		k += 1;
	}
	return (dest);
}
