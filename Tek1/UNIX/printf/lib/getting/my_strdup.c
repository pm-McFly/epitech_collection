/*
** EPITECH PROJECT, 2017
** my_strdup
** File description:
** strdup
*/

#include <stdlib.h>
#include <stdio.h>

char	*my_strdup (char const *src)
{
	char *str;
	int i = 0;
	int j = 0;

	while (src[i] != '\0')
		i++;
	str = malloc(i + 1);
	while (j < i) {
		str[j] = src[j];
		j++;
	}
	return (str);
}
