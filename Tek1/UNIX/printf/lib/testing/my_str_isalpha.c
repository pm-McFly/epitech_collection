/*
** EPITECH PROJECT, 2017
** my_str_isalpha
** File description:
** is alpha
*/

int	info_charx(char c);

int	my_str_isalpha(char const *str)
{
	int i = 0;
	int alpha = 1;

	while (alpha == 1 && str[i] != '\0') {
		alpha -= info_charx(str[i]);
		i++;
	}
	return (alpha);
}

int	info_charx (char c)
{
	if (c > 96 && c < 123)
		return (0);
	if (c > 64 && c < 91)
		return (0);
	else
		return (1);
}
