/*
** EPITECH PROJECT, 2017
** my_str_isnum
** File description:
** isnum
*/

int	info_char3 (char c);

int    my_str_isnum(char const *str)
{
	int i = 0;
	int alpha = 1;

	while (alpha == 1 && str[i] != '\0') {
		alpha -= info_char3(str[i]);
		i++;
	}
	return (alpha);
}

int	info_char3 (char c)
{
	if (c > 96 && c < 123)
		return (1);
	if (c > 64 && c < 91)
		return (1);
	else
		return (0);
}
