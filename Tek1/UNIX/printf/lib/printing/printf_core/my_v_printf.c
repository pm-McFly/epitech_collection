/*
** EPITECH PROJECT, 2017
** my_v_printf.c
** File description:
** Desc
*/

#include "my.h"

static int check(char a)
{
	char *flags = "%cCsSdipuboxX";

	while (flags++)
		if (*flags == a)
			return (1);
	exit (EXIT_ERROR);
}
void my_v_printf(char const *format)
{
	int i = 0;

	while (format[i]) {
		if (*format == '%') {
			++i;
			check(format[i]);
		}
		++i;
	}
}
