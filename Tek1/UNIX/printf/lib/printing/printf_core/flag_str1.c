/*
** EPITECH PROJECT, 2017
** flag_str1.c
** File description:
** Desc
*/

#include "my.h"

int my_print_char(va_list ap)
{
	return (my_putchar(va_arg(ap, int)));
}

int my_print_string(va_list ap)
{
	return (my_putstr(va_arg(ap, char *)));
}

int my_print_ostring(va_list ap)
{
	char *str = va_arg(ap, char *);
	int printed = 0;

	for (int i = 0; str[i] != '\0'; ++i) {
		if (str[i] <= 7) {
			printed += my_putchar('\\');
			printed += my_putstr("00");
			printed += my_u_base_changer((unsigned int) str[i], 8,
			0);
		}
		else if (str[i] <= 31) {
			printed += my_putchar('\\');
			printed += my_putchar('0');
			printed += my_u_base_changer((unsigned int) str[i], 8,
			0);
		}
		else if (str[i] >= 127) {
			printed += my_putchar('\\');
			printed += my_u_base_changer((unsigned int) str[i], 8,
			0);
		}
		else
			printed += my_putchar(str[i]);
	}
	return (printed);
}
