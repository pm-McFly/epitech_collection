/*
** EPITECH PROJECT, 2017
** my_put_nbr
** File description:
** put_nbr
*/

#include "my.h"

int	my_put_nbr (int nb)
{
	int size = 0;
	int div = 10;
	char c;
	int tmp;

	if (nb < 0) {
		size += my_putchar ('-');
		nb = nb * (-1);
	}
	while (nb / div != 0) {
		div = div * 10;
	}

	div = div / 10;
	while (div > 0) {
		tmp = nb / div;
		nb = nb % div;
		c = tmp + 48;
		size += my_putchar(c);
		div = div / 10;
	}
	return (size);
}