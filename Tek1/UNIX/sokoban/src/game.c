/*
** EPITECH PROJECT, 2017
** game.c
** File description:
** Desc
*/

#include "sokoban.h"

static int check_x(char **map)
{
	int j = 0;
	int i = 0;
	int blk = 0;
	int x = 0;

	while (map[i] != 0) {
		if (map[i][j] == '\0') {
			++i;
			j = 0;
		}
		else if (map[i][j] == 'X') {
			blk += check_arround(map[i][j - 1], map[i - 1][j],\
			map[i][j + 1], map[i + 1][j]);
			++x;
		}
		++j;
	}
	if (x != blk)
		return (EXIT_FCTN_NORMALY);
	return (EXIT_FCTN_FAILURE);
}

static char **place_slots(char **map, int **o_pos)
{
	for (int i = 0; o_pos[i] != NULL; ++i)
		if (map[o_pos[i][1]][o_pos[i][0]] == ' ')
			map[o_pos[i][1]][o_pos[i][0]] = 'O';
	return (map);
}

static int check_end(sokoban_t *game)
{
	game->map = place_slots(game->map, game->o_pos);
	if (check_x(game->map) == EXIT_FCTN_FAILURE) {
		endwin();
		exit (EXIT_FCTN_FAILURE);
	}
	for (int i = 0; game->map[i] != 0; ++i) {
		if (my_strstr(game->map[i], "O") != NULL)
			return (EXIT_FCTN_FAILURE);
	}
	return (EXIT_FCTN_NORMALY);
}

static void control_center(sokoban_t *game)
{
	int inp = getch();

	if (inp == KEY_DOWN)
		player_move(game->p_pos, 0, 1, game->map);
	else if (inp == KEY_UP)
		player_move(game->p_pos, 0, -1, game->map);
	else if (inp == KEY_RIGHT)
		player_move(game->p_pos, 1, 0, game->map);
	else if (inp == KEY_LEFT)
		player_move(game->p_pos, -1, 0, game->map);
	else if (inp == 32)
		game_loop(get_map(game->reset));
	else if (inp == 27) {
		endwin();
		exit (EXIT_FCTN_FAILURE);
	}
}

void game_loop(sokoban_t *game)
{
	WINDOW *w;

	w = initscr();
	keypad(w, 1);
	attron(A_NORMAL);
	curs_set(FALSE);

	while (check_end(game) != EXIT_FCTN_NORMALY) {
		display(game->map, game->p_pos, game->x, game->y);
		control_center(game);
	}
	erase();
	endwin();
	exit(EXIT_PRGM_NORMALY);
}
