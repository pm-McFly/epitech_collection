/*
** EPITECH PROJECT, 2017
** display.c
** File description:
** Desc
*/

#include "sokoban.h"

void display(char **map, skbn_char_t *p_pos, int s_x, int s_y)
{
	if (s_x > COLS || s_y > LINES)
		signal(SIGWINCH, handle_winch);
	else {
		for (int i = 0; map[i]; ++i)
			mvprintw(0 + i, 0, "%s\n", map[i]);
		mvprintw(p_pos->y, p_pos->x, "P");
	}
}
