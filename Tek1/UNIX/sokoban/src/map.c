/*
** EPITECH PROJECT, 2017
** map.c
** File description:
** Desc
*/

#include "sokoban.h"

static void check_map(char *buff)
{
	for (int i = 0; buff[i] != '\0'; ++i) {
		if (buff[i] == ' ');
		else if (buff[i] == '#');
		else if (buff[i] == 'P');
		else if (buff[i] == 'X');
		else if (buff[i] == 'O');
		else if (buff[i] == '\n');
		else {
			throw_error("BAD TOKEN FOUND <map>\n");
			exit (INVALID_CHAR);
		}
	}
}

sokoban_t *get_map(char *file)
{
	sokoban_t *game;
	char *buff = get_file_in_buff(file);

	check_map(buff);
	if ((game = malloc(sizeof(sokoban_t))) == NULL) {
		throw_error("BAD MALLOC\n");
		exit (BAD_RETURN);
	}
	game->reset = my_strdup(file);
	game->x = max_len(buff);
	game->y = high(buff);
	game->map = store_map(buff, game->x, game->y);
	game->p_pos = get_player_pos(game->map);
	game->o_pos = get_slot_pos(game->map, count_o(buff));
	return (game);
}
