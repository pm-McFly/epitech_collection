/*
** EPITECH PROJECT, 2017
** strcut_sokoban.c
** File description:
** Desc
*/

#include "sokoban.h"

static char **char_alloc(int length, int lines)
{
	int i = 0;
	char **map = NULL;

	if ((map = malloc(sizeof(char *) * (lines + 1))) == NULL) {
		throw_error("BAD MALLOC\n");
		exit (BAD_RETURN);
	}
	while (i != lines) {
		if ((map[i++] = malloc(sizeof(char) * (length + 1))) == NULL) {
			throw_error("BAD MALLOC\n");
			exit (BAD_RETURN);
		}
	}
	return (map);
}

int max_len(char *map)
{
	int x = 0;
	int save = 0;
	int i = 0;

	while (map[i++]) {
		if (map[i] == '\n') {
			((save < x) ? (save = x) : (save = save));
			x = 0;
		}
		else
			++x;
	}
	return (save);
}

int high(char *map)
{
	int l = 0;

	for (int i = 0; map[i]; ++i) {
		if (map[i] == '\n')
			++l;
	}
	return (l);
}

char **store_map(char *buff, int length, int lines)
{
	int i = 0;
	int j = 0;
	char **map = char_alloc(length, lines);

	while (*buff) {
		if (*buff == '\n') {
			map[i][j] = '\0';
			++i;
			j = 0;
		}
		else
			map[i][j++] = *buff;
		buff++;
	}
	map[i] = 0;
	return (map);
}
