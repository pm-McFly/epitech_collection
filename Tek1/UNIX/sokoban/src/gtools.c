/*
** EPITECH PROJECT, 2017
** tools.c
** File description:
** Desc
*/

#include "sokoban.h"

void check_err(int n)
{
	if (n != 2)
		exit(EXIT_USAGE);
}

int count_o(char *map)
{
	int nb = 0;

	for (int i = 0; map[i]; ++i) {
		if (map[i] == 'O')
			++nb;
	}
	return (nb);
}

int check_arround(char a, char b, char c, char d)
{
	if (a == '#' && b == '#')
		return (EXIT_FCTN_FAILURE);
	if (b == '#' && c == '#')
		return (EXIT_FCTN_FAILURE);
	if (c == '#' && d == '#')
		return (EXIT_FCTN_FAILURE);
	if (d == '#' && a == '#')
		return (EXIT_FCTN_FAILURE);
	return (EXIT_FCTN_NORMALY);
}

void handle_winch(int sig)
{
	char *tmp = "Your Terminal is too small please resize it !";
	int x = COLS / 2 - strlen(tmp) / 2;
	int y = LINES / 2 - 1;

	signal(SIGWINCH, SIG_IGN);
	endwin();
	initscr();
	refresh();
	curs_set(FALSE);
	clear();
	mvaddstr(y - (sig-sig), x, tmp);
	refresh();
	signal(SIGWINCH, handle_winch);
}
