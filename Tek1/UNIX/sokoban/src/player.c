/*
** EPITECH PROJECT, 2017
** player.c
** File description:
** Desc
*/

#include "sokoban.h"

void player_move(skbn_char_t *p, int vx, int vy, char **map)
{
	if (map[p->y + vy][p->x + vx] == 'X' &&\
	(map[p->y + (vy * 2)][p->x + (vx * 2)] != '#' &&\
	map[p->y + (vy * 2)][p->x + (vx * 2)] != 'X')) {
		map[p->y + vy][p->x + vx] = ' ';
		map[p->y + (vy * 2)][p->x + (vx * 2)] = 'X';
	}
	if (map[p->y + vy][p->x + vx] != '#' &&\
	map[p->y + vy][p->x + vx] != 'X') {
		p->x += vx;
		p->y += vy;
	}
}

skbn_char_t *get_player_pos(char **map)
{
	int j = 0;
	int i = 0;
	skbn_char_t *player;

	if ((player = malloc(sizeof(skbn_char_t))) == NULL) {
		throw_error("BAD MALLOC\n");
		exit (BAD_RETURN);
	}
	while (map[i] != 0) {
		if (map[i][j] == '\0') {
			++i;
			j = 0;
		}
		else if (map[i][j] == 'P') {
			map[i][j] = ' ';
			player->x = j;
			player->y = i;
			return (player);
		}
		++j;
	}
	throw_error("PLAYER_UNEXIST\n");
	exit (PLAYER_UNEXIST);
}
