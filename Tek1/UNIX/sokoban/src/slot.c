/*
** EPITECH PROJECT, 2017
** slot.c
** File description:
** Desc
*/

#include "sokoban.h"

int **get_slot_pos(char **map, int nb_o)
{
	int j = 0;
	int i = 0;
	int z = 0;
	int **obj;

	if ((obj = malloc(sizeof(int *) * (nb_o + 1))) == NULL) {
		throw_error("BAD MALLOC\n");
		exit (BAD_RETURN);
	}
	while (map[i] != 0) {
		if (map[i][j] == '\0') {
			++i;
			j = 0;
		}
		else if (map[i][j] == 'O') {
			obj[z] = malloc(sizeof(int) * 2);
			obj[z][0] = j;
			obj[z][1] = i;
			++z;
		}
		++j;
	}
	obj[z] = NULL;
	return (obj);
}
