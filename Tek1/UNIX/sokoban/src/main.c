/*
** EPITECH PROJECT, 2017
** main.c
** File description:
** Desc
*/

#include "sokoban.h"

int main(int ac, char **av)
{
	sokoban_t *game = NULL;
	check_err(ac);
	game = get_map(av[1]);
	game_loop(game);
	return (0);
}
