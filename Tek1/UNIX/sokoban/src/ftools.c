/*
** EPITECH PROJECT, 2017
** file_tools.c
** File description:
** Desc
*/

#include "sokoban.h"

static int open_file(char *file_path, int flag)
{
	int fd;

	if ((fd = open(file_path, flag)) == -1) {
		throw_error("FILE CAN'T BE READED | NOT FOUND\n");
		exit (FILE_NOT_FOUND);
	}
	else
		return (fd);
}

static int get_file_size(char *path)
{
	int size;
	int tmp;
	int fd;
	char *buff;

	if ((buff = malloc(sizeof(char) * 10)) == NULL)  {
		throw_error("BAD MALLOC\n");
		exit (BAD_RETURN);
	}
	fd = open(path, O_RDONLY);
	size = 10;
	tmp = read(fd, buff, 10);
	while (tmp == 10) {
		tmp = read(fd, buff, 10);
		size += 10;
	}
	close(fd);
	free(buff);
	return (size);
}

char *get_file_in_buff(char *path)
{
	int fd;
	char *buff = NULL;
	int size = get_file_size(path);

	if ((buff = malloc(sizeof(char) * (size + 1))) == NULL) {
		throw_error("BAD MALLOC\n");
		exit (BAD_RETURN);
	}
	fd = open_file(path, O_RDONLY);
	size = read(fd, buff, size);
	buff[size] = '\0';
	close(fd);
	return (buff);
}
