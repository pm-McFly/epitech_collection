/*
** EPITECH PROJECT, 2017
** ls.c
** File description:
** Desc
*/

#ifndef SOKOBAN_H_
#define SOKOBAN_H_	1

/* Include */
#include "my.h"
#include <curses.h>
#include <sys/ioctl.h>
#include <signal.h>

/* defines */

/* errors */
#define PLAYER_UNEXIST 84

/* structures */
typedef struct skbn_char_s {
	int x;
	int y;
} skbn_char_t;

typedef struct sokoban_s {
	char *reset;
	char **map;
	int x;
	int y;
	skbn_char_t *p_pos;
	int **o_pos;
} sokoban_t;

/* Prototypes */
void check_err(int);
sokoban_t *get_map(char *);
char *get_file_in_buff(char *);
char **store_map(char *, int, int);
int max_len(char *);
int high(char *);
skbn_char_t *get_player_pos(char **);
int **get_slot_pos(char **, int);
int count_o(char *);
void game_loop(sokoban_t *);
void display(char **, skbn_char_t *, int, int);
void player_move(skbn_char_t *, int, int, char **);
int check_arround(char, char, char, char);
void handle_winch(int sig);

#endif
