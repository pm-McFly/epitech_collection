/*
** EPITECH PROJECT, 2017
** flag_nbr.c
** File description:
** my_printf core
*/

#include "my.h"

static const printf_action_t registers_nbr[] = {
	{ 'd', my_print_nbr },
	{ 'i', my_print_nbr },
	{ 'p', my_print_ptr },
	{ 'u', my_print_unbr },
	{ 'b', my_print_binary },
	{ 'o', my_print_octal },
	{ 'x', my_print_lhex },
	{ 'X', my_print_uhex },
	{ 0, NULL }
};

int flag_nbr(char flag, va_list ap)
{
	int i = 0;

	while (registers_nbr[i].flag != 0) {
		if (registers_nbr[i].flag == flag) {
			return (registers_nbr[i].function(ap));
		}
		++i;
	}
	return (0);
}
