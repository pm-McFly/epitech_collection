/*
** EPITECH PROJECT, 2017
** base_changer.c
** File description:
** my_printf core
*/

#include "my.h"

int my_u_base_changer(unsigned int nbr, int base, int caps)
{
	int printed = 0;
	int index = 0;
	int converted_number[1048576];
	char base_digits[16] = "0123456789abcdef";

	if (caps == 1)
		my_strupcase(base_digits);
	while (nbr != 0) {
		converted_number[index] = nbr % base;
		nbr = nbr / base;
		++index;
	}
	--index;
	while (index >= 0) {
		printed += my_putchar(base_digits[converted_number[index]]);
		--index;
	}
	return (printed);
}

int my_l_base_changer(char *nbr, int base)
{
	long adr = (long) nbr;
	int printed = 0;
	long index = 0;
	int converted_number[1048576];
	char base_digits[16] = "0123456789abcdef";

	while (adr != 0) {
		converted_number[index] = adr % base;
		adr = adr / base;
		++index;
	}
	--index;
	for ( ; index >= 0; index--) {
		printed += my_putchar(base_digits[converted_number[index]]);
	}
	return (printed);
}
