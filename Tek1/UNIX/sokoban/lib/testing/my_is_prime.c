/*
** EPITECH PROJECT, 2017
** my_is_prime
** File description:
** is_prime
*/

int	rec_is_prime(int nb, int prime, int div)
{
	if (nb % div == 0)
		prime = 0;
	if (div * div < nb && prime == 1)
		return (rec_is_prime (nb, prime, div + 1));
	return (prime);
}

int	my_is_prime(int nb)
{
	if (nb < 2)
		return (0);
	if (nb == 2)
		return (1);
	return (rec_is_prime (nb, 1, 2));
}
