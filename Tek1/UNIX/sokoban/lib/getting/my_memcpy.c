/*
** EPITECH PROJECT, 2017
** my_memcpy.c
** File description:
** My_printf
*/

#include "my.h"

void *my_memecpy (void *dest, const void *src)
{
	const char *srcp;
	char *dstp;
	int n = sizeof(src);

	srcp = src;
	dstp = dest;
	while (n--)
		*dstp++ = *srcp++;
	return (dstp);
}

void my_bcopy (const void *src, void *dest, size_t len)
{
	if (dest < src) {
		const char *firsts = (const char *) src;
		char *firstd = (char *) dest;
		while (len--)
		*firstd++ = *firsts++;
	}
	else {
		const char *lasts = (const char *)src + (len-1);
		char *lastd = (char *)dest + (len-1);
		while (len--)
			*lastd-- = *lasts--;
	}
}

PTR my_memcpy (PTR out, const PTR in, size_t length)
{
	my_bcopy(in, out, length);
	return out;
}
