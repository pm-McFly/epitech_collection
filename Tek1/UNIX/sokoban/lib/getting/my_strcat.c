/*
** EPITECH PROJECT, 2017
** my_strcat.c
** File description:
** cat str
*/

char	*my_strcat (char *dest, char const *src)
{
	char *p = dest;

	while (*p)
		p++;
	while ((*p++ = *src++))
		;
	return dest;
}
