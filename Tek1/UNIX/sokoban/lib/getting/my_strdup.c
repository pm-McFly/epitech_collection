/*
** EPITECH PROJECT, 2017
** my_strdup
** File description:
** strdup
*/

#include "my.h"

char *my_strdup(const char *s)
{
	size_t len = (long unsigned int)my_strlen(s) + 1;
	char *result;
	if ((result = (char*) malloc (len)) == NULL)
		return (NULL);
	if (result == (char*) 0)
		return (char*) 0;
	return ((char*) my_memcpy (result, s, len));
}
