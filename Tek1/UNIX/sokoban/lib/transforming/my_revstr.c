/*
** EPITECH PROJECT, 2017
** my_revstr
** File description:
** reverse str
*/

char	*my_revstr(char *str)
{
	int i = 0;
	int j = 0;
	char tmp;

	while (str[j] != '\0')
		j++;
	j -= 1;
	while (i < j) {
		tmp = str[j];
		str[j] = str[i];
		str[i] = tmp;
		i++;
		j--;
	}
	return (str);
}
