/*
** EPITECH PROJECT, 2017
** ls.c
** File description:
** ls copy
*/

#include "my.h"
#include "my_ls.h"

int main (int argc, char **argv)
{
	int *pos;
	int start = 1;
	char *flag;

	pos = flag_core(argc, argv);
	flag = fdecryptor(pos[0]);
	if (argc == 1 || pos[start] == -1) {
		if (my_strstr(flag, "R") != NULL)
			my_putstr(".:\n");
		dirwalk(".", flag);
	}
	else
		while (pos[start] != -1) {
			if ((my_strstr(flag, "R") != NULL) ||\
			(pos[2] != -1 && my_strstr(flag, "d") == NULL))
				my_printf("%s:\n", argv[pos[start]]);
			dirwalk(argv[pos[start]], fdecryptor(pos[0]));
			++start;
			if (pos[2] != -1 && pos[start] != -1 &&\
			my_strstr(flag, "d") == NULL)
				my_putchar('\n');
		}
	free (pos);
	return (0);
}
