/*
** EPITECH PROJECT, 2017
** display_tools.c
** File description:
** Desc
*/

#include "my_ls.h"
#include "my.h"

char *truncate_my_str(char *str, int size)
{
	str[size - 1] = '\0';
	return (str);
}

void display_lflag(char *path)
{
	struct stat stbuf;

	if (stat(path, &stbuf) == -1) {
		perror("");
		throw_error(path);
		exit (EXIT_ERROR);
	}
	my_printf("%d %s %s %d %s ", stbuf.st_nlink,\
	getpwuid(stbuf.st_uid)->pw_name,\
	getpwuid(stbuf.st_gid)->pw_name, stbuf.st_size,\
	truncate_my_str(ctime(&stbuf.st_mtime) + 4, 13));
}
