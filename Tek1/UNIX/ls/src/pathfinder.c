/*
** EPITECH PROJECT, 2017
** pathfinder.c
** File description:
** Desc
*/

#include "my.h"
#include "my_ls.h"

void pathfinder(char **paths, char *flag, int l)
{
	struct stat stbuf;

	for (int i = 0; paths[i] != 0; ++i) {
		if (stat(paths[i], &stbuf) == -1) {
			throw_error(paths[i]);
			perror("");
			return;
		}
		if (my_strncmp(paths[i] + l, ".", 1) == 0)
			continue;
		if ((stbuf.st_mode & _S_IFDIR) != 0) {
			my_printf("\n%s:\n", paths[i]);
			dirwalk(paths[i], flag);
		}
		else
			continue;
	}
}
