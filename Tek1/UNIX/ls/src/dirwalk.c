/*
** EPITECH PROJECT, 2017
** dirwalk.c
** File description:
** ls project 2.0
*/

#include "my.h"
#include "my_ls.h"

static void display_rights(char *path)
{
	struct stat stbuf;

	if (stat(path, &stbuf) == -1) {
		throw_error(path);
		perror("");
		return;
	}
	((stbuf.st_mode & _S_IFDIR) != 0) ? my_putchar('d') : my_putchar('-');
	((stbuf.st_mode & S_IRUSR) != 0) ? my_putchar('r') : my_putchar('-');
	((stbuf.st_mode & S_IWUSR) != 0) ? my_putchar('w') : my_putchar('-');
	((stbuf.st_mode & S_IXUSR) != 0) ? my_putchar('x') : my_putchar('-');
	((stbuf.st_mode & S_IRGRP) != 0) ? my_putchar('r') : my_putchar('-');
	((stbuf.st_mode & S_IWGRP) != 0) ? my_putchar('w') : my_putchar('-');
	((stbuf.st_mode & S_IXGRP) != 0) ? my_putchar('x') : my_putchar('-');
	((stbuf.st_mode & S_IROTH) != 0) ? my_putchar('r') : my_putchar('-');
	((stbuf.st_mode & S_IWOTH) != 0) ? my_putchar('w') : my_putchar('-');
	((stbuf.st_mode & S_IXOTH) != 0) ? my_putchar('x') : my_putchar('-');
	my_putstr(". ");
}

static void dflag(char *path, char *flag)
{
	struct stat stbuf;

	if (stat(path, &stbuf) == -1) {
		perror("");
		throw_error(path);
		return;
	}
	if (my_strstr(flag, "i") != NULL)
		my_printf("%d ", stbuf.st_ino);
	if (my_strstr(flag, "s") != NULL)
		my_printf("%d ", stbuf.st_blocks / 2);
	if (my_strstr(flag, "l") != NULL) {
		display_rights(path);
		my_printf("%d %s %s %d %s ", stbuf.st_nlink,\
		getpwuid(stbuf.st_uid)->pw_name,\
		getpwuid(stbuf.st_gid)->pw_name, stbuf.st_size,\
		truncate_my_str(ctime(&stbuf.st_mtime) + 4, 13));
	}
	my_printf("%s\n", path);
}

static void display_content(char **paths, int l, char *flag)
{
	struct stat stbuf;

	if (my_strstr(flag, "l") != NULL)
		my_printf("Total %i\n", 0);
	for (int i = 0; paths[i] != 0; ++i) {
		if (stat(paths[i], &stbuf) == -1) {
			perror("");
			throw_error(paths[i]);
			exit (EXIT_ERROR);
		}
		if ((my_strncmp(paths[i] + l, ".", 1) == 0) &&\
		(my_strstr(flag, "a") == NULL))
			continue;
		if (my_strstr(flag, "i") != NULL)
			my_printf("%d ", stbuf.st_ino);
		if (my_strstr(flag, "s") != NULL)
			my_printf("%d ", stbuf.st_blocks / 2);
		if (my_strstr(flag, "l") != NULL) {
			display_rights(paths[i]);
			display_lflag(paths[i]);
		}
		my_printf("%s\n", paths[i] + l);
	}
}

void dirwalk(char *path, char *flag)
{
	char **paths;
	struct dirent *dp;
	DIR *dfd;
	int fls;
my_printf("%s\n", flag);
	if (my_strstr(flag, "d") != NULL) {
		dflag(path, flag);
		return;
	}
	fls = count_files(path);
	paths = malloc(sizeof(*paths) * fls + 1);
	dfd = openfold (path);
	for (fls = 0; (dp = readdir(dfd)) != NULL; ++fls) {
		paths[fls] = my_strdup(listing(dp->d_name, paths[fls], path));
	}
	paths[fls] = 0;
	display_content(paths, len_path(path), flag);
	if (my_strstr(flag, "R") != NULL)
		pathfinder(paths, flag, len_path(path));
	free (paths);
	free (flag);
}
