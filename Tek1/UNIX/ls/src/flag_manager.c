/*
** EPITECH PROJECT, 2017
** flag_manager.c
** File description:
** copy ls | flag manager
*/

#include "my.h"

char const *flags = "alRisdtr";

char *fdecryptor(int FID)
{
	int i = my_strlen(flags) - 1;
	int j = 0;
	char *c;

	if ((c = malloc (sizeof(c) * i)) == NULL) {
		perror("");
		exit (EXIT_ERROR);
	}
	if (FID == 0)
		return (c);
	while (flags[i]) {
		if ((FID - my_pow(2, i)) >= 0) {
			c[j] = flags[i];
			++j;
			FID -= my_pow(2, i);
		}
		--i;
	}
	return (c);
}

static int fencryptor(char c)
{
	int FID = 0;
	int i = 0;

	while (flags[i]) {
		if (c == flags[i]) {
			FID += my_pow(2, i);
		}
		++i;
	}
	return (FID);
}

static int fdescriptor(char *str)
{
	int FID = 0;

	while (*str) {
		FID += fencryptor(*str);
		str++;
	}
	return (FID);
}

int *flag_core(int size, char **values)
{
	int FID = 0;
	int pos_path = 1;
	int *pos;
	int i = 1;

	if ((pos = malloc(sizeof(int *) * (size + 1))) == NULL) {
		perror("");
		exit (EXIT_ERROR);
	}
	while (pos_path < size) {
		if (values[pos_path][0] == '-')
			FID += fdescriptor(values[pos_path] + 1);
		else {
			pos[i] = pos_path;
			i++;
		}
		pos_path++;
	}
	pos[i] = -1;
	pos[0] = FID;
	return (pos);
}
