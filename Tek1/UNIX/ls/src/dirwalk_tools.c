/*
** EPITECH PROJECT, 2017
** dirwalk_tools.c
** File description:
** ls 2.0 dirwalk_tools
*/

#include "my.h"
#include "my_ls.h"

DIR *openfold(char *path)
{
	DIR *dfd;

	if ((dfd = opendir(path)) == NULL) {
		throw_error(path);
		perror("");
		exit (EXIT_ERROR);
	}
	return (dfd);
}

int count_files(char *dir)
{
	struct dirent *dp;
	DIR *dfd;
	int fls;

	dfd = openfold(dir);
	fls = 0;
	while ((dp = readdir(dfd)) != NULL)
		++fls;
	closedir(dfd);
	dfd = openfold (dir);
	return (fls);
}

int len_path(char *name)
{
	int len_path;

	len_path = my_strlen(name) + 1;
	if (name[my_strlen(name) - 1] == '/')
		len_path = my_strlen(name);
	return (len_path);
}

char *listing(char *name, char *paths, char *dir)
{
	char *tmp;
	char *save;

	tmp = my_strdup(name);
	if ((save = malloc(sizeof(name) + sizeof(tmp) + 1)) == NULL) {
		perror("");
		exit (EXIT_ERROR);
	}
	save = my_strdup(dir);
	if (dir[my_strlen(dir) - 1] != '/')
		my_strcat(save, "/");
	my_strcat(save, tmp);
	paths = my_strdup(save);
	free (save);
	free (tmp);
	return (paths);
}
