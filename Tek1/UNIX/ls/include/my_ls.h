/*
** EPITECH PROJECT, 2017
** ls.c
** File description:
** Desc
*/

#ifndef LS_H_
#define LS_H_	1

/* missing defines */
#define MAX_PATH 1024
#define _S_IFDIR 0x4000

/* Include */
#include "ansi.h"
#include "dir_tools.h"
#include <string.h>
#include <fcntl.h>
#include <sys/types.h>
#include <sys/stat.h>
#include <sys/dir.h>
#include "dirent.h"
#include <pwd.h>
#include <grp.h>
#include <time.h>

/* Prototypes */
int *flag_core(int, char **);
void dirwalk(char *, char *);
void pathfinder(char **paths, char *flag, int l);
char *fdecryptor(int);
DIR *openfold(char *);
void display_lflag(char *path);

#endif
