/*
** EPITECH PROJECT, 2017
** macromaths
** File description:
** macromath
*/

#ifndef MY_MATHS_H_
#define MY_MATHS_H_	1

#define MIN(a, b)	((a) < (b) ? (a) : (b))
#define MAX(a, b)	((a) > (b) ? (a) : (b))
#define ABS(a)		((a) < 0 ? (a) * (-1) : (a))
#define PARITY(a)	((a % 2) == 0) ? EVEN : ODD

#endif
