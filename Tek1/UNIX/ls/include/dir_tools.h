/*
** EPITECH PROJECT, 2017
** dir_tools.h
** File description:
** header for tools
*/

#ifndef DIR_TOOLS_H_
#define DIR_TOOLS_H_	1

#include "my_ls.h"

int count_files(char *);
int len_path(char *);
char *listing(char *, char *, char *);
char *truncate_my_str(char *, int);

#endif
