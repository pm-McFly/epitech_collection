/*
** EPITECH PROJECT, 2017
** my_strcpy.c
** File description:
** Desc
*/

char *my_strcpy(char *dest, char const *src)
{
	char *saved = dest;

	while (*src) {
		*dest++ = *src++;
	}
	*dest = 0;
	return (saved);
}
