/*
** EPITECH PROJECT, 2017
** my_str_isnum
** File description:
** isnum
*/

int	info_char3 (char c);

int    my_str_isnum(char const *str)
{
	int i = 0;
	int alpha = 1;

	while (alpha == 1 && str[i] != '\0') {
		alpha -= info_char3(str[i]);
		i++;
	}
	return (alpha);
}

int	info_char3 (char c)
{
	if (c > 47 && c < 58)
		return (0);
	else
		return (1);
}
