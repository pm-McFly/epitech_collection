/*
** EPITECH PROJECT, 2017
** my_putstr
** File description:
** put_str
*/

#include "my.h"

int	my_putstr(char const *str)
{
	write(1, str, my_strlen(str));
	return (my_strlen(str));
}

void throw_error (char *error)
{
	write(2, error, my_strlen(error));
	exit (EXIT_ERROR);
}
