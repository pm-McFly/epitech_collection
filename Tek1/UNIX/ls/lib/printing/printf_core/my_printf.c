/*
** EPITECH PROJECT, 2017
** my_printf.c
** File description:
** My_printf
*/

#include "my.h"
#include <stdarg.h>

int my_printf(const char *format, ...)
{
	va_list ap;
	int printed = 0;
	char flag;

	va_start(ap, format);
	my_v_printf(format);
	for (int i = 0; format[i] != '\0'; ++i) {
		if (format[i] != '%')
			printed += my_putchar(format[i]);
		else {
			++i;
			flag = format[i];
			printed += parser(flag, ap);
			flag = 0;
		}
	}
	va_end(ap);
	return (printed);
}
