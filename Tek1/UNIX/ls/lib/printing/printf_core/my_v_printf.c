/*
** EPITECH PROJECT, 2017
** my_v_printf.c
** File description:
** Desc
*/

#include "my.h"

static int check(char a)
{
	int i = 0;
	char *flags = "%cCsSdipuboxX";

	while (flags[i]) {
		if (flags[i] == a && a != 0)
			return (1);
		++i;
	}
	exit (EXIT_ERROR);
}

void my_v_printf(char const *format)
{
	int i = 0;

	while (format[i]) {
		if (format[i] == '%') {
			++i;
			check(format[i]);
		}
		++i;
	}
}
