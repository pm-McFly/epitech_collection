/*
** EPITECH PROJECT, 2017
** flag_str1.c
** File description:
** Desc
*/

#include "my.h"

int my_print_char(va_list ap)
{
	return (my_putchar(va_arg(ap, int)));
}

int my_print_string(va_list ap)
{
	return (my_putstr(va_arg(ap, char *)));
}

static int printing_ostring(int nb_zeros, char hide)
{
	int printed = 0;

	printed += my_putchar('\\');
	for (int i = 0; i < nb_zeros; ++i)
		printed += my_putchar('0');
	printed += my_u_base_changer((unsigned int) hide, 8, 0);
	return (printed);
}

int my_print_ostring(va_list ap)
{
	char *str = va_arg(ap, char *);
	int printed = 0;

	for (int i = 0; str[i] != '\0'; ++i) {
		if (str[i] <= 7) {
			printed += printing_ostring(2, str[i]);
		}
		else if (str[i] <= 31) {
			printed += printing_ostring(1, str[i]);
		}
		else if (str[i] >= 127) {
			printed += printing_ostring(0, str[i]);
		}
		else
			printed += my_putchar(str[i]);
	}
	return (printed);
}
