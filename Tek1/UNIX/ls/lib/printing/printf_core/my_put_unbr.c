/*
** EPITECH PROJECT, 2017
** my_put_nbr
** File description:
** put_nbr
*/

#include "my.h"

int my_put_unbr(unsigned int nb)
{
	int size = 0;
	long div = 10;
	char c;
	unsigned int tmp;

	while (nb / div != 0) {
		div = div * 10;
	}

	div = div / 10;
	while (div > 0) {
		tmp = nb / div;
		nb = nb % div;
		c = tmp + 48;
		size += my_putchar(c);
		div = div / 10;
	}
	return (size);
}
