/*
** EPITECH PROJECT, 2018
** t_clean_str.c
** File description:
** test clean_str()
*/

#include "../include/tests.h"
#include "../../include/my.h"

Test(Clean_str, empty)
{
	char *empty = my_strdup("\0");

	empty = clean_str(empty);
	cr_assert_str_eq(empty, "\0");
	free(empty);
}

Test(Clean_str, spaces)
{
	char *one = my_strdup(" ");
	char *oneA = my_strdup(" A");
	char *oneAone = my_strdup(" A ");
	char *Aone = my_strdup("A ");

	one = clean_str(one);
	oneA = clean_str(oneA);
	oneAone = clean_str(oneAone);
	Aone = clean_str(Aone);
	cr_assert_str_eq(one, "\0");
	cr_assert_str_eq(oneA, "A");
	cr_assert_str_eq(oneAone, "A");
	cr_assert_str_eq(Aone, "A");
	free(one);
	free(oneA);
	free(oneAone);
	free(Aone);
}

Test(Clean_str, tabs)
{
	char *one = my_strdup("\t");
	char *oneA = my_strdup("\tA");
	char *oneAone = my_strdup("\tA\t");
	char *Aone = my_strdup("A\t");

	one = clean_str(one);
	oneA = clean_str(oneA);
	oneAone = clean_str(oneAone);
	Aone = clean_str(Aone);
	cr_assert_str_eq(one, "\0");
	cr_assert_str_eq(oneA, "A");
	cr_assert_str_eq(oneAone, "A");
	cr_assert_str_eq(Aone, "A");
	free(one);
	free(oneA);
	free(oneAone);
	free(Aone);
}

Test(Clean_str, spaces_tabs)
{
	char *mixed = my_strdup("	 	    		  ");
	char *mixedA = my_strdup(" A");
	char *mixedAmixed = my_strdup(" A ");
	char *Amixed = my_strdup("A ");

	mixed = clean_str(mixed);
	mixedA = clean_str(mixedA);
	mixedAmixed = clean_str(mixedAmixed);
	Amixed = clean_str(Amixed);
	cr_assert_str_eq(mixed, "\0");
	cr_assert_str_eq(mixedA, "A");
	cr_assert_str_eq(mixedAmixed, "A");
	cr_assert_str_eq(Amixed, "A");
	free(mixed);
	free(mixedA);
	free(mixedAmixed);
	free(Amixed);
}

Test(Clean_str, word_mixed_word_more)
{
	char *test = my_strdup("	 	  A  	 	 B 	    	  	C ");
	char *test2 = my_strdup("  ls	-la	 -R  |	grep	PWD	");

	test = clean_str(test);
	test2 = clean_str(test2);
	cr_assert_str_eq(test2, "ls -la -R | grep PWD");
	cr_assert_str_eq(test, "A B C");
	free(test);
}
