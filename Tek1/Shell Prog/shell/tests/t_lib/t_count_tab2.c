/*
** EPITECH PROJECT, 2018
** t_count_tab2.c
** File description:
** tests for my_count_tab2()
*/

#include "../../include/my.h"
#include "../include/tests.h"

Test(my_count_tab2, null)
{
	char **tab = NULL;

	cr_assert_eq(my_count_tab2(tab), 0);
}

Test(my_count_tab2, diff_sizes)
{
	char *tab1[] = {
		"hello",
		"troll",
		NULL,
	};
	char *tab2[] = {
		"ls",
		NULL,
	};

	cr_assert_eq(my_count_tab2(tab1), 2);
	cr_assert_eq(my_count_tab2(tab2), 1);
}
