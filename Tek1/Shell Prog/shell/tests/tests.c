/*
** EPITECH PROJECT, 2017
** test.c
** File description:
** unit_tests
*/

#include <unistd.h>
#include "./include/tests.h"

void	redirect_all_std(void)
{
	cr_redirect_stdout();
	cr_redirect_stderr();
}

Test(General, output, .init = redirect_all_std)
{
	write(STDOUT_FILENO, "hello world\n", 12);
	write(STDERR_FILENO, "hello world\n", 12);
	cr_assert_stdout_eq_str("hello world\n");
	cr_assert_stderr_eq_str("hello world\n");
}
