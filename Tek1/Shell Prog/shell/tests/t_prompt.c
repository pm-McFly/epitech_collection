/*
** EPITECH PROJECT, 2017
** test.c
** File description:
** unit_tests
*/

#include "./include/tests.h"
#include "../include/prompt.h"
#include "../include/my.h"

Test(Global, prompt_display, .init = redirect_all_std)
{
	char *prompt = my_stradd("\n", getcwd(NULL, 0));

	prompt = my_stradd(prompt, "\n>> : ");
	display_prompt();
	cr_assert_stdout_eq_str(prompt);
}
