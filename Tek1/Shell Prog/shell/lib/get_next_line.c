/*
** EPITECH PROJECT, 2018
** get_next_line
** File description:
** function
*/

#include "get_next_line.h"

char	*get_next_line(int fd)
{
	char *line;
	int i = 0;
	int size = 0;

	if (fd == -1)
		return (NULL);
	if ((line = malloc(1)) == NULL)
		return (NULL);
	if ((size = scroll_buff(fd, &line[i])) == 0)
		return (NULL);
	while (line[i] != '\0') {
		line = my_get_line(line, i);
		i = i + 1;
		size = scroll_buff(fd, &line[i]);
	}
	return (line);
}

int	scroll_buff(int fd, char *line)
{
	static char buff[READ_SIZE];
	static int i = 0;
	static int size = READ_SIZE;

	if (i == 0 || i >= size) {
		i = 0;
		if ((size = read(fd, buff, READ_SIZE)) < 0)
			return (0);
	}
	if (size == 0)
		*line = '\0';
	else {
		if (buff[i] == '\n' || buff[i] == '\0')
			*line = '\0';
		else
			*line = buff[i];
		i = i + 1;
	}
	return (size);
}

char	*my_get_line(char *line, int size)
{
	int i = 0;
	char *res_line;

	if ((res_line = malloc(size + 2)) == NULL)
		return (NULL);
	while (i <= size) {
		res_line[i] = line[i];
		i = i + 1;
	}
	free(line);
	return (res_line);
}
