/*
** EPITECH PROJECT, 2018
** my_putchar.c
** File description:
** print on STDOUT_FILENO the given char
*/

#include <unistd.h>
#include "../include/my.h"

ssize_t	my_putstr(const char *str)
{
	return (write(STDOUT_FILENO, str, my_strlen(str)));
}

ssize_t	my_puterr(const char *str)
{
	return (write(STDERR_FILENO, str, my_strlen(str)));
}
