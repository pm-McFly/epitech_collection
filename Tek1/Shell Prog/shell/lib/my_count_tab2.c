/*
** EPITECH PROJECT, 2018
** my_count_tab2.c
** File description:
** count the number of lines in the given char **
*/

#include <stddef.h>

int	my_count_tab2(char **tab)
{
	int i = 0;

	if (!tab)
		return (0);
	while (tab[i] != NULL)
		++i;
	return (i);
}
