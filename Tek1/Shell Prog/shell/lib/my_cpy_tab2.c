/*
** EPITECH PROJECT, 2018
** my_cpy_tab2.c
** File description:
** copy the given tab2_cpy
*/

#include <stdlib.h>
#include "../include/my.h"

static	unsigned int count_lines(char * const *tab)
{
	unsigned int y = 0;

	while (tab[y])
		++y;
	return (y);
}

char	**my_cpy_tab2(char * const *tab)
{
	char **ntab = NULL;
	unsigned int i = 0;
	unsigned int y = 0;

	y = count_lines(tab);
	if (!(ntab = malloc(sizeof(char *) * (y + 1))))
		return (NULL);
	while (tab[i]) {
		ntab[i] = my_strdup(tab[i]);
		++i;
	}
	ntab[i] = NULL;
	return (ntab);
}
