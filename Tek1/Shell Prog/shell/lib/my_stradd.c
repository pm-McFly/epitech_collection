/*
** EPITECH PROJECT, 2018
** my_stradd.c
** File description:
** return a new char * who constains s2 after s1
*/

#include <stdlib.h>
#include "../include/my.h"

char	*my_stradd(char const *s1, char const *s2)
{
	size_t l_s1;
	size_t l_s2;
	char *s3;
	char *svg;

	l_s1 = my_strlen(s1);
	l_s2 = my_strlen(s2);
	if ((s3 = malloc(sizeof(char) * (l_s1 + l_s2 + 1))) == NULL)
		return (NULL);
	svg = s3;
	while (*s1 && (*svg++ = *s1++));
	while (*s2 && (*svg++ = *s2++));
	*svg = 0;
	return (s3);
}
