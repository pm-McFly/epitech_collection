/*
** EPITECH PROJECT, 2018
** my_free_tab2.c
** File description:
** wipe the given tab2
*/

#include <stdlib.h>
#include "../include/my.h"

void	my_free_tab2(char *tab[])
{
	if (!tab)
		return;
	while (*tab) {
		free(*tab++);
	}
}
