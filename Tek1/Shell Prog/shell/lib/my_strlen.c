/*
** EPITECH PROJECT, 2017
** my_strlen
** File description:
** strlen
*/

#include <stddef.h>

size_t my_strlen(const char *str)
{
	size_t i = 0;

	while (str[i] != 0)
		i++;
	return (i);
}
