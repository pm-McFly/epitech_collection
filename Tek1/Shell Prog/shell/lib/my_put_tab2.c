/*
** EPITECH PROJECT, 2017
** my_show_word_array
** File description:
** show word array
*/

#include <my.h>

int	my_put_tab2(char * const *tab)
{
	int n = 0;
	int x = 0;

	if (!tab)
		return (-1);
	while (tab[x]) {
		n += my_putstr(tab[x]);
		n += my_putchar('\n');
		++x;
	}
	return (n);
}
