/*
** EPITECH PROJECT, 2018
** my_putchar.c
** File description:
** print on STDOUT_FILENO the given char
*/

#include <unistd.h>

ssize_t	my_putchar(const char c)
{
	return (write(STDOUT_FILENO, &c, 1));
}
