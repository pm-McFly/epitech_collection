/*
** EPITECH PROJECT, 2017
** clean_str.c
** File description:
** remove useless chars in the given string
*/

#include <stddef.h>
#include "../include/my.h"
#include <stdio.h>

static char *roll_back(char *s, int i)
{
	while (s[i + 1]) {
		s[i] = s[i + 1];
		++i;
	}
	s[i] = '\0';
	return (s);
}

static int	is_allowed(char c)
{
	return (c > ' ' && c < 127);
}

static char	*remove_tabs(char *s)
{
	int i = 0;

	while (s[i]) {
		if (is_allowed(s[i]) || s[i] == ' ')
			++i;
		else
			s[i] = ' ';
	}
	return (s);
}

static char	*let_only_one_space(char *s)
{
	int i = 0;

	while (s[i]) {
		if (s[i] != ' ' || (s[i] == ' ' && s[i + 1] != ' '))
			++i;
		else
			s = roll_back(s, i);
	}
	return (s);
}

char	*clean_str(char *s)
{
	int i = 0;

	if (!s)
		return (NULL);
	s = remove_tabs(s);
	s = let_only_one_space(s);
	if (s[0] == ' ')
		s = roll_back(s, 0);
	i = my_strlen(s) - 1;
	i <= 0 ? i = 0 : 0;
	if (s[i] == ' ')
		s[i] = '\0';
	return (s);
}
