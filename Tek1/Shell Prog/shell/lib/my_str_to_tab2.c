/*
** EPITECH PROJECT, 2018
** my_str_to_tab2.c
** File description:
** splits a string into words list
*/

#include <stdlib.h>
#include "../include/my.h"

int count_word(char const *s, char const c)
{
	int i = 0;
	int n = 0;

	if (!s)
		return (0);
	for (i = 0; s[i]; ++i) {
		if (s[i] == c)
			++n;
	}
	++n;
	return (n);
}

char **my_str_to_tab2(char *s, char const c)
{
	int i = 0;
	char **tab;
	char *p = s;
	int cw = count_word(s, c);
	int w = 0;

	if (!s)
		return (NULL);
	if ((tab = malloc(sizeof(char *) * (cw + 2))) == NULL)
		return (NULL);
	for (i = 0; p[i]; ++i) {
		if (p[i] == c) {
			p[i] = '\0';
			tab[w] = my_strdup(s);
			++w;
			s = (p + (i + 1));
		}
	}
	tab[w] = my_strdup(s);
	tab[cw] = NULL;
	return (tab);
}
