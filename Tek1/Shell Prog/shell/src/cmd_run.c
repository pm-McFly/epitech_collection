/*
** EPITECH PROJECT, 2018
** cmd_run.c
** File description:
** seek & run the cmd contain in the given struc shell_t
*/

#include <signal.h>
#include <sys/types.h>
#include <sys/wait.h>
#include <stdio.h>
#include <string.h>
#include <stdlib.h>
#include "../include/my.h"
#include "../include/cmd.h"

bool	cmd_exit(shell_t *opt)
{
	if (opt->exit == true) {
		my_puterr(INPT_ERR);
		free(opt->line);
		return (true);
	}
	if (!(my_strncmp(*(opt->argv), "exit", my_strlen(*opt->argv)))) {
		if (opt->argv[1])
			opt->r_value = my_getnbr(opt->argv[1]);
		my_free_tab2(opt->argv);
		return (true);
	}
	return (false);
}

static bool	seeker(shell_t *opt)
{
	opt->line = cmd_search(&opt->env, opt->argv[0], opt->argv);
	if (opt->line && !my_strncmp(opt->line, "bltns", my_strlen(opt->line)))
		return (false);
	if (!opt->line) {
		opt->r_value = 127;
		opt->err_msg = my_stradd(opt->argv[0], ERR_CMD);
		my_puterr(opt->err_msg);
		free(opt->err_msg);
		return (false);
	}
	return (true);
}

bool	cmd_run(shell_t *opt)
{
	pid_t cpid;

	if (!seeker(opt))
		return (false);
	cpid = fork();
	if (cpid == -1) {
		return (false);
	} else if (cpid == 0) {
		if (execve(opt->line, opt->argv, opt->env) == -1) {
			perror(NULL);
			exit(-1);
		}
	} else {
		wait(&opt->r_value);
		if (opt->r_value != -1 && WIFSIGNALED(opt->r_value)) {
			my_putstr(strsignal(WTERMSIG(opt->r_value)));
			WCOREDUMP(opt->r_value) ? my_putstr(C_DUMP) : 0;
			my_putchar('\n');
		}
	}
	return (true);
}
