/*
** EPITECH PROJECT, 2018
** cmd_line_input.c
** File description:
** functions for command line input
*/

#include "../include/cmd.h"
#include "../include/msh.h"
#include "../include/get_next_line.h"
#include "../include/my.h"

bool get_cmd_line(shell_t *opt)
{
	if (!(opt->line = get_next_line(STDIN_FILENO)))
		opt->exit = true;
	else
		opt->exit = false;
	opt->line = clean_str(opt->line);
	opt->argv = my_str_to_tab2(opt->line, ' ');
	if (!opt->line)
		return (false);
	if (*opt->line)
		free(opt->line);
	return (true);
}
