/*
** EPITECH PROJECT, 2018
** cmd_search.c
** File description:
** check if the the cmd is existing
*/

#include <unistd.h>
#include <stdlib.h>
#include "../include/my.h"
#include "../include/env.h"

const char	*b_list[] = {
	"cd",
	"env",
	"setenv",
	"unsetenv",
	NULL,
};

static char	**launch_builtins(char *argv[], char *env[], int i)
{
	char **test;
	if (i == -1)
		return (NULL);
	switch (i) {
	case 0:
		return (my_cpy_tab2(b_cd(argv, env)));
		break;
	case 1:
		return (my_cpy_tab2(b_env(argv, env)));
		break;
	case 2:
		test = my_cpy_tab2(b_setenv(argv, env));
		return (test);
		break;
	case 3:
		return (my_cpy_tab2(b_unsetenv(argv, env)));
		break;
	default:
		return (NULL);
	}
	return (NULL);
}

static int	find_builtins(char *cmd)
{
	int i = 0;

	while (b_list[i]) {
		if (!my_strncmp(cmd, b_list[i], my_strlen(cmd)))
			return (i);
		++i;
	}
	return (-1);
}

static char	**chk_builtins(char *cmd, char *argv[], char *env[])
{
	char **test;

	test = launch_builtins(argv, env, find_builtins(cmd));
	return (test);
}

static char	*cmd_search_env(char *env[], char *cmd)
{
	int i = 0;
	char *ok = NULL;
	char *path;
	char **list;

	path = get_env(env, "PATH");
	list = my_str_to_tab2(path, ':');
	free(path);
	i = my_count_tab2(list) - 1;
	for (i = i; i >= 0 && ok == NULL; --i) {
		path = my_stradd(list[i], "/");
		path = my_stradd(path, cmd);
		if (access(path, X_OK) != -1)
			ok = my_strdup(path);
		free(path);
	}
	my_free_tab2(list);
	return (ok);
}

char	*cmd_search(char **env[], char *cmd, char *argv[])
{
	char *path;
	char **test = NULL;

	if (access(cmd, X_OK) != -1)
		return (cmd);
	if ((test = chk_builtins(cmd, argv, *env))) {
		*env = my_cpy_tab2(test);
		my_free_tab2(test);
		return ("bltns");
	} else
		path = cmd_search_env(*env, cmd);
	return (path);
}
