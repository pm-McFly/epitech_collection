/*
** EPITECH PROJECT, 2018
** b_unsetenv.c
** File description:
** reproduces the behaviour of a part of env syscall
*/

#include <stdbool.h>
#include "../../include/env.h"
#include "../../include/my.h"

char	**b_unsetenv(char *argv[], char *env[] __attribute__((unused)))
{
	int i = 1;

	if (!argv[1]) {
		my_puterr("unsetenv: Too few arguments.\n");
		return (env);
	}
	while (argv[i]) {
		env = e_remove(env, argv[i]);
		++i;
	}
	return (env);
}
