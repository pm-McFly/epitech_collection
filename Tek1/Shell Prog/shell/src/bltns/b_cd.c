/*
** EPITECH PROJECT, 2018
** b_cd.c
** File description:
** reproduces the behaviour of cd syscall
*/

#include <stdlib.h>
#include <unistd.h>
#include <stdio.h>
#include <stdbool.h>
#include "../../include/my.h"
#include "../../include/env.h"

static char	*get_from_args(char *argv[], char *env[])
{
	char *cur = get_env(env, "PWD");
	char *path;

	if (!argv[1]) {
		path = my_stradd(get_env(env, "HOME"), "/");
	} else {
		if (cur[my_strlen(cur)] != '/')
			path = my_stradd(cur, "/");
		else
			path = my_strdup(cur);
		path = my_stradd(path, argv[1]);
	}
	free (cur);
	return (path);
}

char	**b_cd(char *argv[], char *env[])
{
	int i = 0;
	char *path;

	path = get_from_args(argv, env);
	i = chdir(path);
	if (i == -1)
		perror(argv[1]);
	else
		env = e_replace(env, "PWD", getcwd(NULL, 0));
	free (path);
	return (env);
}
