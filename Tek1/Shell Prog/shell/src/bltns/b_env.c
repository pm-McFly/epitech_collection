/*
** EPITECH PROJECT, 2018
** b_env.c
** File description:
** reproduces the behaviour of env syscall
*/

#include <stdbool.h>
#include "../../include/my.h"

char	**b_env(char *argv[] __attribute__((unused)), char *env[])
{
	my_put_tab2(env);
	return (env);
}
