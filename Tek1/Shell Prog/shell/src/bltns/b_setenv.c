/*
** EPITECH PROJECT, 2018
** b_setenv.c
** File description:
** reproduces the behaviour of a part of env syscall
*/

#include <stdlib.h>
#include <stdbool.h>
#include "../../include/my.h"
#include "../../include/env.h"

char	**b_setenv(char *argv[], char *env[])
{
	char *value = "\0";

	if (my_count_tab2(argv) > 3)
		my_puterr("setenv: Too many arguments.\n");
	if (!argv[1])
		return (b_env(argv, env));
	if (argv[2])
		value = my_strdup(argv[2]);
	env = e_replace(env, argv[1], value);
	if (*value)
		free (value);
	return (env);
}
