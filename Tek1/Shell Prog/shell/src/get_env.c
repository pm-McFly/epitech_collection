/*
** EPITECH PROJECT, 2018
** get_env.c
** File description:
** get the content of the wanted env var
*/

#include "../include/my.h"

char	*get_env(char *env[], char const *var)
{
	size_t len = my_strlen(var);
	int i = 0;

	while (env[i] && (my_strncmp(env[i], var, len) != 0))
		++i;
	if (i == my_count_tab2(env))
		return (NULL);
	return (my_strdup(&env[i][len + 1]));
}
