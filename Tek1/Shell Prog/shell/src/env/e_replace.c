/*
** EPITECH PROJECT, 2018
** e_replace.c
** File description:
** replace the conetent of a variable in env
*/

#include <stdlib.h>
#include "../../include/my.h"
#include "../../include/env.h"

char	**e_replace(char *env[], char *name, char *value)
{
	char *val = "\0";
	int i = e_find(env, name);

	if (i == -1) {
		return (e_add(env, name, value));
	}
	val = my_stradd(val, name);
	val = my_stradd(val, "=");
	val = my_stradd(val, value);
	free(env[i]);
	env[i] = my_strdup(val);
	free(val);
	return (env);
}
