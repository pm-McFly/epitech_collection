/*
** EPITECH PROJECT, 2018
** e_remove.c
** File description:
** remove the name from env variable
*/

#include <stddef.h>
#include "../../include/env.h"

char	**e_remove(char *env[], char *name)
{
	char **envp;
	int i = e_find(env, name);

	if (i == -1)
		return (env);
	for (envp = (env + i); *(envp + 1) != NULL; ++envp) {
		*envp = *(envp + 1);
	}
	*envp = NULL;
	return (env);
}
