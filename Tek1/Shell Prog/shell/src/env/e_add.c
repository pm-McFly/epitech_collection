/*
** EPITECH PROJECT, 2018
** e_add.c
** File description:
** add name with it's value to env
*/

#include <stdlib.h>
#include "../../include/my.h"

char	**e_add(char *env[], char *name, char *value)
{
	int i = my_count_tab2(env);
	char **new;
	char *val = "\0";

	val = my_stradd(val, name);
	val = my_stradd(val, "=");
	val = my_stradd(val, value);
	new = malloc(sizeof(char *) * (i + 2));
	i = 0;
	while (env[i]) {
		new[i] = my_strdup(env[i]);
		++i;
	}
	new[i] = my_strdup(val);
	new[++i] = NULL;
	free (val);
	return (new);
}
