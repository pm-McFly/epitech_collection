/*
** EPITECH PROJECT, 2018
** e_find.c
** File description:
** find the idx of the name in the env variable
*/

#include <stdio.h>
#include "../../include/my.h"

unsigned int	e_find(char *env[], char *name)
{
	int len = my_strlen(name);
	int lt = my_count_tab2(env);
	int i = 0;

	while (i < lt && (env[i][len] != '=' || \
		my_strncmp(env[i], name, len) != 0)) {
		++i;
	}
	if (i == lt)
		return (-1);
	return (i);
}
