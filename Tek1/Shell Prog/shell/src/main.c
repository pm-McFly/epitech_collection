/*
** EPITECH PROJECT, 2018
** main.c
** File description:
** main for minishell
*/

#include <signal.h>
#include "../include/cmd.h"
#include "../include/my.h"
#include "../include/prompt.h"
#include "../include/get_next_line.h"
#include "../include/msh.h"

bool r_sig = false;

static void	crtl_c_hdlr(int sig __attribute__((unused)))
{
	my_putchar('\n');
	r_sig = true;
}

int	main(int ac __attribute__((unused)), char *av[]\
		__attribute__((unused)), char *envp[])
{
	shell_t opt;

	signal(SIGINT, crtl_c_hdlr);
	opt.r_value = 0;
	if (*envp == NULL)
		return (84);
	if (*(opt.env = my_cpy_tab2(envp)) == NULL)
		return (84);
	while (true) {
		display_prompt();
		get_cmd_line(&opt);
		if (r_sig == true) {
			r_sig = false;
			continue;
		}
		if (cmd_exit(&opt))
			break;
		cmd_run(&opt);
	}
	my_free_tab2(opt.env);
	return (opt.r_value);
}
