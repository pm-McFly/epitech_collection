/*
** EPITECH PROJECT, 2018
** prompt.c
** File description:
** function to display the prompt
*/

#include <unistd.h>
#include "../include/my.h"

void	display_prompt(void)
{
	my_putchar('\n');
	my_putstr(getcwd(NULL, 0));
	my_putstr("\n>> : ");
}
