/*
** EPITECH PROJECT, 2018
** cmd.h
** File description:
** header for command line input
*/

#pragma once

#include <stdbool.h>
#include "../include/msh.h"

#ifndef INPT_ERR /* INPT_ERR */
#	define INPT_ERR "exit\n"
#endif /* !INPT_ERR */

#ifndef C_DUMP /* C_DUMP */
#	define C_DUMP " (core dumped)"
#endif /* !C_DUMP */

bool	get_cmd_line(shell_t *opt);
