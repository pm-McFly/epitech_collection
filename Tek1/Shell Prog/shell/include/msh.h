/*
** EPITECH PROJECT, 2018
** msh.h
** File description:
** header for mysh
*/

#pragma once

#include <stdbool.h>

#define	ERR_CMD ": Command not found.\n"

typedef struct shell_s {
	char *line;
	char *err_msg;
	char **argv;
	char **env;
	int r_value;
	bool exit;
} shell_t;

typedef struct bltin_s {
	char *cmd;
	bool (*fctn)(char **, char **);
} bltin_t;
