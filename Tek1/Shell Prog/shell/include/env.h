/*
** EPIETCH PROJECT, 2018
** env.h
** File description:
** prototypes for env edition
*/

#pragma once

#include <stdbool.h>

char	**b_cd(char *argv[], char *env[]);
char	**b_env(char *argv[], char *env[]);
char	**b_setenv(char *argv[], char *env[]);
char	**b_unsetenv(char *argv[], char *env[]);
char	**e_remove(char *env[], char *name);
unsigned int	e_find(char *env[], char *name);
char	**e_replace(char *env[], char *name, char *value);
char	**e_add(char *env[], char *name, char *value);
