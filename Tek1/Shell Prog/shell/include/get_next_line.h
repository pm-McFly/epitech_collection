/*
** EPITECH PROJECT, 2018
** get_next_line
** File description:
** .h
*/

#ifndef GET_NEXT_LINE_ /* GET_NEXT_LINE_ */
#define GET_NEXT_LINE_

#include <stdlib.h>
#include <unistd.h>
#include <fcntl.h>

#ifndef READ_SIZE /* READ_SIZE */
#	define READ_SIZE 1
#endif /* !READ_SIZE */

char	*get_next_line(int fd);
char	*my_get_line(char *line, int size);
int	scroll_buff(int fd, char *line);
#endif /* !GET_NEXT_LINE_ */
