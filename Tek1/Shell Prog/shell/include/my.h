/*
** EPITECH PROJECT, 2018
** my.h
** File description:
** header for my library
*/

#pragma once

#include "./msh.h"
#include <stdbool.h>
#include <unistd.h>

ssize_t	my_putchar(const char c);
ssize_t	my_putstr(const char *str);
ssize_t	my_puterr(const char *str);
size_t	my_strlen(const char *str);
int	my_put_tab2(char * const *tab);
char	**my_cpy_tab2(char * const *tab);
char	*my_strdup(const char *s);
char 	*my_strcpy(char *dest, char const *src);
void	my_free_tab2(char *tab[]);
char	*clean_str(char *s);
char	**my_str_to_tab2(char *s, char const c);
int	my_strncmp(char const *s1, char const * s2, int nb);
char	*get_env(char *env[], char const *var);
int	count_word(char const *s, char const c);
bool	cmd_run(shell_t *opt);
char	*cmd_search(char **env[], char *cmd, char *argv[]);
int	my_count_tab2(char **tab);
char	*my_stradd(char const *s1, char const *s2);
bool	cmd_exit(shell_t *opt);
int	my_getnbr(char *s);
