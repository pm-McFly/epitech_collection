/*
** EPITECH PROJECT, 2017
** square.c
** File description:
** Desc
*/

#include "bsq.h"

static void display_final(grid_t *grid)
{
	int size = (grid->x * (grid->y));
	int z = 0;

	for (int i = 0; i != size; ++i) {
		if (z == grid->x) {
			my_putchar('\n');
			z = 0;
		}
		if (grid->data[i] == 0)
			my_putchar('o');
		else if (grid->data[i] == -5)
			my_putchar('x');
		else
			my_putchar('.');
		++z;
	}
	my_putchar('\n');
}

static max_t *get_max(grid_t *grid)
{
	max_t *max = malloc(sizeof(max_t));
	int size = (grid->x * grid->y);
	int x = 0;
	int y = 0;
	int i = D_POS(y, x, grid->x);

	max->value = -2;
	while (i <= size) {
		if (x > (grid->x - 1)) {
			x = 1;
			++y;
		}
		if (grid->data[i] > max->value){
			max->value = grid->data[i];
			max->x = x;
			max->y = y;
		}
		++x;
		i = D_POS(y, x, grid->x);
	}
	return (max);
}

grid_t *place_x(grid_t *grid)
{
	max_t *max = get_max(grid);

	for (int i = 0; i < max->value; ++i) {
		for (int j = 0; j < max->value; ++j)
			grid->data[D_POS((max->y - i),\
			(max->x - j), grid->x)] = -5;
	}
	free (max);
	display_final(grid);
	return (grid);
}
