/*
** EPITECH PROJECT, 2017
** read.c
** File description:
** Desc
*/

#include "bsq.h"

static int get_case(char c)
{
	if (c == 'o')
		return (0);
	else if (c == '.')
		return (1);
	else if (c == '\n')
		return (-1);
	return (-2);
}

static int x_compute(char *grid)
{
	int x = 0;

	for (int i = 0; grid[i] != '\n'; ++i) {
		++x;
	}
	return (x);
}

static grid_t *stock_grid(char *grid)
{
	grid_t *final;
	int z = 0;
	int i = 0;
	int size;

	final = malloc(sizeof(grid_t));
	final->y = my_getnbr(grid);
	while (*grid++ != '\n');
	final->x = x_compute(grid);
	final->data = malloc(sizeof(int) * (final->x * (final->y)));
	size = final->x * (final->y);
	if (size >= 36000000)
		exit (BAD_RETURN);
	while (grid[i] != '\0' && z != size) {
		if (grid[i] == '\n')
			i += 1;
		else {
			final->data[z] = get_case(grid[i]);
			++i;
			++z;
		}
		if (final->data[z] == -2)
			exit (BAD_RETURN);
	}
	return (final);
}

grid_t *get_grid(char *file_path)
{
	int fd = 0;
	int size = 0;
	char *buffer;
	struct stat fst;

	fst = get_stat(fst, file_path);
	fd = open_file(fd, file_path);
	size = fst.st_size + 1;
	buffer = malloc(sizeof(char) * size);
	size = read(fd, buffer, size);
	buffer[size] = '\0';
	return (stock_grid(buffer));
}
