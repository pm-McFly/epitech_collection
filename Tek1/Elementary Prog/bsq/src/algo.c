/*
** EPITECH PROJECT, 2017
** algo.c
** File description:
** Desc
*/

#include "bsq.h"
#include "my_maths.h"

grid_t *compute_grid(grid_t *grid)
{
	int size = (grid->x * grid->y);
	int x = 1;
	int y = 1;
	int i = D_POS(y, x, grid->x);

	while (i <= size) {
		if (x > (grid->x - 1)) {
			x = 1;
			++y;
		}
		else if (grid->data[i] == 0)
			++x;
		else {
			grid->data[i] = ((MIN(\
				grid->data[D_POS(y, (x - 1), grid->x)],\
				MIN(grid->data[D_POS((y - 1), (x - 1), grid->x)],\
				grid->data[D_POS((y - 1), x, grid->x)]))) + 1);
			++x;
		}
		i = D_POS(y, x, grid->x);
	}
	return (grid);
}
