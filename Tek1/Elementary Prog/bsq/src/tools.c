/*
** EPITECH PROJECT, 2017
** tools.c
** File description:
** Desc
*/

#include "bsq.h"

struct stat get_stat(struct stat fst, char *file_path)
{
	if (stat(file_path, &fst) == -1) {
		throw_error("FILE NOT FOUND\n");
		exit (STAT_IMPOSSIBLE);
	}
	else
		return (fst);
}

int open_file(int fd, char *file_path)
{
	if ((fd = open(file_path, O_RDONLY)) == -1) {
		throw_error("FILE CAN'T BE READED\n");
		exit (STAT_IMPOSSIBLE);
	}
	else
		return (fd);
}
