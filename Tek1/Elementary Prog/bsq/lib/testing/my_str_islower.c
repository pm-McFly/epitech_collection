/*
** EPITECH PROJECT, 2017
** my_str_islower
** File description:
** is_lower
*/

int	info_char12 (char c);

int	my_str_islower(char const *str)
{
	int i = 0;
	int alpha = 1;

	while (alpha == 1 && str[i] != '\0') {
		alpha -= info_char12(str[i]);
		i++;
	}
	return (alpha);
}

int	info_char12 (char c)
{
	if (c > 96 && c < 123)
		return (0);
	if (c > 63 && c < 91)
		return (1);
	else
		return (1);
}
