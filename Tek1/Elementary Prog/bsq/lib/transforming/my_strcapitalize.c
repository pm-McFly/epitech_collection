/*
** EPITECH PROJECT, 2017
** my_strcapitalize
** File description:
** capitalize
*/

int	info_char1 (char c);

char	*my_strcapitalize (char *str)
{
	int i = 0;
	int prec;

	while (str[i] != '\0') {
		prec = info_char1 (str[i - 1]);
		if (info_char1(str[i]) == 1 && prec < 0 && prec != -42)
			str[i] -= 32;
		if (info_char1(str[i]) == 2 && (prec > 0 || prec == -42))
			str[i] += 32;
		i++;
	}
	return (str);
}

int	info_char1 (char c)
{
	if (c > 96 && c < 123)
		return (1);
	if (c > 64 && c < 91)
		return (2);
	if (c > 47 && c < 58)
		return (-42);
	else
		return (-1);
}
