/*
** EPITECH PROJECT, 2017
** my_sort_int_array
** File description:
** sortintarray
*/

void	my_sort_int_array(int *array, int size)
{
	int x = 0;
	int y = 0;
	int temp = 0;

	while (x < size) {
		y = x;
		while (array[y] > array[y+1] && y < size - 1) {
			temp = array[y+1];
			array[y+1] = array[y];
			array[y] = temp;
			y++;
		}
		if (array[x] < array[x+1])
			x++;
		y = x;
	}
}
