/*
** EPITECH PROJECT, 2017
** get_next_line.c
** File description:
** clone function getline
*/

#include "get_next_line.h"

static char *save_end_buff(char *s)
{
	int lf = 0;
	int j = 0;
	int i = 0;
	char *end = "";

	while (s[i]) {
		if (s[i] == '\n' && lf == 0) {
			s[i] = '\0';
			j = i;
			lf = 1;
		}
		++i;
	}
	end = malloc (sizeof (char) * (i - j));
	i = 0;
	++j;
	while ((end[i++] = s[j++]));
	return (end);
}

static char *my_stradd(char *dest, char *src)
{
	char *out;
	int i = 0;
	int j = 0;

	for (i = 0; src[i]; ++i);
	for (j = 0; dest[j]; ++j);
	out = malloc (sizeof (char) * (i + j));
	i = 0;
	j = 0;
	while ((out[i++] = dest[j++]));
	--i;
	j = 0;
	while ((out[i++] = src[j++]));
	free (dest);
	free (src);
	return (out);
}

static int my_str_contain(char *s, char c)
{
	for (int i = 0; s[i]; ++i) {
		if (s[i] == c)
			return (1);
	}
	return (0);
}

static char *get_read_size(int fd)
{
	int i = 0;
	char *buff = malloc (sizeof (char) * (READ_SIZE + 1));

	if ((i = read (fd, buff, READ_SIZE)) <= 0) {
		free (buff);
		return (NULL);
	}
	buff[i] = '\0';
	return (buff);
}

char *get_next_line(int fd)
{
	if (READ_SIZE <= 0)
		return (NULL);
	static char *svg = "";
	char *buff = get_read_size (fd);

	if (fd == 0 || fd == -1)
		return (NULL);
	if (buff == NULL) {
		buff = malloc (sizeof (char));
		buff[0] = '\0';
		if (*svg == '\0') {
			free (svg);
			free (buff);
			return (NULL);
		}
	}
	if (*svg != '\0')
		buff = my_stradd (svg, buff);
	while (my_str_contain (buff, '\n') != 1) {
		buff = my_stradd (buff, get_read_size (fd));
	}
	svg = save_end_buff (buff);
	return (buff);
}
