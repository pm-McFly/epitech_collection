/*
** EPITECH PROJECT, 2017
** my_strchr.c
** File description:
** Desc
*/

char *my_strchr (const char *s, int c)
{
	do {
		if (*s == c) {
			return (char *)s;
		}
	} while (*s++)
		;
	return (0);
}
