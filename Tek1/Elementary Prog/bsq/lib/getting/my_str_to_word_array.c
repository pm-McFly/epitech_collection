/*
** EPITECH PROJECT, 2018
** my_str_to_word_array.c
** File description:
** splits a string into words
*/

#include "my.h"

static int count_word(char *s, char c)
{
	int n = 0;

	for (int i = 0; s[i]; ++i) {
		if (s[i] == c)
			++n;
	}
	++n;
	return (n);
}

char **my_str_to_word_array(char *s, char c)
{
	char **tab;
	char *p = s;
	int cw = count_word(s, c);
	int w = 0;

	tab = malloc(sizeof(char *) * cw + 1);
	if (tab == NULL)
		return (NULL);
	for (int i = 0; p[i]; ++i) {
		if (p[i] == c) {
			p[i] = '\0';
			tab[w] = my_strdup(s);
			++w;
			s = (p + (i + 1));
		}
	}
	tab[w] = my_strdup(s);
	tab[cw] = NULL;
	return (tab);
}