/*
** EPITECH PROJECT, 2017
** my_strdup
** File description:
** strdup
*/

#include "my.h"

char *my_strdup(const char *s)
{
	char *dup;

	dup = malloc(sizeof(char) * my_strlen(s));
	if (dup == NULL)
		return (NULL);
	else
		return (my_strcpy(dup, s));
}