/*
** EPITECH PROJECT, 2017
** my_strlen
** File description:
** strlen
*/

int my_strlen(const char *str)
{
	int i = 0;

	while (str[i] != 0)
		i++;
	return (i);
}
