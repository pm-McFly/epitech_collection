/*
** EPITECH PROJECT, 2017
** my_strcmp
** File description:
** strcmp
*/

int	is_shorter(char const *s1, char const *s2, int i, int ret);

int	my_strcmp(char const *s1, char const *s2)
{
	int ret = 0;
	int i = 0;

	while (s1[i] != '\0' && s2[i] != '\0' && ret == 0) {
		if (s1[i] < s2[i])
			ret--;
		else if (s1[i] > s2[i])
			ret++;
		else
			i++;
	}
	return (is_shorter (s1, s2, i, ret));
}

int	is_shorter(char const *s1, char const *s2, int i, int ret)
{
	if (ret != 0)
		return (ret);
	else if (s1[i] == '\0' && s2[i] != '\0')
		return (-1);
	else if (s1[i] != '\0')
		return (1);
	else
		return (0);
}
