/*
** EPITECH PROJECT, 2017
** my_print_ptr.c
** File description:
** my_printf
*/

#include "my.h"

int my_print_binary(va_list ap)
{
	return (my_u_base_changer(va_arg(ap, int), 2, 0));
}

int my_print_octal(va_list ap)
{
	return (my_u_base_changer(va_arg(ap, int), 8, 0));
}

int my_print_lhex(va_list ap)
{
	return (my_u_base_changer(va_arg(ap, int), 16, 0));
}

int my_print_uhex(va_list ap)
{
	return (my_u_base_changer(va_arg(ap, int), 16, 1));
}
