/*
** EPITECH PROJECT, 2017
** my.h
** File description:
** global prgm header
*/

#ifndef	MY_H_
#define	MY_H_

#include "my_maths.h"
#include <stdarg.h>
#include <stdio.h>
#include <stdlib.h>
#include <stddef.h>
#include <string.h>
#include <unistd.h>
#include <sys/types.h>
#include <sys/stat.h>
#include <fcntl.h>

#define	FILE_NOT_FOUND (84)
#define	STAT_IMPOSSIBLE (84)
#define	OPEN_FAIL (84)
#define	BAD_RETURN (84)
#define	OVERFLOW_PREVENT (84)
#define	EXIT_PRGM_NORMALY (0)
#define	EXIT_FCTN_NORMALY (0)
#define EXIT_FCTN_FAILURE (1)
#define EXIT_FCTN_DANGER (NULL)
#define EXIT_FCTN_WARNING (-1)
#define EXIT_ERROR (84)
#define INVALID_CHAR (84)
#define INVALID_NUMBER (84)
#define EXIT_USAGE (84)

typedef struct {
	char flag;
	int (*function)(va_list ap);
} printf_action_t;

int	my_putchar(char);
int	my_isneg(int);
int	my_put_nbr(int);
void	my_swap(int *, int *);
int	my_putstr(char const *);
int	my_strlen(char const *);
int	my_getnbr(char const *);
void	my_sort_int_array(int *, int);
int	my_compute_power_rec(int, int);
int	my_compute_square_root(int);
int	my_is_prime(int);
int	my_find_prime_sup(int);
char	*my_strcpy(char *, char const *);
char	*my_strncpy(char *, char const *, int);
char	*my_revstr(char *);
char	*my_strstr(char const *, char const *);
int	my_strcmp(char const *, char const *);
int	my_strncmp(char const *, char const *, int);
char	*my_strupcase(char *);
char	*my_strlowcase(char *);
char	*my_strcapitalize(char *);
int	my_str_isalpha(char const *);
int	my_str_isnum(char const *);
int	my_str_islower(char const *);
int	my_str_isupper(char const *);
int	my_str_isprintable(char const *);
int	my_showstr(char const *);
int	my_showmem(char const *, int);
char	*my_strcat(char *, char const *);
char	*my_strncat(char *, char const *, int);
char	*my_strdup(char const *);
char	*fix_expr(char *);
int	my_printf(const char *, ...);
void	*my_memecpy(void *, const void *);
int	my_char_isnum(char);
int	my_put_unbr (unsigned int);
int	parser(char, va_list);
int	my_l_base_changer(char *, int);
int	my_u_base_changer(unsigned int, int, int);
int	flag_nbr(char, va_list);
int	flag_str(char, va_list);
int	my_print_ptr(va_list);
int	my_print_nbr(va_list);
int	my_print_unbr(va_list);
int	my_print_binary(va_list);
int	my_print_octal(va_list);
int	my_print_lhex(va_list);
int	my_print_uhex(va_list);
int	my_print_char(va_list);
int	my_print_string(va_list);
int	my_print_ostring(va_list);
int	my_v_printf(char const *);
int	my_pow(int, int);
int	throw_error (char *);
char	*my_strchr (register const char *, int);
int	check_help_display(int, char **);
char	**my_str_to_word_array(char *, char);

#endif
