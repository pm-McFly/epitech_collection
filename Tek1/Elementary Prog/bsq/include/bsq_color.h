#pragma once

#ifndef BSQ_RESET_COLOR
	#define BSQ_RESET_COLOR "\033[0m"
#endif /* !BSQ_RESET_COLOR */

#ifndef BSQ_OBSTACLE_COLOR
        #define BSQ_OBSTACLE_COLOR "\033[1;31m"
#endif /* !BSQ_OBSTACLE_COLOR */

#ifndef BSQ_SQUARE_COLOR
        #define BSQ_SQUARE_COLOR "\033[1;34m"
#endif /* !BSQ_SQUARE_COLOR */
