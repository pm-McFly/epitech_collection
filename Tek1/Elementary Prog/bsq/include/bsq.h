/*
** EPITECH PROJECT, 2017
** bsq.h
** File description:
** Desc
*/

#ifndef BSQ_H_
#define BSQ_H_	1

/* include */
#include "my.h"
#include <sys/types.h>
#include <sys/stat.h>
#include <fcntl.h>

/* structures */
typedef struct grid_s {
	int *data;
	int x;
	int y;
} grid_t;

typedef struct max_s {
	int value;
	int x;
	int y;
} max_t;

/* prototype */
struct stat get_stat(struct stat fst, char *file_path);
int open_file(int fd, char *file_path);
grid_t *get_grid(char *file_path);
grid_t *compute_grid(grid_t *grid);
grid_t *place_x(grid_t *grid);

#endif
