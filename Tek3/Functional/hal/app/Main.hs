module Main where

import           Control.Exception
import           ErrorsTypes
import           Eval
import           Parentheses
import           REPL
import           System.Environment
import           System.Exit

main :: IO ()
main = getArgs >>=
    \x -> if null x
            then repl []
            else
                if "-i" `elem` x
                    then
                        if length x == 1
                            then repl []
                            else mapM readFile (filter (/= "-i") x) >>=
                                \xl -> repl (snd $ evalList (cleanToks $ unwords xl) [])
                    else (mapM readFile x >>=
                        \xl -> putStr (last (fst $ evalList (cleanToks $ unwords xl) []))) `catch`
                            \e -> putStrLn ("*** ERROR: " ++ show (e :: MyException)) >> exitWith (ExitFailure 84)
