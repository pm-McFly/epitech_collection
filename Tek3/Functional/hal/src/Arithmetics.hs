module Arithmetics
    ( hadd
    , hsub
    , hmul
    , hdiv
    , hmod
    , hless
    , hdiv'
    ) where

import           Control.Exception
import           Data.Ratio
import           ErrorsTypes

hadd :: [Int] -> [(String, String)] -> (String, [(String, String)])
hadd [] env   = ("0", env)
hadd list env = (show (sum list), env)

hsub :: [Int] -> [(String, String)] -> (String, [(String, String)])
hsub [] env     = throw SubNotEnoughArgs
hsub [x] env    = (show $ negate x, env)
hsub (x:xs) env = (show $ x - sum xs, env)

hmul :: [Int] -> [(String, String)] -> (String, [(String, String)])
hmul [] env   = ("1", env)
hmul list env = (show (product list), env)

hdiv :: [Int] -> [(String, String)] -> (String, [(String, String)])
hdiv [] env  = ("DIV: Wrong Number Of Args: 1 or more needed", env)
hdiv [x] env
    | x == 0 = ("DIV: WARNING division by 0", env)
    | x < 0  = ("-1/" ++ show (negate x), env)
    | x > 0  = ("1/" ++ show x, env)
-- hdiv (x:xs) env = (show (toFraction (x / (product xs))), env)

hdiv' :: [Int] -> [(String, String)] -> (String, [(String, String)])
hdiv' [] env                         = throw DivNotEnoughArgs
hdiv' [x] env                        = throw DivNotEnoughArgs
hdiv' (x:xs) env
    | length xs == 1 && head xs /= 0 = (show $ x `quot` head xs, env)
    | length xs == 1 && head xs == 0 = throw DivDivisionByZero
    | otherwise = throw DivNotEnoughArgs

hmod :: [Int] -> [(String, String)] -> (String, [(String, String)])
hmod [] env                          = throw ModNotEnoughArgs
hmod [x] env                         = throw ModNotEnoughArgs
hmod (x:xs) env
    | length xs == 1 && head xs /= 0 = (show $ x `mod` head xs, env)
    | length xs == 1 && head xs == 0 = throw ModDivisionByZero
    | otherwise = throw ModNotEnoughArgs

hless :: [Int] -> [(String, String)] -> (String, [(String, String)])
hless [] env                         = throw LessNotEnoughArgs
hless [x] env                        = throw LessNotEnoughArgs
hless (x:xs) env
    | length xs == 1 && x < head xs  = ("#t", env)
    | length xs == 1 && x >= head xs = ("#f", env)
    | otherwise                      = throw LessNotEnoughArgs
