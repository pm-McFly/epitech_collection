module Expression
    ( getExpression
    ) where

import           Parentheses
import           System.IO

promptLine :: String -> IO ()
promptLine "" = hFlush stdout
promptLine x  = putStr x >>
    hFlush stdout

getExpression :: String -> String -> IO String
getExpression s p = promptLine p >>
    getLine >>=
    \x ->
        (\sx -> if checkExpression sx
                    then return sx
                    else getExpression (prettifySex sx) (shiftPrompt sx)
        ) $ trim (s ++ x)

checkExpression :: String -> Bool
checkExpression s = unbalanced s == 0

countChar :: String -> Char -> Int
countChar xs x = length $ filter (== x) xs

unbalanced :: String -> Int
unbalanced "" = 0
unbalanced s  = countChar s '(' - countChar s ')'

shiftPrompt :: String -> String
shiftPrompt sx = "~" ++ replicate (unbalanced sx + 11) ' '

prettifySex :: String -> String
prettifySex "" = ""
prettifySex sEx
    | last sEx == '(' || last sEx == ' ' = sEx
    | otherwise = sEx ++ " "

