module ErrorsTypes
    ( MyException (..)
    )
    where

import           Control.Exception
import           Data.Typeable

data MyException =
    SubNotEnoughArgs    |
    DivNotEnoughArgs    |
    DivDivisionByZero   |
    ModNotEnoughArgs    |
    ModDivisionByZero   |
    LessNotEnoughArgs   |
    UnboundVariable     |
    QuoteNotEnoughArgs  |
    InvalidInput        |
    CarNotEnoughArgs    |
    CarInvalidInput     |
    CdrNotEnoughArgs    |
    CdrInvalidInput     |
    ConsNotEnoughArgs   |
    EqNotEnoughArgs     |
    NullNotEnoughArgs   |
    NotNotEnoughArgs    |
    AndNotEnoughArgs    |
    OrNotEnoughArgs     |
    IllegalFunctionCall |
    DefineNotEnoughArgs |
    CondInvalidInput    |
    ArithInvalidInput
    deriving Typeable

instance Show MyException where
    show SubNotEnoughArgs       = "SUB: Not enough arguments: 1 or more expected !"
    show DivNotEnoughArgs       = "DIV: Wrong number of arguments: 2 expected !"
    show DivDivisionByZero      = "DIV: Wrong number of arguments: 2 expected !"
    show ModNotEnoughArgs       = "MOD: Wrong number of arguments: 2 expected !"
    show ModDivisionByZero      = "MOD: Wrong number of arguments: 2 expected !"
    show LessNotEnoughArgs      = "LESS: Wrong number of arguments: 2 expected !"
    show UnboundVariable        = "Unbound Variable !"
    show QuoteNotEnoughArgs     = "QUOTE: Wrong number of arguments: 1 expected !"
    show InvalidInput           = "Invalid Input"
    show CarNotEnoughArgs       = "CAR: Wrong number of arguments: 1 expected !"
    show CarInvalidInput        = "CAR: Invalid input: List expected !"
    show CdrNotEnoughArgs       = "CDR: Wrong number of arguments: 1 expected !"
    show CdrInvalidInput        = "CDR: Invalid input: List expected !"
    show ConsNotEnoughArgs      = "CONS: Wrong number of arguments: 2 expected !"
    show EqNotEnoughArgs        = "EQ: Wrong number of arguments: 2 expected !"
    show NullNotEnoughArgs      = "NULL: Wrong number of arguments: 1 expected !"
    show NotNotEnoughArgs       = "NOT: Wrong number of arguments: 1 expected !"
    show AndNotEnoughArgs       = "AND: Wrong number of arguments: 2 expected !"
    show OrNotEnoughArgs        = "AND: Wrong number of arguments: 2 expected !"
    show IllegalFunctionCall    = "Illegal function call !"
    show DefineNotEnoughArgs    = "DEFINE: Wrong number of arguments: 2 expected !"
    show CondInvalidInput       = "COND: Invalid input: lists expected !"
    show ArithInvalidInput      = "ARITH: Invalid input: Int expected !"

instance Exception MyException
