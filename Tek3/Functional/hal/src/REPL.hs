module REPL
    ( repl
    ) where

import           Control.Exception
import           Data.Typeable
import           ErrorsTypes
import           Eval
import           Expression
import           Parentheses
import           System.Exit


repl :: [(String, String)] -> IO ()
repl env = replEv env `catch` handler
           where
               handler :: SomeException -> IO ()
               handler ex = exitSuccess

replEv :: [(String, String)] -> IO ()
replEv env = (getExpression "" "HAL-REPL ?> " >>=
            \sEx ->
                (\re -> putStrLn (last (fst re)) >> replEv (snd re))
                        $ evalList (cleanToks sEx) env) `catch`
                            \e -> putStrLn ("*** ERROR: " ++ show (e :: MyException)) >> replEv env
