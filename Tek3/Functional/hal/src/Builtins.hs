module Builtins
    ( assoc
    , quote
    , car
    , cdr
    , cons
    , ato
    , hlist
    , heq
    , hnull
    , hnot
    , hand
    , hor
    ) where

import           Control.Exception
import           Data.Char
import           ErrorsTypes
import           Parentheses

assoc :: String -> [(String, String)] -> [(String, String)] -> (String, [(String, String)])
assoc atom [] cst  = throw UnboundVariable
assoc atom env cst = if fst (head env) == atom
                        then (snd (head env), cst)
                        else assoc atom (tail env) cst

quote :: [String] -> [(String, String)] -> (String, [(String, String)])
quote [] env    = throw QuoteNotEnoughArgs
quote (x:xs) env
    | null xs   = (x, env)
    | otherwise = throw QuoteNotEnoughArgs

car :: String -> [(String, String)] -> (String, [(String, String)])
car "" env       = throw CarNotEnoughArgs
car "NIL" env    = ("NIL", env)
car ('(':xs) env = (head (words xs), env)
car x env        = throw CarInvalidInput

cdr :: String -> [(String, String)] -> (String, [(String, String)])
cdr "" env                                                                         = throw CdrNotEnoughArgs
cdr "NIL" env                                                                      = ("NIL", env)
cdr ('(':xs) env
    | unwords (tail (words xs)) == ""                                              = ("NIL", env)
    | trim (head (tail (words xs))) == "." && all isDigit (init (last (words xs))) = (init (last (words xs)), env)
    | trim (head (tail (words xs))) == "."                                         = ("(" ++ init (last (words xs)) ++ ")", env)
    | otherwise                                                                    = ("(" ++ unwords (tail (words xs)), env)
cdr x env                                                                          = throw CdrInvalidInput

cons :: [String] -> [(String, String)] -> (String, [(String, String)])
cons [] env                                                        = throw ConsNotEnoughArgs
cons [x] env                                                       = throw ConsNotEnoughArgs
cons args env
    | length args == 2 && head args == "NIL" && last args == "NIL" = ("(NIL)", env)
    | length args == 2 && head args /= "NIL" && last args == "NIL" = ("(" ++ head args ++ ")", env)
    | length args == 2 && head (last args) == '('                  = ("(" ++ head args ++ " " ++ tail (init (last args)) ++ ")", env)
    | length args == 2                                             = ("(" ++ head args ++ " . " ++ last args ++ ")", env)
    | otherwise                                                    = throw ConsNotEnoughArgs

ato :: String -> [(String, String)] -> (String, [(String, String)])
ato x env = if head x == '('
                then ("#f", env)
                else ("#t", env)

hlist :: [String] -> [(String, String)] -> (String, [(String, String)])
hlist [] env = ("NIL", env)
hlist x env  = ("(" ++ unwords x ++ ")", env)

heq :: [String] -> [(String, String)] -> (String, [(String, String)])
heq [] env          = throw EqNotEnoughArgs
heq [x] env         = throw EqNotEnoughArgs
heq x env
    | length x == 2 = if head x == last x
                        then ("#t", env)
                        else ("#f", env)
    | otherwise     = throw EqNotEnoughArgs

hnull :: [String] -> [(String, String)] -> (String, [(String, String)])
hnull [] env     = throw NullNotEnoughArgs
hnull [x] env
    | x == "NIL" = ("#t", env)
    | otherwise  = ("#f", env)
hnull _ env      = throw NullNotEnoughArgs

hnot :: [String] -> [(String, String)] -> (String, [(String, String)])
hnot [] env     = throw NotNotEnoughArgs
hnot [x] env
    | x == "#f" = ("#t", env)
    | otherwise = ("#f", env)
hnot _ env      = throw NotNotEnoughArgs

hand :: [String] -> [(String, String)] -> (String, [(String, String)])
hand [] env          = throw AndNotEnoughArgs
hand [x] env         = throw AndNotEnoughArgs
hand (x:xs) env
    | length xs == 1 = if x == "#t" && head xs == "#t" then ("#t", env) else ("#f", env)
    | otherwise      = throw AndNotEnoughArgs

hor :: [String] -> [(String, String)] -> (String, [(String, String)])
hor [] env           = throw OrNotEnoughArgs
hor [x] env          = throw OrNotEnoughArgs
hor (x:xs) env
    | length xs == 1 = if x == "#t" || head xs == "#t" then ("#t", env) else ("#f", env)
    | otherwise      = throw OrNotEnoughArgs
