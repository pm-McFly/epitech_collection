module Parentheses
    ( tokenizer
    , cleanToks
    , fillSegs
    , trim
    ) where

import           Data.Char     (isSpace)
import           Data.Function
import           Data.List

import           Debug.Trace

tokenizer :: String -> [String]
tokenizer "" = []
tokenizer s  = parenSegs s

cleanToks :: String -> [String]
cleanToks "" = []
cleanToks s  = cleanSegs $ splitter (tokenizer s)

parenSegs :: String -> [String]
parenSegs "" = []
parenSegs s  = map (f s) (fillSegs (checkQuote (highNestedSegs (sort (parenPairs s))) s) 0 (length s))
  where
    f s (i, j) = take (j - i + 1) (drop i s)

parenPairs :: String -> [(Int, Int)]
parenPairs = go 0 []
  where
    go _ _        []         = []
    go j acc      ('(' : cs) =          go (j + 1) (j : acc) cs
    go j []       (')' : cs) =          go (j + 1) []        cs -- unbalanced parentheses!
    go j (i : is) (')' : cs) = (i, j) : go (j + 1) is        cs
    go j acc      (c   : cs) =          go (j + 1) acc       cs

firstParenSeg :: String -> String
firstParenSeg s = f s (minimum (parenPairs s))
  where
    f s (i, j) = take (j - i + 1) (drop i s)

highNestedSegs :: [(Int, Int)] -> [(Int, Int)]
highNestedSegs []     = []
highNestedSegs [x]    = [x]
highNestedSegs (x:xs) = x : filter (\(_,a) -> a > snd x) (highNestedSegs xs)

fillSegs :: [(Int, Int)] -> Int -> Int -> [(Int, Int)]
fillSegs [] 0 0                        = []
fillSegs [] b l                        = [(b, l - 1)]
fillSegs x b 0                         = []
fillSegs [x] b l
    | (fst x == b) && (snd x == l - 1) = [x]
    | (fst x == b) && (snd x /= l - 1) = [x, (snd x + 1, l - 1)]
    | (fst x /= b) && (snd x == l - 1) = [(b, fst x - 1), x]
    | (fst x /= b) && (snd x /= l - 1) = [(b, fst x - 1), x, (snd x + 1, l - 1)]
fillSegs (x:xs) b l
    | (b == 0)
        && (fst x /= b)
        && (fst (head xs) > snd x + 1)
        && (snd (head xs) /= l - 1)    = [(0, fst x - 1), x, (snd x + 1, fst (head xs) - 1)] ++ fillSegs xs (fst (head xs)) l
    | (length xs == 1)
        && (fst x == b)
        && (fst (head xs) > snd x + 1)
        && (snd (head xs) == l - 1)    = [x, (snd x + 1, fst (head xs) - 1), head xs]
    | (length xs == 1)
        && (b == 0)
        && (fst x /= b)
        && (fst (head xs) > snd x + 1)
        && (snd (head xs) == l - 1)    = [(0, fst x - 1), x, (snd x + 1, fst (head xs) - 1), head xs]
    | (fst x == b)
        && (fst (head xs) > snd x + 1)
        && (snd (head xs) /= l - 1)    = [x, (snd x + 1, fst (head xs) - 1)] ++ fillSegs xs (fst (head xs)) l
    | (fst (head xs) > snd x + 1)
        && (snd (head xs) /= l - 1)    = [x, (snd x + 1, fst (head xs) - 1)] ++ fillSegs xs (fst (head xs)) l

trim :: String -> String
trim = f . f
  where f = reverse . dropWhile isSpace

cleanSegs :: [String] -> [String]
cleanSegs []                          = []
cleanSegs [x]
    | trim x == ""                    = []
    | head x == '(' || head x == '\'' = [x]
    | otherwise                       = words x
cleanSegs (x:xs)
    | trim x == ""                    = cleanSegs xs
    | otherwise                       = unwords (words x) : cleanSegs xs

checkQuote :: [(Int, Int)] -> String -> [(Int, Int)]
checkQuote [] s                                                             = []
checkQuote (x:xs) s
    | (fst x /= 0) && ((s !! fst x) == '(') && ((s !! (fst x - 1)) == '\'') = (fst x - 1, snd x) : checkQuote xs s
    | otherwise                                                             = x : checkQuote xs s

splitter :: [String] -> [String]
splitter []         = []
splitter (x:xs)
    | head x == '(' = x : splitter xs
    | otherwise     = words x ++ splitter xs
