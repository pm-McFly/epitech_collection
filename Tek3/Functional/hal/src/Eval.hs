module Eval
    ( eval
    , evArgsInt
    , evalList
    ) where

import           Arithmetics
import           Builtins
import           Control.Exception
import           Data.Char
import           ErrorsTypes
import           Parentheses

eval :: String -> [(String, String)] -> (String, [(String, String)])
eval "#t" env   = ("#t", env)
eval "#f" env   = ("#f", env)
eval "NIL" env  = ("NIL", env)
eval "'NIL" env = ("NIL", env)
eval "nil" env  = ("NIL", env)
eval "'nil" env = ("NIL", env)
eval "()" env   = ("NIL", env)
eval "'()" env  = ("NIL", env)
eval "" env     = ("; No Value", env)
eval sEx env    = if head sEx == '(' && last sEx == ')'
                    then evFunc (tail (init sEx)) env
                    else evAtom sEx env

evFunc :: String -> [(String, String)] -> (String, [(String, String)])
evFunc call = callFunc (head (words call)) (tail (words call))

evAtom :: String -> [(String, String)] -> (String, [(String, String)])
evAtom atom env = case atom of
                    ('\'':xs) -> quote (cleanToks xs) env
                    ('-':xs) -> if all isDigit xs
                                    then quote (cleanToks atom) env
                                    else throw InvalidInput
                    _ -> if all isDigit atom
                            then quote (cleanToks atom) env
                            else assoc (head (words atom)) env env

callFunc :: String -> [String] -> [(String, String)] -> (String, [(String, String)])
callFunc name args env = case name of
                            "quote"               -> quote (cleanToks (unwords args)) env
                            "car"                 -> car (fst $ eval (unwords args) env) env
                            "cdr"                 -> cdr (fst $ eval (unwords args) env) env
                            "cons"                -> cons (fst $ evalList (cleanToks (unwords args)) env) env
                            "define"              -> define args env
                            "atom?"               -> ato (fst $ eval (unwords args) env) env
                            "list"                -> hlist (fst $ evalList (cleanToks (unwords args)) env) env
                            "eq?"                 -> heq (fst $ evalList (cleanToks (unwords args)) env) env
                            "cond"                -> evCond (cleanToks $ unwords args) env
                            "null?"               -> hnull (fst $ evalList (cleanToks (unwords args)) env) env
                            "not"                 -> hnot (fst $ evalList (cleanToks (unwords args)) env) env
                            "and?"                -> hand (fst $ evalList (cleanToks (unwords args)) env) env
                            "or?"                 -> hor (fst $ evalList (cleanToks (unwords args)) env) env
                            c
                             | c `elem` ["+"
                                        , "-"
                                        , "*"
                                        , "div"
                                        , "mod"
                                        , "<"
                                        , "/"
                                        ]         -> evMaths name (unwords args) env
                            _                     -> throw IllegalFunctionCall

define :: [String] -> [(String, String)] -> (String, [(String, String)])
define [] env          = throw DefineNotEnoughArgs
define [x] env         = throw DefineNotEnoughArgs
define args env
    | length args == 2 = (head args, env ++ [(head args, fst $ eval (unwords $ tail args) env)])
    | otherwise        = throw DefineNotEnoughArgs

checkInputList :: [String] -> [String]
checkInputList []     = []
checkInputList (x:xs) = if all isDigit x
                            then x : checkInputList xs
                            else throw ArithInvalidInput

evArgsInt :: [String] -> [Int]
evArgsInt x = map read (checkInputList x)

evalList :: [String] -> [(String, String)] -> ([String], [(String, String)])
evalList [] env     = ([], env)
evalList (x:xs) env = (fst (eval x env) : fst (evalList xs (snd (eval x env))), snd (evalList xs (snd (eval x env))))

evMaths :: String -> String -> [(String, String)] -> (String, [(String, String)])
evMaths name args env
    | name == "+"   = hadd  (evArgsInt (fst $ evalList (cleanToks args) env)) env
    | name == "-"   = hsub  (evArgsInt (fst $ evalList (cleanToks args) env)) env
    | name == "*"   = hmul  (evArgsInt (fst $ evalList (cleanToks args) env)) env
    | name == "div" = hdiv' (evArgsInt (fst $ evalList (cleanToks args) env)) env
    | name == "/"   = hdiv  (evArgsInt (fst $ evalList (cleanToks args) env)) env
    | name == "mod" = hmod  (evArgsInt (fst $ evalList (cleanToks args) env)) env
    | name == "<"   = hless (evArgsInt (fst $ evalList (cleanToks args) env)) env

evCond :: [String] -> [(String, String)] -> (String, [(String, String)])
evCond [] env                                      = ("NIL", env)
evCond (x:xs) env
    | head (trim x) == '(' && last (trim x) == ')' = if fst (eval (head (cleanToks (tail (init x)))) env) == "#t"
                                                        then (fst (eval (unwords (tail (cleanToks (tail (init x))))) env), env)
                                                        else evCond xs env
    | otherwise                                    = throw CondInvalidInput

-- ; The Lisp defined in McCarthy's 1960 paper, translated into CL.
--; Assumes only quote, atom, eq, cons, car, cdr, cond.
--; Bug reports to lispcode@paulgraham.com.
--
--(defun null. (x)
--  (eq x '()))
--
--(defun and. (x y)
--  (cond (x (cond (y 't) ('t '())))
--        ('t '())))
--
--(defun not. (x)
--  (cond (x '())
--        ('t 't)))
--
--(defun append. (x y)
--  (cond ((null. x) y)
--        ('t (cons (car x) (append. (cdr x) y)))))
--
--(defun list. (x y)
--  (cons x (cons y '())))
--
--(defun pair. (x y)
--  (cond ((and. (null. x) (null. y)) '())
--        ((and. (not. (atom x)) (not. (atom y)))
--         (cons (list. (car x) (car y))
--               (pair. (cdr x) (cdr y))))))
--
--(defun assoc. (x y)
--  (cond ((eq (caar y) x) (cadar y))
--        ('t (assoc. x (cdr y)))))
--
--(defun eval. (e a)
--  (cond
--    ((atom e) (assoc. e a))
--    ((atom (car e))
--     (cond
--       ((eq (car e) 'quote) (cadr e))
--       ((eq (car e) 'atom)  (atom   (eval. (cadr e) a)))
--       ((eq (car e) 'eq)    (eq     (eval. (cadr e) a)
--                                    (eval. (caddr e) a)))
--       ((eq (car e) 'car)   (car    (eval. (cadr e) a)))
--       ((eq (car e) 'cdr)   (cdr    (eval. (cadr e) a)))
--       ((eq (car e) 'cons)  (cons   (eval. (cadr e) a)
--                                    (eval. (caddr e) a)))
--       ((eq (car e) 'cond)  (evcon. (cdr e) a))
--       ('t (eval. (cons (assoc. (car e) a)
--                        (cdr e))
--                  a))))
--    ((eq (caar e) 'label)
--     (eval. (cons (caddar e) (cdr e))
--            (cons (list. (cadar e) (car e)) a)))
--    ((eq (caar e) 'lambda)
--     (eval. (caddar e)
--            (append. (pair. (cadar e) (evlis. (cdr e) a))
--                     a)))))
--
--(defun evcon. (c a)
--  (cond ((eval. (caar c) a)
--         (eval. (cadar c) a))
--        ('t (evcon. (cdr c) a))))
--
--(defun evlis. (m a)
--  (cond ((null. m) '())
--        ('t (cons (eval.  (car m) a)
--                  (evlis. (cdr m) a)))))
