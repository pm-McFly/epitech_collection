module Main where

import           Prelude
import           System.Environment
import           System.IO.Error    (tryIOError)
import           Pipeline

getFileContent :: String -> IO String
getFileContent = readFile

parseArgs :: [String] -> IO ()
parseArgs [] = return ()
parseArgs (path:xs) = do
    content <- getFileContent path
    run path content
    parseArgs xs

main :: IO ()
main = do
    args <- getArgs
    if null args
        then error "Invalid number of arguments"
        else parseArgs args
