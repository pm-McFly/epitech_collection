module Emit.Arithmetic where

import LLVM.AST
import qualified LLVM.AST.FloatingPointPredicate as FP

import Emit.Base
import Emit.State
import Emit.Instructions

fAddition :: Operand -> Operand -> Codegen Operand
fAddition left right = instr double $ FAdd noFastMathFlags left right []

fSubstraction :: Operand -> Operand -> Codegen Operand
fSubstraction left right = instr double $ FSub noFastMathFlags left right []

fMultiplication :: Operand -> Operand -> Codegen Operand
fMultiplication left right = instr double $ FMul noFastMathFlags left right []

fDivision :: Operand -> Operand -> Codegen Operand
fDivision left right = instr double $ FDiv noFastMathFlags left right []

fCompare :: FP.FloatingPointPredicate -> Operand -> Operand -> Codegen Operand
fCompare predicate left right = instr double $ FCmp predicate left right []

fLighter :: Operand -> Operand -> Codegen Operand
fLighter left right = fCompare FP.ULT left right

fNotEqual :: Operand -> Operand -> Codegen Operand
fNotEqual left right = fCompare FP.UNE left right

fFloatCast :: Operand -> Codegen Operand
fFloatCast op = unsignedToFloat op