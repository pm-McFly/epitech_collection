{-# LANGUAGE OverloadedStrings #-}

module Emit.Emit
    ( emit
    ) where

import Control.Monad
import Data.ByteString.Short
import Data.String (fromString)

import qualified LLVM.AST          as AST
import qualified LLVM.AST.Constant as C
import qualified LLVM.AST.Float    as F
import qualified LLVM.AST.FloatingPointPredicate as FP

import Common.Base
import Emit.Base
import Emit.State
import Emit.Instructions
import Emit.Arithmetic
import Emit.Instructions

zeroConstant = toConstant $ C.Float $ F.Double 0.0

emit :: Node -> AST.Module
emit root = runLLVM initModule (emitBlock root)

initModule :: AST.Module
initModule = emptyModule "Koak"

toShortStr :: [String] -> [ShortByteString]
toShortStr list = (map (\arg -> fromString arg) list)

toFunctionArgs :: [ShortByteString] -> [(AST.Type, AST.Name)]
toFunctionArgs args = map (\x -> (double, AST.Name x)) args

emitBlock :: Node -> LLVM ()
emitBlock (Function name args body _) = define double (fromString name) (toFunctionArgs shortStrArgs) block
    where
        shortStrArgs = toShortStr args
        block retType = do
            forM shortStrArgs $ \a -> do
                var <- alloca double
                store var (local double (AST.Name a))
                assign a var
            emitOperand body >>= ret

emitBlock (Extern name args _) = external double (fromString name) (toFunctionArgs $ toShortStr args)

emitBlock (Block (x:xs)) = if Prelude.null xs
    then emitBlock x
    else do
        emitBlock x
        emitBlock $ Block xs

emitBlock (BinaryDef name left right body line) = emitBlock $ Function ("binary" ++ name) [left, right] body line

emitBlock node = define double "main" [] $ \main -> emitOperand node >>= ret

emitOperand :: Node -> Codegen AST.Operand
emitOperand (Var name _) = getVar (fromString name) >>= load
emitOperand (Constant (Floating value) _) = return $ toConstant $ C.Float (F.Double value)
emitOperand (Constant (Fixed value) _) = return $ toConstant $ C.Float (F.Double (fromIntegral value))

emitOperand (Let args body _) = do
    emitLetArgs args
    emitOperand body
    where
        emitLetArgs [(name, expr)] = do
            var <- alloca double
            value <- emitOperand expr
            store var value
            assign (fromString name) var
            return value

        emitLetArgs ((name, expr):xs) = do
            var <- alloca double
            value <- emitOperand expr
            store var value
            assign (fromString name) var
            emitLetArgs xs

emitOperand (Binary op leftNode rightNode line) = do
    left <- emitOperand leftNode
    right <- emitOperand rightNode
    case op of
        "+" -> fAddition left right
        "-" -> fSubstraction left right
        "*" -> fMultiplication left right
        "/" -> fDivision left right
        "<" -> fLighter left right >>= fFloatCast
        _ -> emitOperand (Call ("binary" ++ op) [leftNode, rightNode] line)

emitOperand (Call name args _) = do
    callArgs <- mapM emitOperand args
    call (externRef (getFunctionType $ types) (AST.Name $ fromString name)) callArgs
    where
        types = take (Prelude.length args) (repeat double)

emitOperand (If condition body body2 _) = do
    thenBlock <- addBlock "if.then"
    elseBlock <- addBlock "if.else"
    exitBlock <- addBlock "if.exit"
    -- If Entry
    ifTest <- emitOperand condition
    result <- fNotEqual ifTest zeroConstant
    cbr result thenBlock elseBlock
    -- Then
    setBlock thenBlock
    bodyValue <- emitOperand body
    br exitBlock
    thenBlock <- getBlock
    -- Else
    setBlock elseBlock
    body2Value <- emitOperand body2
    br exitBlock
    elseBlock <- getBlock
    -- Exit
    setBlock exitBlock
    phi double [(bodyValue, thenBlock), (body2Value, elseBlock)]

emitOperand (For name expr condition increment body _) = do
    loopBlock <- addBlock "for.loop"
    exitBlock <- addBlock "for.exit"
    -- For Entry
    i <- alloca double
    exprValue <- emitOperand expr
    stepValue <- emitOperand increment
    store i exprValue
    assign (fromString name) i
    br loopBlock
    -- Loop
    setBlock loopBlock
    bodyValue <- emitOperand body
    iValue <- load i
    iNext <- fAddition iValue stepValue
    store i iNext
    conditionValue <- emitOperand condition
    loopTest <- fNotEqual conditionValue zeroConstant
    cbr loopTest loopBlock exitBlock
    -- Exit
    setBlock exitBlock
    return zeroConstant
