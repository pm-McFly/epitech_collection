{-# LANGUAGE GeneralizedNewtypeDeriving #-}
{-# LANGUAGE OverloadedStrings #-}

module Emit.Instructions where

import Data.ByteString.Short
import Data.Function

import LLVM.AST
import LLVM.AST.AddrSpace
import LLVM.AST.Global
import Control.Monad.State
import LLVM.AST.Typed
import qualified LLVM.AST.CallingConvention as CC
import qualified LLVM.AST.Linkage as L

import Emit.Base
import Emit.State

newtype LLVM a = LLVM (State Module a)
  deriving (Functor, Applicative, Monad, MonadState Module)

runLLVM :: Module -> LLVM a -> Module
runLLVM mod (LLVM m) = execState m mod

emptyModule :: ShortByteString -> Module
emptyModule label = defaultModule {moduleName = label}

addDefn :: Definition -> LLVM ()
addDefn d = do
  defs <- gets moduleDefinitions
  modify $ \s -> s {moduleDefinitions = defs ++ [d]}

define :: Type -> ShortByteString -> [(Type, Name)] -> (Type -> Codegen a) -> LLVM ()
define retType label argsTypes body = addDefn $ GlobalDefinition $ functionDefaults {
    name = Name label,
    parameters = ([Parameter ty nm [] | (ty, nm) <- argsTypes], False),
    returnType = retType,
    basicBlocks = bls
}
  where
    bls = createBlocks $ execCodegen $ do
      enter <- addBlock entryBlockName
      _ <- setBlock enter
      body ptrThisType
    ptrThisType = PointerType {
        pointerReferent = FunctionType {
            resultType = retType,
            argumentTypes = map fst argsTypes,
            isVarArg = False
        },
        pointerAddrSpace = AddrSpace 0
    }

external :: Type -> ShortByteString -> [(Type, Name)] -> LLVM ()
external retType label argsType =
    addDefn $ GlobalDefinition $ functionDefaults {
        name = Name label,
        linkage = L.External,
        parameters = ([Parameter ty nm [] | (ty, nm) <- argsType], False),
        returnType = retType,
        basicBlocks = []
    }

fnPtr :: Name -> LLVM Type
fnPtr nm = findType <$> gets moduleDefinitions
    where
        findType defs = case fnDefByName of
            [] -> error $ "Undefined function: " ++ show nm
            [fn] -> PointerType (typeOf fn) (AddrSpace 0)
            _ -> error $ "Ambiguous function name: " ++ show nm
            where
                globalDefs = [g | GlobalDefinition g <- defs]
                fnDefByName = [f | f@(Function {name = nm'}) <- globalDefs, nm' == nm]

call :: Operand -> [Operand] -> Codegen Operand
call fn args = instr double $ Call Nothing CC.C [] (Right fn) (map (\x -> (x, [])) args) [] []

alloca :: Type -> Codegen Operand
alloca ty = instr double $ Alloca ty Nothing 0 []

store :: Operand -> Operand -> Codegen ()
store ptr val = unnminstr $ Store False ptr val Nothing 0 []

load :: Operand -> Codegen Operand
load ptr = instr double $ Load False ptr Nothing 0 []

br :: Name -> Codegen (Named Terminator)
br val = terminator $ Do $ Br val []

cbr :: Operand -> Name -> Name -> Codegen (Named Terminator)
cbr cond tr fl = terminator $ Do $ CondBr cond tr fl []

phi :: Type -> [(Operand, Name)] -> Codegen Operand
phi ty incoming = instr double $ Phi ty incoming []

ret :: Operand -> Codegen (Named Terminator)
ret val = terminator $ Do $ Ret (Just val) []

retvoid :: Codegen (Named Terminator)
retvoid = terminator $ Do $ Ret Nothing []

unsignedToFloat :: Operand -> Codegen Operand
unsignedToFloat a = instr double $ UIToFP a double []
