{-# LANGUAGE GeneralizedNewtypeDeriving #-}
{-# LANGUAGE OverloadedStrings #-}

module Emit.State where

import qualified Data.Map as Map
import Data.ByteString.Short
import Data.String (fromString)
import Data.List
import Data.Function
import Data.Word

import Control.Monad.State
import Control.Applicative

import LLVM.AST
import LLVM.AST.Global

import Emit.Base

type Names = Map.Map ShortByteString Int

type SymbolTable = [(ShortByteString, Operand)]

data CodegenState = CodegenState {
        currentBlock :: Name,
        blocks :: Map.Map Name BlockState,
        symtab :: SymbolTable,
        blockCount :: Int,
        count :: Word,
        names :: Names
    }
    deriving (Show)

data BlockState = BlockState {
        idx :: Int,
        stack :: [Named Instruction],
        term :: Maybe (Named Terminator)
    }
    deriving (Show)

newtype Codegen a = Codegen {
        runCodegen :: State CodegenState a
    }
    deriving (Functor, Applicative, Monad, MonadState CodegenState)

uniqueName :: ShortByteString -> Names -> (ShortByteString, Names)
uniqueName name names =
  case Map.lookup name names of
    Nothing -> (name, Map.insert name 1 names)
    Just idx -> (name <> fromString (show idx), Map.insert name (idx + 1) names)

sortBlocks :: [(Name, BlockState)] -> [(Name, BlockState)]
sortBlocks = sortBy (on compare (idx . snd))

createBlocks :: CodegenState -> [BasicBlock]
createBlocks state = map makeBlock $ sortBlocks $ Map.toList (blocks state)

makeBlock :: (Name, BlockState) -> BasicBlock
makeBlock (l, (BlockState _ s t)) = BasicBlock l (reverse s) (maketerm t)
    where
        maketerm (Just x) = x
        maketerm Nothing = error $ "Block has no terminator: " ++ (show l)

entryBlockName :: ShortByteString
entryBlockName = "entry"

emptyBlock :: Int -> BlockState
emptyBlock i = BlockState i [] Nothing

emptyCodegen :: CodegenState
emptyCodegen = CodegenState (Name entryBlockName) Map.empty [] 1 0 Map.empty

execCodegen :: Codegen a -> CodegenState
execCodegen m = execState (runCodegen m) emptyCodegen

fresh :: Codegen Word
fresh = do
    i <- gets count
    modify $ \s -> s {count = 1 + i}
    return $ i + 1

instr :: Type -> Instruction -> Codegen (Operand)
instr instType inst = do
    n <- fresh
    let ref = (UnName n)
    blk <- current
    let i = stack blk
    modifyBlock (blk {stack = (ref := inst) : i})
    return $ local instType ref

unnminstr :: Instruction -> Codegen ()
unnminstr ins = do
    blk <- current
    let i = stack blk
    modifyBlock (blk {stack = (Do ins) : i})

terminator :: Named Terminator -> Codegen (Named Terminator)
terminator trm = do
    blk <- current
    modifyBlock (blk {term = Just trm})
    return trm

entry :: Codegen Name
entry = gets currentBlock

addBlock :: ShortByteString -> Codegen Name
addBlock bname = do
    bls <- gets blocks
    count <- gets blockCount
    nms <- gets names
    let new = emptyBlock count
        (qname, supply) = uniqueName bname nms
    modify $ \s -> s {
        blocks = Map.insert (Name qname) new bls,
        blockCount = count + 1,
        names = supply
    }
    return (Name qname)

setBlock :: Name -> Codegen Name
setBlock bname = do
    modify $ \s -> s {currentBlock = bname}
    return bname

getBlock :: Codegen Name
getBlock = gets currentBlock

modifyBlock :: BlockState -> Codegen ()
modifyBlock new = do
    active <- gets currentBlock
    modify $ \s -> s {blocks = Map.insert active new (blocks s)}

current :: Codegen BlockState
current = do
    c <- gets currentBlock
    blks <- gets blocks
    case Map.lookup c blks of
        Just x -> return x
        Nothing -> error $ "No such block: " ++ show c

assign :: ShortByteString -> Operand -> Codegen ()
assign var x = do
    lcls <- gets symtab
    modify $ \s -> s {symtab = [(var, x)] ++ lcls}

getVar :: ShortByteString -> Codegen Operand
getVar var = do
    syms <- gets symtab
    case lookup var syms of
        Just x -> return x
        Nothing -> error $ "Local variable not in scope: " ++ show var