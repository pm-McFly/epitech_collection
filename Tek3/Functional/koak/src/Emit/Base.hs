module Emit.Base where

import LLVM.AST
import LLVM.AST.AddrSpace
import qualified LLVM.AST.Constant as C

double :: Type
double = FloatingPointType DoubleFP

void :: Type
void = VoidType

local :: Type -> Name -> Operand
local = LocalReference

global :: Type -> Name -> C.Constant
global = C.GlobalReference

externRef :: Type -> Name -> Operand
externRef refType name = ConstantOperand (C.GlobalReference refType name)

getFunctionType :: [Type] -> Type
getFunctionType types = PointerType (FunctionType double types False) (AddrSpace 0)

toConstant :: C.Constant -> Operand
toConstant = ConstantOperand