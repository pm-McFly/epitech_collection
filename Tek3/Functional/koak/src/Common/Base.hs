module Common.Base
    (   Type (..)
    ,   Node (..)
    ,   Name
    ,   Line
    ,   Token
    ,   display
    ) where

type Name = String



type Line = Int
type Token = (String, Line)

data Type
    = Void
    | Fixed       Int
    | Floating    Double
    deriving (Eq, Show)

-- instance Show Type where
--     show Void = "void"
--     show (Fixed num) = show num
--     show (Floating num) = show num

data Node = Null
    -- Instruction block
    | Block     [Node]

    -- Instructions
    | Constant  Type    Line
    | Var       Name    Line
    | Operator  Name    Line
    | Unary     Name    Node    Line
    | Binary    Name    Node    Node    Line

    -- Statements
    | Function  Name    [Name]  Node    Line
    | Call      Name    [Node]  Line
    | Extern    Name    [Name]  Line
    | If        Node    Node    Node    Line
    | For       Name    Node    Node    Node    Node    Line
    | BinaryDef Name    Name    Name    Node    Line
    | UnaryDef  Name    Name    Node    Line
    | Let       [(Name, Node)]  Node    Line
    -- deriving (Eq)
    deriving (Eq, Show)

-- instance Show Node where
--     show any = display any 0

display :: Node -> Int -> String
display Null tabs = ""

display (Block []) _ = []
display (Block [x]) tabs = getTabs tabs ++ display x tabs
display (Block (x:xs)) tabs = getTabs tabs ++ display x tabs ++ "\n" ++ display (Block xs) tabs

display (Constant var _) _ = show var
display (Var name _) _ = '@' : name
display (Operator name _) _ = name

display (Binary name left right _) tabs = display left tabs ++ " " ++ name ++ " " ++ display right tabs
display (Unary name arg _) tabs = name ++ " " ++ display arg tabs

display (Call name args _) tabs =  name ++ "(" ++ displayArguments args tabs ++ ")"

display (If condition body Null _) tabs
    =  getTabs tabs ++ "if " ++ displaySingleLineBlock condition tabs ++ " then\n"
        ++ display body (tabs + 1) ++ ";"
display (If condition body body2 _) tabs
    =  getTabs tabs ++ "if " ++ displaySingleLineBlock condition tabs ++ " then\n"
        ++ display body (tabs + 1)
    ++ getTabs tabs ++ "\nelse\n"
        ++ display body2 (tabs + 1) ++ ";"

display (For var expr condition increment body _) tabs
    =  "for " ++ var ++ " = " ++ displaySingleLineBlock expr tabs ++ ", " ++ displaySingleLineBlock condition tabs ++ ", " ++ displaySingleLineBlock increment tabs ++ " in\n"
        ++ display body (tabs + 1) ++ ";"

display (Function name args body _) tabs
    =  "def " ++ name ++ "(" ++ displayArgs args ++ ")\n"
        ++ display body (tabs + 1) ++ ";"

display (Let decls body _) tabs
    =  "var " ++ displayDeclaration decls tabs ++ " in\n"
        ++ display body (tabs + 1) ++ ";"

display (Extern name args _) tabs
    =  "extern " ++ name ++ "(" ++ displayArgs args ++ ");"

display (UnaryDef name arg body _) tabs
    =  "#unop " ++ name ++ "(" ++ arg ++ ")\n"
        ++ display body (tabs + 1) ++ ";"

display (BinaryDef name arg1 arg2 body _) tabs
    =  "#binop " ++ name ++ "(" ++ arg1 ++ " " ++ arg2 ++ ")\n"
        ++ display body (tabs + 1) ++ ";"

displaySingleLineBlock :: Node -> Int -> String
displaySingleLineBlock (Block []) _ = []
displaySingleLineBlock (Block [x]) tabs = displaySingleLineBlock x tabs
displaySingleLineBlock (Block (x:xs)) tabs = displaySingleLineBlock x tabs ++ " " ++ displaySingleLineBlock (Block xs) tabs
displaySingleLineBlock other tabs = display other tabs

displayArguments :: [Node] -> Int -> String
displayArguments [] _ = []
displayArguments [x] tabs = displaySingleLineBlock x tabs
displayArguments (x:xs) tabs = displaySingleLineBlock x tabs ++ ", " ++ displayArguments xs tabs

getTabs :: Int -> String
getTabs 0 = ""
getTabs count = '\t' : getTabs (count - 1)

displayArgs :: [String] -> String
displayArgs [] = ""
displayArgs [x] = x
displayArgs (x:xs) = x ++ " " ++ displayArgs xs

displayDeclaration :: [(Name, Node)] -> Int -> String
displayDeclaration [] _ = ""
displayDeclaration [(name, expr)] tabs = name ++ " = " ++ displaySingleLineBlock expr tabs
displayDeclaration ((name, expr):xs) tabs = name ++ " = " ++ displaySingleLineBlock expr tabs ++ ", " ++ displayDeclaration xs tabs