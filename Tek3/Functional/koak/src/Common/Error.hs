module Common.Error where

import Common.Base

type Context = String
type Path = String
type Message = String

data Result a
    = Valid a
    | Error Context Message
    | FileError Message Path Line
    deriving (Eq)

instance (Show a) => Show (Result a) where
    show (Error ctx msg) = ctx ++ ": " ++ msg
    show (FileError msg "" line) = msg ++ "\n\tLine " ++ show line
    show (FileError msg path line) = msg ++ ":\n\tPath " ++ path ++ "\n\tLine " ++ show line
    show (Valid res) = show res

convertError :: (Result a) -> (Result b)
convertError (Error ctx msg) = Error ctx msg
convertError (FileError msg path line) = FileError msg path line
convertError _ = Error "Error::convertError" "Invalid conversion from Valid to Error"

isError :: (Result a) -> Bool
isError (Error _ _) = True
isError (FileError _ _ _) = True
isError other = False