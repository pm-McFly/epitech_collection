module Parser.Parser
    ( parse
    , parseFile
    ) where

import Common.Base
import Common.Error
import Parser.Lexer
import Parser.Constructor
import Parser.Syntax

type BlockTerminator = [String]

parseFile :: String -> String -> Result Node
parseFile path input = let tokens = processLexer input in
    case parse tokens of
        Valid [] -> FileError "Empty file" path 0
        Valid nodes -> case constructNodes nodes of
            Valid cNodes -> Valid $ Block cNodes
            error -> convertError error
        FileError msg _ line -> FileError msg path line
        error -> convertError error

parse :: [Token] -> Result [Node]
parse [] = Error "Parser::parse" "Empty token list"
parse tokens = parseImpl tokens 0

parseImpl :: [Token] -> Line -> Result [Node]
parseImpl [] line = Valid []
parseImpl tokens line = case parseBlockUntil tokens [";", ""] line of
    Valid (node, []) -> Valid [node]
    Valid (node, ((";", endLine):endLeft)) -> case parseImpl endLeft endLine of
        Valid nodes -> Valid $ (node) : nodes
        error -> error
    Valid other -> Error "Parser::parse" "Invalid parseBlock return type"
    error -> convertError error

reduceNode :: Node -> Node
reduceNode node = case node of
    Block [x] -> x
    other -> other

isAcceptableFinalEnd :: [String] -> Bool
isAcceptableFinalEnd [] = True
isAcceptableFinalEnd ("":xs) = True
isAcceptableFinalEnd (x:xs) | null xs = False
isAcceptableFinalEnd (x:xs) | otherwise = isAcceptableFinalEnd xs

isAnyOf :: String -> BlockTerminator -> Bool
isAnyOf str [] = False
isAnyOf str (x:xs) | str == x = True
isAnyOf str (x:xs) | otherwise = isAnyOf str xs

parseBlockUntil :: [Token] -> BlockTerminator -> Line -> Result (Node, [Token])
parseBlockUntil tokens end line = case parseBlockUntilImpl tokens end line of
    Valid (node, left) -> Valid (reduceNode node, left)
    error -> error
    where
        parseBlockUntilImpl :: [Token] -> BlockTerminator -> Line -> Result (Node, [Token])
        parseBlockUntilImpl [] end line | isAcceptableFinalEnd end = Valid (Block [], [])
        parseBlockUntilImpl [] end line | otherwise = FileError ("Couldn't find any end of block token " ++ show end) "" line
        parseBlockUntilImpl tokens@((word, wordLine):xs) end line
            | isAnyOf word end = Valid (Block [], tokens)
            | otherwise = case parseNextToken tokens of
                Valid (node, tokensLeft) -> case parseBlockUntilImpl tokensLeft end line of
                    Valid (Block [], left) -> Valid (Block [node], left)
                    Valid (Block nodes, left) -> Valid (Block (node : nodes), left)
                    error -> error
                error -> error

parseNextToken :: [Token] -> Result (Node, [Token])
parseNextToken [] = Error "Parser::parseNextToken" "Unexpected end of token list"

parseNextToken (("if", line):tokens) = case parseBlockUntil tokens ["then"] line of
    Valid (condition, ((end, thenLine):conditionLeft)) -> case parseBlockUntil conditionLeft ["else", ";"] thenLine of
        Valid (body, bodyLeft@((";", _):_)) -> Valid (If condition body Null line, bodyLeft)
        Valid (body, (("else", elseLine):bodyLeft)) -> case parseBlockUntil bodyLeft [";"] elseLine of
            Valid (body2, body2Left) -> Valid (If condition body body2 line, body2Left)
            error -> error
        error -> error
    error -> error

parseNextToken (("for", line):tokens) = case parseDeclarations tokens [","] line of
    Valid ([(name, expr)], declsLeft) -> case parseBlockUntil declsLeft [","] line of
        Valid (condition, (end:conditionLeft)) -> case parseBlockUntil conditionLeft ["in"] line of
            Valid (increment, (end:incrementLeft)) -> case parseBlockUntil incrementLeft [";"] line of
                Valid (body, bodyLeft) -> Valid (For name expr condition increment body line, bodyLeft)
                error -> error
            error -> error
        error -> error
    Valid _ -> FileError "Invalid for initialization" "" line
    error -> convertError error

parseNextToken (("def", line):(name, nameLine):nameLeft)
    | isWord name = case parseFunctionParameters nameLeft nameLine of
        Valid (args, argsLeft) -> case parseBlockUntil argsLeft [";"] line of
            Valid (body, bodyLeft) -> Valid (Function name args body line, bodyLeft)
            error -> error
        error -> convertError error
    | otherwise = FileError "Expected valid function name" "" line
parseNextToken [("def", line)] = FileError "Expected function name" "" line

parseNextToken (("extern", line):(name, nameLine):nameLeft)
    | isWord name = case parseFunctionParameters nameLeft nameLine of
        Valid (args, argsLeft@((";", _):_)) -> Valid (Extern name args line, argsLeft)
        Valid (_, _) -> FileError "Missing end of 'extern' statement token ';'" "" nameLine
        error -> convertError error
    | otherwise = FileError "Expected extern name" "" line
parseNextToken [("extern", line)] = FileError "Expected extern name" "" line

parseNextToken (("var", line):tokens) = case parseDeclarations tokens [",", "in"] line of
    Valid (decls, declsLeft) -> case parseBlockUntil declsLeft [";"] line of
        Valid (body, bodyLeft) -> Valid (Let decls body line, bodyLeft)
        error -> error
    error -> convertError error

parseNextToken (("#unop", line):(name, nameLine):nameLeft)
    | isAnyOperator name || isWord name = case parseFunctionParameters nameLeft nameLine of
        Valid (args, argsLeft) -> case args of
            [arg] -> case parseBlockUntil argsLeft [";"] line of
                Valid (body, bodyLeft) -> Valid (UnaryDef name arg body line, bodyLeft)
                error -> error
            _ -> FileError "Unexpected number of argument of unary definition" "" line
        error -> convertError error
    | otherwise = FileError "Expected valid unary function name" "" line
parseNextToken [("#unop", line)] = FileError "Expected #unop name" "" line

parseNextToken (("#binop", line):(name, nameLine):nameLeft)
    | isAnyOperator name || isWord name = case parseFunctionParameters nameLeft nameLine of
        Valid (args, argsLeft) -> case args of
            [arg1, arg2] -> case parseBlockUntil argsLeft [";"] line of
                Valid (body, bodyLeft) -> Valid (BinaryDef name arg1 arg2 body line, bodyLeft)
                error -> error
            _ -> FileError "Unexpected number of argument of binary definition" "" line
        error -> convertError error
    | otherwise = FileError "Expected valid binary function name" "" line
parseNextToken [("#binop", line)] = FileError "Expected #binop name" "" line

parseNextToken ((name, line):("(", _):xs) | isWord name = case parseCallParameters xs line of
    Valid (args, left) -> Valid (Call name args line, left)
    error -> convertError error

parseNextToken tokens@((word, line):xs)
    | isAnyOperator word = Valid (Operator word line, xs)
    | isFixed word = Valid (Constant (Fixed $ toFixed word) line, xs)
    | isFloating word = Valid (Constant (Floating $ toFloating word) line, xs)
    | isWord word = Valid ((Var word line), xs)
    | otherwise = FileError ("Couldn't identify token '" ++ word ++ "'") "" line


parseFunctionParameters :: [Token] -> Line -> Result ([Name], [Token])
parseFunctionParameters (("(", beginLine):xs) line = parseFunctionParametersImpl xs beginLine
parseFunctionParameters _ line = FileError "Expected token '(' in parameters list" "" line

parseFunctionParametersImpl :: [Token] -> Line -> Result ([Name], [Token])
parseFunctionParametersImpl ((")", _):xs) line = Valid ([], xs)
parseFunctionParametersImpl ((x, argLine):xs) line | isWord x = case parseFunctionParametersImpl xs line of
    Valid (names, left) -> Valid (x : names, left)
    error -> error
parseFunctionParametersImpl _ line | otherwise = FileError "Invalid parameter list" "" line

parseCallParameters :: [Token] -> Line -> Result ([Node], [Token])
parseCallParameters ((")", _):xs) line = Valid ([], xs)
parseCallParameters tokens line = case parseBlockUntil tokens [",", ")"] line of
    Valid (arg, ((",", _):left)) -> case parseCallParameters left line of
        Valid (args, argsLeft) -> Valid (arg : args, argsLeft)
        error -> error
    Valid (arg, ((")", _):left)) -> Valid ([arg], left)
    error -> convertError error

-- WARNING: This function is intended to be used with at LEAST [","] as end token list
parseDeclarations :: [Token] -> [String] -> Line -> Result ([(Name, Node)], [Token])
parseDeclarations ((name, nameLine):("=", _):nameLeft) end line
    | isWord name = case parseBlockUntil nameLeft end line of
        Valid (expr, ((",", exprLine):exprLeft)) -> if length end == 1
            then Valid ([(name, expr)], exprLeft)
            else case parseDeclarations exprLeft end exprLine of
                Valid (decls, declsLeft) -> Valid ((name, expr) : decls, declsLeft)
                error -> error
        Valid (expr, ((end, _):exprLeft)) -> Valid ([(name, expr)], exprLeft)
        error -> convertError error
    | otherwise = FileError ("Invalid token in declaration " ++ name) "" line
parseDeclarations _ _ line = FileError "Invalid declaration" "" line
