module Parser.Constructor
    ( constructNodes
    , constructNode
    , shuntingYard
    , processOperator
    , buildAst
    ) where

import Common.Base
import Common.Error
import Parser.Syntax

constructNodes :: [Node] -> Result [Node]
constructNodes [] = Valid []
constructNodes (x:xs) = case constructNode x of
    Valid node -> case constructNodes xs of
        Valid nodes -> Valid (node : nodes)
        error -> error
    error -> convertError error

constructNamedNodes :: [(Name, Node)] -> Result [(Name, Node)]
constructNamedNodes [] = Valid []
constructNamedNodes ((name, node):left) = case constructNode node of
    Valid validNode -> case constructNamedNodes left of
        Valid nodes -> Valid ((name, validNode) : nodes)
        error -> error
    error -> convertError error

constructNode :: Node -> Result Node
constructNode node@(Var value _) = Valid node
constructNode node@(Constant value _) = Valid node

-- Construct If node
constructNode node@(If condition body body2 line) = case constructNode condition of
    Valid validCondition -> case constructNode body of
        Valid validBody -> case constructNode body2 of
            Valid validBody2 -> Valid (If validCondition validBody validBody2 line)
            error -> error
        error -> error
    error -> error

-- Construct Function node
constructNode node@(Function name args body line) = case constructNode body of
    Valid validBody -> Valid (Function name args validBody line)
    error -> error

-- Construct Call node
constructNode node@(Call name args line) = case constructNodes args of
    Valid validArgs -> Valid (Call name validArgs line)
    error -> convertError error

-- Construct Let node
constructNode node@(Let args body line) = case constructNamedNodes args of
    Valid validArgs -> case constructNode body of
        Valid validBody -> Valid (Let validArgs validBody line)
        error -> error
    error -> convertError error

-- Construct For node
constructNode node@(For name init condition body body2 line) = case constructNode init of
    Valid validInit -> case constructNode condition of
        Valid validCondition -> case constructNode body of
            Valid validBody -> case constructNode body2 of
                Valid validBody2 -> Valid (For name validInit validCondition validBody validBody2 line)
                error -> error
            error -> error
        error -> error
    error -> error

-- Construct BinaryDef node
constructNode node@(BinaryDef operator a b body line) = case constructNode body of
    Valid validBody -> Valid (BinaryDef operator a b validBody line)
    error -> error

-- Construct UnaryDef node
constructNode node@(UnaryDef operator a body line) = case constructNode body of
    Valid validBody -> Valid (UnaryDef operator a validBody line)
    error -> error

-- Construct Block node
constructNode (Block []) = Error "Parser::constructNode" "Empty block"
constructNode (Block nodes) = case shuntingYard nodes of
    Valid npi -> buildAst npi
    error -> convertError error

-- Any other (Extern, ...)
constructNode node = Valid node

popOperatorImpl :: [Node] -> [Node] -> [Node] -> Node -> ([Node], [Node], [Node])
popOperatorImpl nodes opds opts op = let (newOpds, newOpts, newNodes) = popOperator nodes opds opts in
    processOperator newNodes newOpds newOpts op op

insertParen :: [Node] -> [Node]
insertParen [] = [(Operator ")" 0)]
insertParen [node] = (node:[(Operator ")" 0)])
insertParen (node:nodes) = (node:(Operator ")" 0):nodes)

processMinusOperator :: [Node] -> [Node] -> [Node] -> Node -> Node -> ([Node], [Node], [Node])
processMinusOperator nodes opds opts op (Operator _ _) = processBasicOperator (insertParen nodes) ((Constant (Fixed 0) 0):opds) ((Operator "(" 0):opts) op
processMinusOperator nodes opds opts op _ = processBasicOperator nodes opds opts op

processBasicOperator :: [Node] -> [Node] -> [Node] -> Node -> ([Node], [Node], [Node])
processBasicOperator nodes opds [] op = (opds, [op], nodes)
processBasicOperator nodes opds originalOpts@(opt:opts) op
    | getName opt /= "(" && precedence opt > precedence op = popOperatorImpl nodes opds originalOpts op
    | getName opt /= "(" && (precedence opt == precedence op && leftAssociative op) = popOperatorImpl nodes opds originalOpts op
    | otherwise = (opds, (op:originalOpts), nodes)

processOperator :: [Node] -> [Node] -> [Node] -> Node -> Node -> ([Node], [Node], [Node])
processOperator nodes [] [] op@(Operator "-" line) lastNode = processBasicOperator nodes [(Constant (Fixed 0) line)] [] op
processOperator nodes opds [] op lastNode = (opds, [op], nodes)
processOperator nodes opds opts op@(Operator "(" _) lastNode = (opds, (op:opts), nodes)
processOperator nodes opds opts op@(Operator ")" _) lastNode = popOperatorParen nodes opds opts
processOperator nodes opds opts op@(Operator "-" _) lastNode = processMinusOperator nodes opds opts op lastNode
processOperator nodes opds originalOpts@(opt:opts) op lastNode = processBasicOperator nodes opds originalOpts op

-- Pop opts until paren
popOperatorParen :: [Node] -> [Node] -> [Node] -> ([Node], [Node], [Node])
popOperatorParen nodes opds (opt@(Operator "(" _):opts) = (opds, opts, nodes)
popOperatorParen nodes opds [] = (opds, [], nodes)
popOperatorParen nodes opds (opt:opts) = popOperatorParen nodes (opt:opds) opts

-- Pop opts into opds
popOperator :: [Node] -> [Node] -> [Node] -> ([Node], [Node], [Node])
popOperator nodes opds [] = (opds, [], nodes)
popOperator nodes opds (opt:opts) = ((opt:opds), opts, nodes)


-- Shunting Yard
shuntingYard :: [Node] -> Result [Node]
shuntingYard nodes = shuntingYardImpl nodes [] [] (Operator "(" 0)

shuntingYardImpl :: [Node] -> [Node] -> [Node] -> Node-> Result [Node]
shuntingYardImpl [] [] [] lastNode = Valid []
-- Operator token -> add in opts queue
shuntingYardImpl (node@(Operator op line):nodes) opds opts lastNode =
    let (newOpds, newOpts, newNodes) = processOperator nodes opds opts node lastNode in
        shuntingYardImpl newNodes newOpds newOpts node
-- Operand token -> add in opds queue
shuntingYardImpl (node:nodes) opds opts lastNode = case constructNode node of
    Valid res -> shuntingYardImpl nodes (res:opds) opts node
    error -> convertError error
-- No token to read and opts empty -> return operands stack
shuntingYardImpl [] opds [] (Operator ")" _) = Valid (reverse opds)
shuntingYardImpl [] opds [] (Operator _ _) = Error "Constructor::ShuntingYard" "Invalid number of token"
shuntingYardImpl [] opds [] lastNode = Valid (reverse opds)
-- No token to read and opts not empty -> pop operators to opds
shuntingYardImpl [] opds opts lastNode = let (newOpds, newOpts, newNodes) = popOperator [] opds opts in
    shuntingYardImpl newNodes newOpds newOpts lastNode


-- Construct AST

buildNode :: [Node] -> Int -> Node -> ([Node], Int)
buildNode npi idx op@(Operator name _) | isBinaryOperator name = ( (take (idx - 2) npi) ++ ((constructBinary op (npi !! (idx - 2)) (npi !! (idx - 1)))):(drop (idx + 1) npi), idx - 1)
buildNode npi idx op | otherwise = (npi, idx + 1)

buildAst :: [Node] -> Result Node
buildAst [] = Error "Parser::constructNode" "BuildAst: Empty npi input"
buildAst npi = buildAstImpl npi 0

buildAstImpl :: [Node] -> Int -> Result Node
buildAstImpl [head] _ = Valid head
buildAstImpl npi idx
    | idx >= length npi = Error "Parser::constructNode" "buildAst: index out of range"
    | otherwise = let (newNpi, newIdx) = buildNode npi idx (npi !! idx) in
        buildAstImpl newNpi newIdx

constructBinary :: Node -> Node -> Node -> Node
constructBinary (Operator opt line) opd1 opd2 = (Binary opt opd1 opd2 line)

constructUnary :: Node -> Node -> Node
constructUnary (Operator opt line) opd = (Unary opt opd line)

precedence :: Node -> Int
precedence (Operator "!" _) = 10
precedence (Operator "*" _) = 9
precedence (Operator "/" _) = 9
precedence (Operator "+" _) = 8
precedence (Operator "-" _) = 8
precedence (Operator "==" _) = 7
precedence (Operator "!=" _) = 7
precedence (Operator "<" _) = 6
precedence (Operator ">" _) = 6
precedence (Operator "<=" _) = 5
precedence (Operator ">=" _) = 5
precedence (Operator "=" _) = 4
precedence (Operator "(" _) = 0
precedence (Operator ")" _) = 0
precedence (Operator _ _) = 8

leftAssociative :: Node -> Bool
leftAssociative (Operator "+" _) = True
leftAssociative (Operator "-" _) = True
leftAssociative (Operator "*" _) = True
leftAssociative (Operator "/" _) = True
leftAssociative (Operator "!" _) = False
leftAssociative (Operator "=" _) = True
leftAssociative (Operator "==" _) = True
leftAssociative (Operator "!=" _) = True
leftAssociative (Operator "<" _) = True
leftAssociative (Operator ">" _) = True
leftAssociative (Operator "(" _) = True
leftAssociative (Operator ")" _) = True
leftAssociative (Operator _ _) = True

getValid :: Result Node -> Node
getValid (Valid a) = a

getName :: Node -> String
getName (Operator name _) = name