module Parser.Lexer
    (   processLexer
    ) where

import Data.Char

numbers :: String
numbers = "0123456789."

delimiters :: String
delimiters = ",;:"

operators :: String
operators = "+-/*%="

symbolchar :: String
symbolchar = "#abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ_!?<>@" -- extras added for LLVM syntax needs

checkdelimiterchar :: Char -> Bool
checkdelimiterchar  char = elem char delimiters

checknumberchar :: Char -> Bool
checknumberchar  char = elem char numbers

checksymbolchar :: Char -> Bool
checksymbolchar char = elem char symbolchar

checkoperator :: Char -> Bool
checkoperator char = elem char operators

checkcomment :: String -> Bool
checkcomment ('#':x:xs) | isSpace x = True
checkcomment x = False


parse_array :: (Char -> Bool) -> String -> String -> Int -> [(String, Int)]
parse_array fct [] str id = (str, id) : tokenizer [] id
parse_array fct (x:xs) str id
    | fct x = parse_array fct xs (str ++ [x]) id
    | otherwise = (str, id) : tokenizer (x:xs) id

parse_comment :: String -> Int -> [(String, Int)]
parse_comment [] id = tokenizer [] id
parse_comment (x:xs) id
    | x == '\n' = tokenizer xs (id + 1)
    | otherwise = parse_comment xs id

tokenizer :: String -> Int -> [(String, Int)]
tokenizer [] id = []
tokenizer tokens@(x:xs) id
    | x == '\n' = tokenizer xs (id + 1) -- new line
    | isSpace x = tokenizer xs id -- check for 2,4,8 manual tab
    | x == '(' || x == ')' = ([x], id) : tokenizer xs id -- record ()
    | checkcomment tokens = parse_comment xs id
    | checkdelimiterchar x = parse_array (checkdelimiterchar) xs [x] id
    | checkoperator x = parse_array (checkoperator) xs [x] id
    | checksymbolchar x = parse_array (checksymbolchar) xs [x] id
    | checknumberchar x = parse_array (checknumberchar) xs [x] id
    | otherwise = tokenizer xs id

processLexer :: String -> [(String, Int)]
processLexer file_content = tokenizer file_content 0