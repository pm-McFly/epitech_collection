module Parser.Syntax where

import Data.Char

isEndOfBlock :: String -> Bool
isEndOfBlock "else" = True
isEndOfBlock "then" = True
isEndOfBlock "in" = True
isEndOfBlock "," = True
isEndOfBlock ";" = True
isEndOfBlock _ = False

isUnaryOperator :: String -> Bool
isUnaryOperator "!" = True
isUnaryOperator "-" = True
isUnaryOperator _ = False

isBinaryOperator :: String -> Bool
isBinaryOperator "+" = True
isBinaryOperator "-" = True
isBinaryOperator "*" = True
isBinaryOperator "/" = True
isBinaryOperator "%" = True
isBinaryOperator "=" = True
isBinaryOperator "==" = True
isBinaryOperator "!=" = True
isBinaryOperator "<" = True
isBinaryOperator ">" = True
isBinaryOperator op = not (isEndOfBlock op || isWord op || isSpecialOperator op || isFloating op || isFixed op)

isSpecialOperator :: String -> Bool
isSpecialOperator "(" = True
isSpecialOperator ")" = True
isSpecialOperator _ = False

isAnyOperator :: String -> Bool
isAnyOperator str = isUnaryOperator str || isBinaryOperator str || isSpecialOperator str || isEndOfBlock str

isWord :: String -> Bool
isWord "" = False
isWord str = all isAlphaNum str

isFixed :: String -> Bool
isFixed "" = False
isFixed str = all isDigit str

isFloating :: String -> Bool
isFloating str = not (null str) && isFloatingImpl str False
    where
        isFloatingImpl :: String -> Bool -> Bool
        isFloatingImpl [] hasDot = True
        isFloatingImpl (x:xs) hasDot | isDigit x = isFloatingImpl xs hasDot
        isFloatingImpl (x:xs) hasDot | x == '.' && not hasDot = isFloatingImpl xs True
        isFloatingImpl str hasDot | otherwise = False

toFixed :: String -> Int
toFixed str = toFixedImpl str 0
    where
        toFixedImpl :: String -> Int -> Int
        interpretNum :: String -> Int -> Int

        toFixedImpl [] num = num
        toFixedImpl str num
            | isDigit $ head str = interpretNum str num
            | otherwise = error ("Invalid character in fixed " ++ [head str])

        interpretNum (x:xs) num = toFixedImpl xs (num * 10 + digitToInt x)

toFloating :: String -> Double
toFloating str = toFloatingImpl str 0 0.0
    where
        toFloatingImpl :: String -> Double -> Double -> Double
        interpretNum :: String -> Double -> Double -> Double
        digitToFloat :: Char -> Double

        toFloatingImpl [] num depth = num
        toFloatingImpl str num depth | isDigit $ head str = interpretNum str num depth
        toFloatingImpl (x:xs) num depth | x == '.' && depth == 0.0 = toFloatingImpl xs num 10.0
        toFloatingImpl str num depth | otherwise = error ("Invalid character in floating " ++ [head str])

        interpretNum (x:xs) num depth | depth == 0.0 = toFloatingImpl xs (num * 10 + digitToFloat x) depth
        interpretNum (x:xs) num depth | otherwise = toFloatingImpl xs (num + digitToFloat x * (1.0 / depth)) (depth * 10.0)

        digitToFloat c = fromInteger $ fromIntegral $ digitToInt c :: Double
