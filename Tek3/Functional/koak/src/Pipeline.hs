module Pipeline
    ( run
    ) where

import Prelude

import qualified LLVM.AST as AST
import LLVM.Module
import LLVM.Target
import LLVM.Context
import LLVM.Analysis

import Common.Base
import Common.Error
import Parser.Parser
import Emit.Emit

run :: String -> String -> IO ()
run path input = case parseFile path input of
    Valid root -> do
        let fpath = path ++ ".o"
        Prelude.putStrLn (fpath ++ ": Parsing success")
        print root
        Prelude.putStrLn ""
        generate (fpath) (emit root)
        Prelude.putStrLn (fpath ++ ": Compilation success")
    err -> error $ show err

generate :: String -> AST.Module -> IO ()
generate file mod = do
    withContext $ \ctx ->
        withModuleFromAST ctx mod $ \llvmMod -> do
            verify llvmMod
            withHostTargetMachineDefault $ \tm -> writeObjectToFile tm (File file) llvmMod