module LexerSpec where

import Test.Hspec
import Control.Exception as Control

import Common.Base
import Common.Error
import Parser.Lexer

spec :: Spec
spec = do
    describe "Basics" $ do
        it "Simple sentence" $ do
            (processLexer "Hello, world") `shouldBe` [("Hello", 0), (",", 0), ("world", 0)]
        it "Sentence with calculations" $ do
            (processLexer "Hello, 1+1") `shouldBe` [("Hello", 0), (",", 0), ("1", 0), ("+", 0), ("1", 0)]
        it "Simple calculation stick" $ do
            (processLexer "1+11") `shouldBe` [("1", 0), ("+", 0), ("11", 0)]
        it "Simple calculation spaced" $ do
            (processLexer "1 + 11") `shouldBe` [("1", 0), ("+", 0), ("11", 0)]
        it "Newline test" $ do
            (processLexer "Blanc\nNoir") `shouldBe` [("Blanc", 0), ("Noir", 1)]
        it "Complex numbers" $ do
            (processLexer "(2.789327, 1213 1313)") `shouldBe` [("(", 0), ("2.789327", 0), (",", 0), ("1213", 0), ("1313", 0), (")", 0)]
        it "Comment" $ do
            (processLexer "# this is a comment\ndef binary : 1 (x y) y;") `shouldBe` [("def", 1), ("binary", 1), (":", 1), ("1", 1), ("(", 1), ("x", 1), ("y", 1), (")", 1), ("y", 1), (";", 1)]
        it "Hard calculation" $ do
            (processLexer "2 + (2*21)/12") `shouldBe` [("2", 0), ("+", 0), ("(", 0), ("2", 0), ("*", 0), ("21", 0), (")", 0), ("/", 0), ("12", 0)]

    describe "Medium" $ do
        it "Small kaleidoscop code file" $ do
            (processLexer "# Define ':' for sequencing: as a low-precedence operator that ignores operands\n\
            \# and just returns the RHS.\n\
            \def binary : 1 (x y) y;\n\
            \# Recursive fib, we could do this before.\n\
            \def fib(x)\n\
            \    if (x < 3) then\n\
            \        1\n\
            \    else\n\
            \        fib(x-1)+fib(x-2);\n\
            \# Iterative fib.\n\
            \def fibi(x)\n\
            \    var a = 1, b = 1, c = 0 in\n\
            \    (for i = 3, i < x, 1.0 in\n\
            \        c = (a + b) : \n\
            \        a = b : \n\
            \        b = c) : \n\
            \    b;\n\
            \# Call it.\n\
            \fibi(10);\n") `shouldBe` [("def", 2), ("binary", 2), (":", 2), ("1", 2), ("(", 2), ("x", 2), ("y", 2), (")", 2), ("y", 2), (";", 2), ("def", 4), ("fib", 4), ("(", 4), ("x", 4), (")", 4), ("if", 5), ("(", 5), ("x", 5), ("<", 5), ("3", 5), (")", 5),("then", 5), ("1", 6), ("else", 7), ("fib", 8), ("(", 8), ("x", 8), ("-", 8), ("1", 8), (")", 8), ("+", 8), ("fib", 8), ("(", 8), ("x", 8), ("-", 8), ("2", 8), (")", 8), (";", 8), ("def", 10), ("fibi", 10), ("(", 10), ("x", 10), (")", 10), ("var", 11), ("a", 11), ("=", 11), ("1", 11), (",", 11), ("b", 11), ("=", 11), ("1", 11), (",", 11), ("c", 11), ("=", 11), ("0", 11), ("in", 11), ("(", 12), ("for", 12), ("i", 12), ("=", 12), ("3", 12), (",", 12), ("i", 12), ("<", 12), ("x", 12), (",", 12), ("1.0", 12), ("in", 12), ("c", 13), ("=", 13), ("(", 13), ("a", 13), ("+", 13), ("b", 13), (")", 13), (":", 13), ("a", 14), ("=", 14), ("b", 14), (":", 14), ("b", 15), ("=", 15), ("c", 15), (")", 15), (":", 15), ("b", 16), (";", 16), ("fibi", 18), ("(", 18), ("10", 18), (")", 18), (";", 18)]
        it "Simple kale script" $ do
            (processLexer "    for i = 1, i < n, 1.0 in\n    putchard(42);  # ascii 42 = '*'") `shouldBe` [("for", 0), ("i", 0), ("=", 0), ("1", 0), (",", 0), ("i", 0), ("<", 0), ("n", 0), (",", 0), ("1.0", 0), ("in", 0), ("putchard", 1), ("(", 1), ("42", 1), (")", 1), (";", 1)]

    describe "Hard tests" $ do
        it "Operation hard" $ do
            (processLexer "def foo(a b) a*a + 2*a*b + b*b") `shouldBe` [("def", 0), ("foo", 0), ("(", 0), ("a", 0), ("b", 0), (")", 0), ("a", 0), ("*", 0), ("a", 0), ("+", 0),("2", 0), ("*", 0), ("a", 0), ("*", 0), ("b", 0), ("+", 0), ("b", 0), ("*", 0), ("b", 0)]