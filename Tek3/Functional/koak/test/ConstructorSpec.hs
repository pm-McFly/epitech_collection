module ConstructorSpec where

import Test.Hspec
import Control.Exception as Control

import Common.Base
import Common.Error
import Parser.Constructor

-- ::l Constructor.hs
cneg = Constant (Fixed (-1)) 0
c0 = Constant (Fixed 0) 0
c1 = Constant (Fixed 1) 0
c2 = Constant (Fixed 2) 0
c3 = Constant (Fixed 3) 0
c4 = Constant (Fixed 4) 0
c5 = Constant (Fixed 5) 0
op0 = Operator "-" 0
op1 = Operator "+" 0
op2 = Operator "*" 0
op3 = Operator "/" 0
op4 = Operator "^" 0
op5 = Operator "!" 0
lparen = Operator "(" 0
rparen = Operator ")" 0

spec :: Spec
spec = do
    describe "Basics" $ do
        it "Construct Var" $ do
            (constructNode $ Var "abc" 0) `shouldBe` (Valid $ Var "abc" 0)
        it "Construct Addition" $ do
            (constructNode $ Block [c1, op1, c1]) `shouldBe` (Valid $ Binary "+" c1 c1 0)
        it "Construct Unary minus" $ do
            (constructNode $ Block [(Operator "-" 0), c3]) `shouldBe` (Valid $ Binary "-" c0 c3 0)

    describe "Medium" $ do
    --     it "Construct Addition" $ do
    --         (constructNode $ Block [c1, op1, (If c1 ]) `shouldBe` (Valid $ Binary "+" c1 c1 0)
        it "Construct Addition & Mult" $ do
            (constructNode $ Block [c1, op1, c2, op2, c4]) `shouldBe` (Valid $ Binary "+" c1 (Binary "*" c2 c4 0) 0)
        it "Construct Addition & Mult" $ do
            (constructNode $ Block [c1, op2, c2, op1, c4]) `shouldBe` (Valid $ Binary "+" (Binary "*" c1 c2 0) c4 0)

    describe "Shunting Yard" $ do
        it "Basic - 1" $ do
            (shuntingYard [c1, op1, c1]) `shouldBe` Valid ([c1, c1, op1])
        it "Basic - 2" $ do
            (shuntingYard [c1, op1, c2, op1, c3]) `shouldBe` Valid ([c1, c2, op1, c3, op1])
        it "Basic - Negative - 0" $ do
            (shuntingYard [op0, c1]) `shouldBe` Valid ([c0, c1, op0])
        it "Basic - Negative - 1" $ do
            (shuntingYard [c1, op0, c1]) `shouldBe` Valid ([c1, c1, op0])
        it "Basic - Negative - 2" $ do
            (shuntingYard [c1, op1, op0, c2]) `shouldBe` Valid ([c1, c0, c2, op0, op1])
        it "Basic - Negative - priority" $ do
            (shuntingYard [c1, op2, op0, c2]) `shouldBe` Valid ([c1, c0, c2, op0, op2])
--        it "Basic - 2" $ do
--            (shuntingYard [c3, op1, c4, op2, c2, op3, lparen, c1, op0, c5, rparen, op4, c2, op4, c3]) `shouldBe` Valid ([c3, c4, c2, op2, c1, c5, op0, c2, c3, op4, op4, op3, op1])
            -- (shuntingYard [c3, op1, c4, op2, c2, op3, lparen, c1, op0, c5, rparen, op4, c2, op4, c3]) `shouldBe` Valid ([c1, c1, op1, c1, op1])
        -- it "Medium" $ do
        --     (shuntingYard [lparen, c1, op1, c1, rparen]) `shouldBe` Valid ([c1, c1, op1])
        it "Addition & mutl priority" $ do
            (shuntingYard [c1, op1, c2, op2, c3]) `shouldBe` Valid ([c1, c2, c3, op2, op1])
        it "Process Operator - Negative" $ do
            (processOperator [] [c1] [op1] op0 op1) `shouldBe` ([c0, c1], [op0, lparen, op1], [rparen])
        it "Process Operator - 1" $ do
            (processOperator [] [c1] [] op1 c1) `shouldBe` ([c1], [op1], [])
        it "Process Operator - Parenthesis 1" $ do
            (processOperator [] [] [] lparen lparen) `shouldBe` ([], [lparen], [])
        -- it "Process Operator - Parenthesis 2" $ do
        --     (processOperator [] [c1] [lparen] op1 c1) `shouldBe` ([c1], [op1, lparen], [])
    describe "Build AST" $ do
        it "Simple Addition" $ do
            (buildAst [c1, c1, op1]) `shouldBe` (Valid (Binary "+" (c1) (c1) 0))