module ParserSpec where

import Test.Hspec
import Control.Exception as Control

import Common.Base
import Common.Error
import Parser.Parser

c0 = Constant (Fixed 0) 0
c1 = Constant (Fixed 1) 0
c2 = Constant (Fixed 2) 0
c3 = Constant (Fixed 3) 0
c4 = Constant (Fixed 4) 0

toTok :: [String] -> [Token]
toTok list = map (\x -> (x, 0)) list

spec :: Spec
spec = do
    describe "Basics" $ do
        it "Empty token list" $ do
            isError (parse []) `shouldBe` True

    describe "Function call" $ do
        it "Valid function call" $ do
            (parse (toTok ["foo", "(", ")"]))
                `shouldBe` Valid [(Call "foo" [] 0)]
            (parse (toTok ["foo", "(", "2", ")"]))
                `shouldBe` Valid [(Call "foo" [c2] 0)]
            (parse (toTok ["foo", "(", "1", ",", "2", ")"]))
                `shouldBe` Valid [(Call "foo" [c1, c2] 0)]
            (parse (toTok ["foo", "(", "1", "+", "2", ")"]))
                `shouldBe` Valid [(Call "foo" [Block [c1, Operator "+" 0, c2]] 0)]

    describe "Function declaration" $ do
        it "Valid Function" $ do
            (parse (toTok ["def", "foo", "(", "x", "y", ")", "x", "+", "y", ";"]))
                `shouldBe` Valid [(Function "foo" ["x", "y"] (Block [Var "x" 0, Operator "+" 0, Var "y" 0]) 0)]
            (parse (toTok ["def", "foo", "(", ")", "1", ";"]))
                `shouldBe` Valid [(Function "foo" [] (c1) 0)]

        it "Invalid Function" $ do
            isError(parse (toTok ["def", "foo", ";"]))
                `shouldBe` True
            isError(parse (toTok ["def", "(", ")", "3", ";"]))
                `shouldBe` True
            isError(parse (toTok ["def", "foo", "(", "x", "y", ";"]))
                `shouldBe` True
--            isError(parse (toTok ["def", "foo", "(", "x", "y", ")", ";"]))
--                `shouldBe` True
            isError (parse (toTok ["def", "foo", "(", "x", "y", ")", "x", "+", "y"]))
                `shouldBe` True


    describe "For Statement" $ do
        it "For" $ do
            (parse (toTok ["for", "i", "=", "0", ",", "i", "<", "1", ",", "2", "in", "3", ";"]))
                `shouldBe` Valid [(For "i" (c0) (Block [Var "i" 0, Operator "<" 0, c1]) (c2) (c3) 0)]

        it "Invalid For" $ do
            isError (parse (toTok ["for", ";"]))
                `shouldBe` True
            isError (parse (toTok ["for", "i", ";"]))
                `shouldBe` True
            isError (parse (toTok ["for", "i", "=", ";"]))
                `shouldBe` True
            isError (parse (toTok ["for", "i", "=", "0", ";"]))
                `shouldBe` True
            isError (parse (toTok ["for", "i", "=", "0", ",", ";"]))
                `shouldBe` True
            isError (parse (toTok ["for", "i", "=", "0", ",", "i", "<", "1", ",", ";"]))
                `shouldBe` True
            isError (parse (toTok ["for", "i", "=", "0", ",", "i", "<", "1", ",", "2", ";"]))
                `shouldBe` True
--            isError (parse (toTok ["for", "i", "=", "0", ",", "i", "<", "1", ",", "2", "in", ";"]))
--                `shouldBe` True
            isError (parse (toTok ["for", "i", "=", "0", ",", "i", "<", "1", ",", "2", "in", "3"]))
                `shouldBe` True

    describe "If Statement" $ do
        it "Valid If" $ do
            (parse (toTok ["if", "1", "<", "2", "then", "3", "else", "4", ";"]))
                `shouldBe` Valid [(If (Block [c1, Operator "<" 0, c2]) (c3) (c4) 0)]

        it "Invalid If" $ do
            isError (parse (toTok ["if", ";"]))
                `shouldBe` True
            isError (parse (toTok ["if", "1", "<", "2", ";"]))
                `shouldBe` True
--            isError (parse (toTok ["if", "1", "<", "2", "then", ";"]))
--                `shouldBe` True
--            isError (parse (toTok ["if", "1", "<", "2", "then", "3", "else", ";"]))
--                `shouldBe` True
            isError (parse (toTok ["if", "1", "<", "2", "then", "3", "else", "4"]))
                `shouldBe` True

    describe "Var statement" $ do
        it "Valid Var" $ do
            (parse (toTok ["var", "a", "=", "1", "in", "a", ";"]))
                `shouldBe` Valid [(Let [("a", c1)] (Var "a" 0) 0)]
            (parse (toTok ["var", "a", "=", "1", ",", "b", "=", "2", "in", "a", "+", "b", ";"]))
                `shouldBe` Valid [(Let [("a", c1), ("b", c2)] (Block [Var "a" 0, Operator "+" 0, Var "b" 0]) 0)]

        it "Invalid Var" $ do
            isError (parse (toTok ["var", "a", "1", ",", "b", "=", "2", "in", "a", "+", "b", ";"]))
                `shouldBe` True
            isError (parse (toTok ["var", "a", "1", ",", "a", ";"]))
                `shouldBe` True
            isError (parse (toTok ["var", "a", "=", "1", ",", "b", "=", "2", ";"]))
                `shouldBe` True
--            isError (parse (toTok ["var", "a", "=", "1", ",", "b", "=", "2", "in", ";"]))
--                `shouldBe` True
            isError (parse (toTok ["var", "a", "=", "1", ",", "b", "=", "2", "in", "a", "+", "b"]))
                `shouldBe` True

    describe "Extern statement" $ do
        it "Valid Extern" $ do
            (parse (toTok ["extern", "foo", "(", ")", ";"]))
                `shouldBe` Valid [(Extern "foo" [] 0)]
            (parse (toTok ["extern", "foo", "(", "x", ")", ";"]))
                `shouldBe` Valid [(Extern "foo" ["x"] 0)]
            (parse (toTok ["extern", "foo", "(", "x", "y", ")", ";"]))
                `shouldBe` Valid [(Extern "foo" ["x", "y"] 0)]

        it "Invalid Extern" $ do
            isError (parse (toTok ["extern", ";"]))
                `shouldBe` True
            isError (parse (toTok ["extern", "foo", ";"]))
                `shouldBe` True
            isError (parse (toTok ["extern", "foo", "x", ";"]))
                `shouldBe` True
            isError (parse (toTok ["extern", "foo", "x", "y", ")", ";"]))
                `shouldBe` True
            isError (parse (toTok ["extern", "foo", "(", "x", "y", ";"]))
                `shouldBe` True
            isError (parse (toTok ["extern", "foo", "(", "x", "y", ")"]))
                `shouldBe` True

    describe "Unary definition" $ do
        it "Valid Unary" $ do
            (parse (toTok ["#unop", "!", "(", "x", ")", "x", ";"]))
                `shouldBe` Valid [(UnaryDef "!" "x" (Var "x" 0) 0)]

        it "Invalid Unary" $ do
            isError (parse (toTok ["#unop", "(", "x", ")", ";"]))
                `shouldBe` True
            isError (parse (toTok ["#unop", "!", "x", ")", ";"]))
                `shouldBe` True
            isError (parse (toTok ["#unop", "!", "(", "x", ";"]))
                `shouldBe` True
            isError (parse (toTok ["#unop", "!", "(", "x", ")", "x"]))
                `shouldBe` True
            isError (parse (toTok ["#unop", "!", "(", "x", "y", ")", "x", ";"]))
                `shouldBe` True
            isError (parse (toTok ["#unop", "!", "(", ")", "x", ";"]))
                `shouldBe` True

    describe "Binary definition" $ do
        it "Valid Binary" $ do
            (parse (toTok ["#binop", "*", "(", "x", "y", ")", "0", ";"]))
                `shouldBe` Valid [(BinaryDef "*" "x" "y" (c0) 0)]

        it "Invalid Binary" $ do
            isError (parse (toTok ["#binop", "*", "(", ")", "0", ";"]))
                `shouldBe` True
            isError (parse (toTok ["#binop", "*", "(", "x", "y", "z", ")", "0", ";"]))
                `shouldBe` True
            isError (parse (toTok ["#binop", "*", "x", "y", ")", "0", ";"]))
                `shouldBe` True
            isError (parse (toTok ["#binop", "(", "x", "y", ")", "0", ";"]))
                `shouldBe` True
            isError (parse (toTok ["#binop", "*", "(", "x", ")", "0", ";"]))
                `shouldBe` True
            isError (parse (toTok ["#binop", "*", "x", "y", ")", "0", ";"]))
                `shouldBe` True
            isError (parse (toTok ["#binop", "*", "(", "x", "y", "0", ";"]))
                `shouldBe` True
            isError (parse (toTok ["#binop", "*", "(", "x", "y", ")", "0"]))
                `shouldBe` True