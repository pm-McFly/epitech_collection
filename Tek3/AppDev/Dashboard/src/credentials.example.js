// Firebase Config
import firebase from 'firebase';

export const config = {
  apiKey: 'API_KEY',
  projectId: 'PROJECT_ID',
  databaseURL: 'DATABASE_URL',
  authDomain: 'AUTH_DOMAIN',
  storageBucket: 'STORAGE_BUCKET',
  messagingSenderId: 'MESSAGING_SENDER_ID',
  appId: 'APPLICATION_ID',
  measurementId: 'MEASUREMENT_ID',
};

export const registeredApp = firebase.initializeApp(config);
export const database = firebase.database();
