import React from 'react';
import { BrowserRouter as Router, Route } from 'react-router-dom';

import {
  FirebaseAuthProvider,
  IfFirebaseAuthed,
  IfFirebaseUnAuthed,
} from '@react-firebase/auth';
import firebase from 'firebase';

import { fireconfig } from './credentials';

import HomePage from './Components/Pages/Home';
import BasePage from './Components/Elements/BasePage';
import Login from './Components/Pages/Login';
import PrivacyPolicy from './Components/Pages/Policy';
import Help from './Components/Pages/Help';
import Example from './Components/Pages/Example';
import CreateUser from './Components/Pages/Login/Components/CreateUser';
import CheckUser from './Components/Pages/Login/Components/CheckUser';

const RouterApp = () => (
  <FirebaseAuthProvider {...fireconfig} firebase={firebase}>
    <IfFirebaseAuthed>
      <Router>
        <BasePage>
          <Route path="/" exact component={HomePage} />
          <Route path="/privacy" exact component={PrivacyPolicy} />
          <Route path="/help" exact component={Help} />
          <Route path="/example" exact component={Example} />
          <Route path="/createuser" exact component={CreateUser} />
          <Route path="/checkuser" exact component={CheckUser} />
        </BasePage>
      </Router>
    </IfFirebaseAuthed>
    <IfFirebaseUnAuthed>
      <Router>
        <BasePage>
          <Route path="*" exact component={Login} />
          <Route path="/example" exact component={Example} />
          <Route path="/privacy" exact component={PrivacyPolicy} />
          <Route path="/help" exact component={Help} />
        </BasePage>
      </Router>
    </IfFirebaseUnAuthed>
  </FirebaseAuthProvider>
);

export default RouterApp;
