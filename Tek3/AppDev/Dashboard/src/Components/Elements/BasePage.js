import React, { useState } from 'react';
import PropTypes from 'prop-types';

import {
  Layout,
} from 'antd';

import BasePageBreadcrumb from './BasePageBreadcrumb';
import BasePageFooter from './BasePageFooter';
import BasePageMenu from './BasePageMenu';

const {
  Content, Sider,
} = Layout;

const BasePage = ({ children }) => {
  const [collapse, setCollapse] = useState(true);

  const toggle = () => {
    setCollapse(!collapse);
  };
  return (
    <Layout style={{ minHeight: '100vh' }}>
      <Sider collapsible collapsed={collapse} onCollapse={toggle}>
        <BasePageMenu />
      </Sider>
      <Layout>
        <Content style={{ margin: '0 16px' }}>
          <BasePageBreadcrumb />
          <div style={{
            padding: 24,
            background: '#fff',
            minHeight: 360,
          }}
          >
            {children}
          </div>
        </Content>
        <BasePageFooter />
      </Layout>
    </Layout>
  );
};

BasePage.propTypes = {
  children: PropTypes.node.isRequired,
};

export default BasePage;
