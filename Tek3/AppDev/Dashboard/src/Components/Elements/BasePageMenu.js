import firebase from 'firebase';

import React from 'react';
import { useHistory } from 'react-router-dom';

import { Menu, Icon } from 'antd';

const { SubMenu } = Menu;

const BasePageMenu = () => {
  const history = useHistory();

  const logout = () => {
    firebase
      .app()
      .auth()
      .signOut()
      .then(
        history.push('/'),
      )
      .catch((e) => {
        console.log(`Error: ${e}`);
      });
  };

  const onClick = (location) => () => {
    history.push(location);
  };

  return (
    <Menu theme="dark" mode="inline">

      <Menu.Item key="account" onClick={logout}>
        {firebase.auth().currentUser
          ? (
            <>
              <Icon type="logout" />
              <span> LogOut </span>
            </>
          )
          : (
            <>
              <Icon type="user" />
              <span>Use Login Method</span>
            </>
          )}
      </Menu.Item>

      <Menu.Item key="home" onClick={onClick('/')}>
        <Icon type="home" />
        <span>Home</span>
      </Menu.Item>

      <SubMenu
        key="widgets"
        title={(
          <span>
            <Icon type="build" />
            <span>Widgets List</span>
          </span>
        )}
      >

        <SubMenu
          key="services"
          title={(
            <span>
              <Icon type="tool" />
              <span>List of Services</span>
            </span>
          )}
        >
          <SubMenu
            key="utilities"
            title={(
              <span>
                <Icon type="tool" />
                <span>Utilities</span>
              </span>
            )}
          >
            <Menu.Item key="Note">
              <span>
                <Icon type="file-text" />
                <span>Note</span>
              </span>
            </Menu.Item>
            <Menu.Item key="PDFConverter">
              <span>
                <Icon type="file-done" />
                <span>URL to PDF converter</span>
              </span>
            </Menu.Item>
          </SubMenu>
          <SubMenu
            key="Reddit"
            title={(
              <span>
                <Icon type="reddit" />
                <span>Reddit</span>
              </span>
            )}
          >
            <Menu.Item key="RedditClient">
              <span>
                <Icon type="reddit" />
                <span>SubReddit client</span>
              </span>
            </Menu.Item>
          </SubMenu>
          <SubMenu
            key="Pokemon"
            title={(
              <span>
                <Icon type="search" />
                <span>Pokemon info</span>
              </span>
            )}
          >
            <Menu.Item key="PokeStat">
              <span>
                <Icon type="line-chart" />
                <span>Display pokemon stats</span>
              </span>
            </Menu.Item>
          </SubMenu>
          <SubMenu
            key="OpenWeather"
            title={(
              <span>
                <Icon type="cloud" />
                <span>Weather</span>
              </span>
            )}
          >
            <Menu.Item key="OpenWeatherClient">
              <span>
                <Icon type="cloud" />
                <span>OpenWeather client</span>
              </span>
            </Menu.Item>
            <Menu.Item key="AirQuality">
              <span>
                <Icon type="heart" />
                <span>Air quality checker</span>
              </span>
            </Menu.Item>
          </SubMenu>
        </SubMenu>
      </SubMenu>

      <Menu.Item key="example" onClick={onClick('/example')}>
        <Icon type="project" />
        <span>Example</span>
      </Menu.Item>

      <Menu.Item key="privacy" onClick={onClick('/privacy')}>
        <Icon type="security-scan" />
        <span>Privacy Policy</span>
      </Menu.Item>

      <Menu.Item key="FAQ" onClick={onClick('/help')}>
        <Icon type="question" />
        <span>Help</span>
      </Menu.Item>

    </Menu>
  );
};

export default BasePageMenu;
