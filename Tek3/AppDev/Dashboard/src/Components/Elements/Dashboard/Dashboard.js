import React from 'react';
import PropTypes from 'prop-types';
import {
  List,
} from 'antd';

import Section from './Section';

const Dashboard = ({ settings, saveToDatabase, example }) => (
  <>
    <List
      grid={{
        gutter: 16,
        column: 1,
      }}
      dataSource={settings}
      rowKey="Section"
      renderItem={(item) => (
        <List.Item>
          <Section elements={item} saveToDatabase={saveToDatabase} example={example} />
        </List.Item>
      )}
    />
  </>
);

Dashboard.propTypes = {
  settings: PropTypes.arrayOf(
    PropTypes.arrayOf(
      PropTypes.shape({
        type: PropTypes.string.isRequired,
        title: PropTypes.string.isRequired,
        content: PropTypes.shape({
          note: PropTypes.string.isRequired,
          quantity: PropTypes.number.isRequired,
        }).isRequired,
      }).isRequired,
    ).isRequired,
  ).isRequired,
  saveToDatabase: PropTypes.func,
  example: PropTypes.bool.isRequired,
};

export default Dashboard;
