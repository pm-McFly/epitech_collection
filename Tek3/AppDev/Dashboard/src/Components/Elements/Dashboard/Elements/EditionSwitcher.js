import { Switch, Icon } from 'antd';
import React from 'react';
import PropTypes from 'prop-types';

const EditionSwitcher = ({ switchMode, editionMode }) => (
  <div style={{ marginBottom: 16 }}>
    <Switch
      checkedChildren={<Icon type="check" />}
      unCheckedChildren={<Icon type="edit" />}
      onChange={switchMode}
    />
    {editionMode ? ' OK! I\'m done' : ' Edit this section'}
  </div>
);

EditionSwitcher.propTypes = {
  switchMode: PropTypes.func.isRequired,
  editionMode: PropTypes.bool.isRequired,
};

export default EditionSwitcher;
