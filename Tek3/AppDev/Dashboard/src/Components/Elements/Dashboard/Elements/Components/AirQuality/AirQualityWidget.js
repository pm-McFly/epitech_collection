import React, { useEffect, useState } from 'react';
import PropTypes from 'prop-types';

import {
  Spin, Comment, Avatar, Typography,
} from 'antd';

import axios from 'axios';

const { Text } = Typography;

const AirQualityWidget = ({ content }) => {
  const [loaded, setLoaded] = useState(false);
  const [data, setData] = useState('');
  const [refetch, setRefetch] = useState(true);

  useEffect(() => {
    axios.get(`${content.url}`)
      .then((res) => {
        setData(res.data);
        setTimeout(() => {
          setRefetch(!refetch);
        }, content.ttl);
        setLoaded(!loaded);
      })
      .catch((e) => console.error(e));
  }, [refetch]);

  if (loaded) {
    return (
      <>
        {data.results.splice(0, content.quantity || data.length)
          .map((item) => (
            <Comment
              datetime={(
                <Typography>
                  <Text>
                    utc:
                    {' '}
                    {item.date.utc}
                    <br />
                    local:
                    {' '}
                    {item.date.local}
                  </Text>
                </Typography>
              )}
              key={item.value}
              author={`${item.city}, ${item.country}`}
              content={(
                <Typography>
                  <Text strong>
                    {item.parameter}
                    :
                    {' '}
                    {item.value}
                    {item.unit}
                  </Text>
                </Typography>
              )}
              avatar={(
                <Avatar
                  icon={parseFloat(item.value) >= 50 ? 'frown' : 'smile'}
                  alt={item.value}
                  style={{ backgroundColor: (parseFloat(item.value) >= 50 ? 'red' : 'green') }}
                />
              )}
            />
          ))}
      </>
    );
  }
  return <Spin />;
};

AirQualityWidget.propTypes = {
  content: PropTypes.shape({
    url: PropTypes.string.isRequired,
    quantity: PropTypes.number.isRequired,
    ttl: PropTypes.number.isRequired,
  }).isRequired,
};

export default AirQualityWidget;
