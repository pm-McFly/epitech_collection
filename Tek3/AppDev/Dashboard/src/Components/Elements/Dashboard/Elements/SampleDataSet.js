export const testData = [
  [
    {
      level: 0,
      id: 1,
      type: 'note',
      title: 'TODO',
      content: {
        note: 'Sample text to show click settings button in edition mode to modify',
        url: '',
        ttl: 6000000,
        quantity: 0,
      },
      hidden: false,
    },
    {
      level: 0,
      id: 2,
      type: 'empty',
      title: '',
      content: {
        note: '',
        ttl: 6000000,
        quantity: 0,
        url: '',
      },
      hidden: true,
    },
    {
      level: 0,
      id: 3,
      type: 'weather',
      title: 'Les Ulis',
      content: {
        note: '',
        quantity: 0,
        ttl: 6000000,
        url: 'https://api.openweathermap.org/data/2.5/weather?q=Les Ulis&units=metric&appid=27e07daa0e505f99430d85fcab92fbae',
      },
      hidden: false,
    },
    {
      level: 0,
      id: 4,
      type: 'empty',
      title: '',
      content: {
        note: '',
        quantity: 0,
        ttl: 6000000,
        url: '',
      },
      hidden: true,
    },
    {
      level: 0,
      id: 5,
      type: 'PokemonInfo',
      title: 'bulbasaur',
      content: {
        quantity: 0,
        note: '',
        ttl: 6000000,
        url: 'https://pokeapi.co/api/v2/pokemon/bulbasaur',
      },
      hidden: false,
    },
    {
      level: 0,
      id: 6,
      type: 'AirQuality',
      title: 'Paris',
      content: {
        quantity: 0,
        ttl: 6000000,
        note: '',
        url: 'https://api.openaq.org/v1/measurements/?city=Paris',
      },
      hidden: false,
    },
  ],
  [
    {
      level: 1,
      id: 1,
      type: 'reddit',
      title: 'nasa',
      content: {
        quantity: 5,
        ttl: 6000000,
        note: '',
        url: 'https://www.reddit.com/r/nasa.json?sort=top&t=month',
      },
      hidden: false,
    },
    {
      level: 1,
      id: 2,
      type: 'empty',
      title: '',
      content: {
        note: '',
        ttl: 6000000,
        quantity: 0,
        url: '',
      },
      hidden: true,
    },
    {
      level: 1,
      id: 3,
      type: 'empty',
      title: '',
      content: {
        note: '',
        ttl: 6000000,
        quantity: 0,
        url: '',
      },
      hidden: true,
    },
    {
      level: 1,
      id: 4,
      type: 'empty',
      title: '',
      content: {
        note: '',
        quantity: 0,
        ttl: 6000000,
        url: '',
      },
      hidden: true,
    },
    {
      level: 1,
      id: 5,
      type: 'empty',
      title: '',
      content: {
        note: '',
        quantity: 0,
        ttl: 6000000,
        url: '',
      },
      hidden: true,
    },
    {
      level: 1,
      id: 6,
      type: 'empty',
      title: '',
      content: {
        note: '',
        ttl: 6000000,
        quantity: 0,
        url: '',
      },
      hidden: true,
    },
  ],
  [
    {
      level: 2,
      id: 1,
      type: 'PdfConverter',
      title: 'https://dashboard.mcfly.braindot.fr/privacy',
      content: {
        note: '',
        ttl: 6000000,
        quantity: 0,
        url: 'http://api.pdflayer.com/api/convert?access_key=11ceb08bc1f1c4e11db6acc51d01e190&document_url=https://dashboard.mcfly.braindot.fr/privacy',
      },
      hidden: false,
    },
    {
      level: 2,
      id: 2,
      type: 'empty',
      title: '',
      content: {
        note: '',
        ttl: 6000000,
        quantity: 0,
        url: '',
      },
      hidden: true,
    },
    {
      level: 2,
      id: 3,
      type: 'empty',
      title: '',
      content: {
        note: '',
        url: '',
        ttl: 6000000,
        quantity: 0,
      },
      hidden: true,
    },
    {
      level: 2,
      id: 4,
      type: 'empty',
      title: '',
      content: {
        quantity: 0,
        note: '',
        url: '',
        ttl: 6000000,
      },
      hidden: true,
    },
    {
      level: 2,
      id: 5,
      type: 'empty',
      title: '',
      content: {
        note: '',
        quantity: 0,
        ttl: 6000000,
        url: '',
      },
      hidden: true,
    },
    {
      level: 2,
      id: 6,
      type: 'empty',
      title: '',
      content: {
        note: '',
        quantity: 0,
        ttl: 6000000,
        url: '',
      },
      hidden: true,
    },
  ],
];
