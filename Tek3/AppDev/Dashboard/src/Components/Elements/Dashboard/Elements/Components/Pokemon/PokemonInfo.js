import React, { useEffect, useState } from 'react';
import PropTypes from 'prop-types';
import { Spin, Typography, Divider } from 'antd';
import axios from 'axios';

const { Text } = Typography;

const PokemonInfo = ({ content }) => {
  const [loaded, setLoaded] = useState(false);
  const [data, setData] = useState({});
  const [refetch, setRefetch] = useState(true);

  useEffect(() => {
    axios.get(`${content.url}`)
      .then((res) => {
        setData(res.data);
        setTimeout(() => {
          setRefetch(!refetch);
        }, content.ttl);
        setLoaded(!loaded);
      })
      .catch((e) => console.error(e));
  }, []);

  if (loaded) {
    return (
      <>
        <span>
          <Text>
Base_exp:
            {' '}
            {data.base_experience}
          </Text>
          <Divider type="vertical" />
          <Text>
Height:
            {' '}
            {data.height}
          </Text>
          <Divider type="vertical" />
          <Text>
Weight:
            {' '}
            {data.weight}
          </Text>
        </span>
        <br />
        <br />
        <span>
        Abilities:
          {' '}
          {data.abilities.map((item) => (
            <div key={item.ability.name}>
              <a href={item.ability.url}>
                {item.ability.name}
              </a>
            </div>
          ))}
        </span>
        <br />
        <span>
        Types:
          {' '}
          {data.types.map((item) => (
            <div key={item.type.name}>
              <a href={item.type.url}>
                {item.type.name}
              </a>
            </div>
          ))}
        </span>
        <br />
        <span>
        Stat:
          {' '}
          Base_level, effort
          {data.stats.map((item) => (
            <div key={item.stat.name}>
              <a href={item.stat.url}>
                {item.stat.name}
              </a>
              :
              {' '}
              {' '}
              {item.base_stat}
              ,
              {' '}
              {item.effort}
            </div>
          ))}
        </span>
      </>
    );
  }
  return <Spin />;
};

PokemonInfo.propTypes = {
  content: PropTypes.shape({
    url: PropTypes.string.isRequired,
    ttl: PropTypes.number.isRequired,
  }).isRequired,
};

export default PokemonInfo;
