import React, { useState } from 'react';
import {
  Card, Icon, Drawer, Button, Divider,
} from 'antd';
import PropTypes from 'prop-types';

const EmptyWidget = ({
  addFunc, id,
}) => {
  const [visible, setVisible] = useState(false);

  const toggle = () => {
    setVisible(!visible);
  };

  return (
    <Card
      align="center"
    >
      <Icon type="plus" onClick={toggle} style={{ color: 'green' }} />
      <Drawer
        title="Add Widget ?"
        placement="right"
        closable
        onClose={toggle}
        visible={visible}
        hidden={!visible}
        getContainer={false}
        style={{ position: 'absolute' }}
      >
        <Button
          onClick={() => {
            addFunc(id, 'note');
            toggle();
          }}
          type="primary"
          disabled={!visible}
        >
          Note Widget
        </Button>
        <Divider />
        <Button
          onClick={() => {
            addFunc(id, 'reddit');
            toggle();
          }}
          type="primary"
          disabled={!visible}
        >
          Reddit Widget
        </Button>
        <Divider />
        <Button
          onClick={() => {
            addFunc(id, 'weather');
            toggle();
          }}
          type="primary"
          disabled={!visible}
        >
          OpenWeather Widget
        </Button>
        <Divider />
        <Button
          onClick={() => {
            addFunc(id, 'PokemonInfo');
            toggle();
          }}
          type="primary"
          disabled={!visible}
        >
          PokeStat Widget
        </Button>
        <Divider />
        <Button
          onClick={() => {
            addFunc(id, 'AirQuality');
            toggle();
          }}
          type="primary"
          disabled={!visible}
        >
          AirQuality Widget
        </Button>
        <Divider />
        <Button
          onClick={() => {
            addFunc(id, 'PdfConverter');
            toggle();
          }}
          type="primary"
          disabled={!visible}
        >
          URL to PDF Widget
        </Button>
      </Drawer>
    </Card>
  );
};

EmptyWidget.propTypes = {
  addFunc: PropTypes.func.isRequired,
  id: PropTypes.number.isRequired,
};

export default EmptyWidget;
