import { Card, Typography } from 'antd';
import React from 'react';
import PropTypes from 'prop-types';

import NoteWidget from './Components/Utilities/NoteWidget';
import RedditWidget from './Components/Reddit/RedditWidget';
import OpenWeatherWidget from './Components/Weather/OpenWeatherWidget';
import PokemonInfo from './Components/Pokemon/PokemonInfo';
import AirQualityWidget from './Components/AirQuality/AirQualityWidget';
import PdfConverterWidget from './Components/PdfConverter/PdfConverter';

const { Paragraph } = Typography;

const StaticWidget = ({ title, content, type }) => (
  <Card
    title={(
      <Paragraph>
        {(type === 'reddit') && 'Sub: /r/'}
        {title}
      </Paragraph>
    )}
    align="left"
  >
    {type === 'note' && <NoteWidget content={content} />}
    {type === 'reddit' && <RedditWidget content={content} />}
    {type === 'weather' && <OpenWeatherWidget content={content} />}
    {type === 'PokemonInfo' && <PokemonInfo content={content} />}
    {type === 'AirQuality' && <AirQualityWidget content={content} />}
    {type === 'PdfConverter' && <PdfConverterWidget content={content} />}
  </Card>
);

StaticWidget.propTypes = {
  type: PropTypes.string.isRequired,
  title: PropTypes.string.isRequired,
  content: PropTypes.shape({
    note: PropTypes.string.isRequired,
    quantity: PropTypes.number.isRequired,
  }).isRequired,
};

export default StaticWidget;
