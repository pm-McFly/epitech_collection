import React, { useEffect, useState } from 'react';
import PropTypes from 'prop-types';

import { Spin, Comment } from 'antd';

import axios from 'axios';

const RedditWidget = ({ content }) => {
  const [loaded, setLoaded] = useState(false);
  const [data, setData] = useState('');
  const [refetch, setRefetch] = useState(true);

  useEffect(() => {
    axios.get(`${content.url}`)
      .then((res) => {
        setData(res.data.data.children);
        setTimeout(() => {
          setRefetch(!refetch);
        }, content.ttl);
        setLoaded(!loaded);
      })
      .catch((e) => console.error(e));
  }, []);

  if (loaded) {
    return (
      <>
        {data.splice(0, content.quantity || data.length)
          .map((item) => (
            <Comment
              key={item.data.id}
              author={item.data.author}
              content={<a href={item.data.url}>{item.data.title}</a>}
            />
          ))}
      </>
    );
  }
  return <Spin />;
};

RedditWidget.propTypes = {
  content: PropTypes.shape({
    url: PropTypes.string.isRequired,
    quantity: PropTypes.number.isRequired,
    ttl: PropTypes.number.isRequired,
  }).isRequired,
};

export default RedditWidget;
