import React from 'react';
import { Typography } from 'antd';
import PropTypes from 'prop-types';


const { Paragraph } = Typography;

const NoteWidget = ({ content }) => (
  <Paragraph>
    {content.note}
  </Paragraph>
);

NoteWidget.propTypes = {
  content: PropTypes.shape({
    note: PropTypes.string.isRequired,
  }).isRequired,
};

export default NoteWidget;
