import React, { useEffect, useState } from 'react';
import PropTypes from 'prop-types';

import {
  Spin, Comment, Divider, Avatar, Typography,
} from 'antd';

import axios from 'axios';

const { Text } = Typography;

const OpenWeatherWidget = ({ content }) => {
  const [loaded, setLoaded] = useState(false);
  const [data, setData] = useState('');
  const [refetch, setRefetch] = useState(true);

  useEffect(() => {
    axios.get(`${content.url}`)
      .then((res) => {
        setData(res.data);
        setTimeout(() => {
          setRefetch(!refetch);
        }, content.ttl);
        setLoaded(!loaded);
      })
      .catch((e) => console.error(e));
  }, []);

  if (loaded) {
    return (
      <>
        {data.weather.map((item) => (
          <Comment
            key={item.id}
            author={item.main}
            content={(
              <Typography>
                <Text strong>{item.description}</Text>
              </Typography>
            )}
            avatar={(
              <Avatar
                src={`https://openweathermap.org/img/wn/${item.icon}@2x.png`}
                alt={item.main}
              />
            )}
          />
        ))}
        <span>
          <Text>
act:
            {' '}
            {data.main.temp}
            °C
          </Text>
          <Divider type="vertical" />
          <Text>
max:
            {' '}
            {data.main.temp_max}
            °C
          </Text>
          <Divider type="vertical" />
          <Text>
min:
            {' '}
            {data.main.temp_min}
            °C
          </Text>
          <Divider type="vertical" />
          <Text>
humidity:
            {' '}
            {data.main.humidity}
            %
          </Text>
        </span>
      </>
    );
  }
  return <Spin />;
};

OpenWeatherWidget.propTypes = {
  content: PropTypes.shape({
    url: PropTypes.string.isRequired,
    ttl: PropTypes.number.isRequired,
  }).isRequired,
};

export default OpenWeatherWidget;
