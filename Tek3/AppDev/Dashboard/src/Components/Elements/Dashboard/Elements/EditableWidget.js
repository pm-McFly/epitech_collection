import React, { useState } from 'react';
import {
  Icon, Drawer, Button, Card, Typography,
} from 'antd';
import PropTypes from 'prop-types';

import NoteWidget from './Components/Utilities/NoteWidget';
import RedditWidget from './Components/Reddit/RedditWidget';
import OpenWeatherWidget from './Components/Weather/OpenWeatherWidget';
import PokemonInfo from './Components/Pokemon/PokemonInfo';
import AirQualityWidget from './Components/AirQuality/AirQualityWidget';
import PdfConverterWidget from './Components/PdfConverter/PdfConverter';

const { Paragraph } = Typography;

const EditableWidget = ({
  title, content, deleteFunc, editFunc, id, type, editElemContentNote, editElemContentQuantity,
  editElemContentTTL,
}) => {
  const [delVisible, setDelVisible] = useState(false);
  const [confVisible, setconfVisible] = useState(false);

  const toggleDelete = () => {
    setDelVisible(!delVisible);
  };

  const toggleConf = () => {
    setconfVisible(!confVisible);
  };


  return (
    <Card
      title={(
        <Paragraph
          editable={{ onChange: (text) => editFunc(id, text) }}
        >
          {(type === 'reddit') && 'Sub: /r/'}
          {title}
        </Paragraph>
      )}
      align="left"
      actions={[
        <Icon type="setting" key="setting" onClick={toggleConf} />,
        <Icon type="delete" key="delete" style={{ color: 'red' }} onClick={toggleDelete} />,
      ]}
    >
      {type === 'note' && <NoteWidget content={content} />}
      {type === 'reddit' && <RedditWidget content={content} />}
      {type === 'weather' && <OpenWeatherWidget content={content} />}
      {type === 'PokemonInfo' && <PokemonInfo content={content} />}
      {type === 'AirQuality' && <AirQualityWidget content={content} />}
      {type === 'PdfConverter' && <PdfConverterWidget content={content} />}
      <Drawer
        title="Delete Widget ?"
        placement="right"
        closable
        onClose={toggleDelete}
        visible={delVisible}
        hidden={!delVisible}
        getContainer={false}
        style={{ position: 'absolute' }}
      >
        <Button
          onClick={() => deleteFunc(id)}
          type="danger"
          disabled={!delVisible}
        >
          Delete
        </Button>
      </Drawer>
      <Drawer
        title="Edit Widget content"
        placement="right"
        closable
        onClose={toggleConf}
        visible={confVisible}
        hidden={!confVisible}
        getContainer={false}
        style={{ position: 'absolute' }}
      >
        <Paragraph
          editable={{ onChange: (text) => editElemContentNote(id, text) }}
        >
          {content.note}
        </Paragraph>
        <Paragraph
          editable={{ onChange: (n) => editElemContentQuantity(id, parseInt(n, 10)) }}
        >
          {content.quantity.toString()}
        </Paragraph>
        <Paragraph
          editable={{ onChange: (n) => editElemContentTTL(id, parseInt(n, 10)) }}
        >
          {content.ttl.toString()}
        </Paragraph>
      </Drawer>
    </Card>
  );
};

EditableWidget.propTypes = {
  title: PropTypes.string.isRequired,
  content: PropTypes.shape({
    note: PropTypes.string.isRequired,
    quantity: PropTypes.number.isRequired,
    ttl: PropTypes.number.isRequired,
  }).isRequired,
  deleteFunc: PropTypes.func.isRequired,
  editFunc: PropTypes.func.isRequired,
  id: PropTypes.number.isRequired,
  type: PropTypes.string.isRequired,
  editElemContentNote: PropTypes.func.isRequired,
  editElemContentQuantity: PropTypes.func.isRequired,
  editElemContentTTL: PropTypes.func.isRequired,
};

export default EditableWidget;
