import React from 'react';
import PropTypes from 'prop-types';
import { Typography } from 'antd';

const { Text } = Typography;

const PdfConverterWidget = ({ content }) => (
  <Typography>
    <Text>
      <a href={content.url} target="_blank">{content.url}</a>
    </Text>
  </Typography>
);

PdfConverterWidget.propTypes = {
  content: PropTypes.shape({
    url: PropTypes.string.isRequired,
  }).isRequired,
};

export default PdfConverterWidget;
