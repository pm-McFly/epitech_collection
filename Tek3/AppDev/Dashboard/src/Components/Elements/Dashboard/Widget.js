import React from 'react';
import PropTypes from 'prop-types';

import EmptyWidget from './Elements/EmptyWidget';
import EditableWidget from './Elements/EditableWidget';
import StaticWidget from './Elements/StaticWidget';

const Widget = ({
  type, title, content, deleteFunc, addFunc, editFunc, id,
  editionMode, editElemContentNote, editElemContentQuantity, editElemContentTTL,
}) => {
  if (type !== 'empty') {
    if (editionMode) {
      return (
        <EditableWidget
          content={content}
          title={title}
          id={id}
          deleteFunc={deleteFunc}
          editFunc={editFunc}
          type={type}
          editElemContentNote={editElemContentNote}
          editElemContentQuantity={editElemContentQuantity}
          editElemContentTTL={editElemContentTTL}
        />
      );
    }
    return (
      <StaticWidget content={content} title={title} type={type} />
    );
  }
  return (
    <EmptyWidget addFunc={addFunc} id={id} />
  );
};
Widget.propTypes = {
  type: PropTypes.string.isRequired,
  title: PropTypes.string.isRequired,
  content: PropTypes.shape({
    note: PropTypes.string.isRequired,
    quantity: PropTypes.number.isRequired,
    ttl: PropTypes.number.isRequired,
  }).isRequired,
  deleteFunc: PropTypes.func.isRequired,
  addFunc: PropTypes.func.isRequired,
  editFunc: PropTypes.func.isRequired,
  editionMode: PropTypes.bool.isRequired,
  id: PropTypes.number.isRequired,
  editElemContentNote: PropTypes.func.isRequired,
  editElemContentQuantity: PropTypes.func.isRequired,
  editElemContentTTL: PropTypes.func.isRequired,
};

export default Widget;
