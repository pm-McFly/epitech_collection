import React, { useState } from 'react';
import PropTypes from 'prop-types';

import {
  Divider, List,
} from 'antd';

import Widget from './Widget';
import EditionSwitcher from './Elements/EditionSwitcher';

const Section = ({ elements, saveToDatabase, example }) => {
  const [ar, setAr] = useState(elements);
  const [editionMode, setEditionMode] = useState(false);

  const delElem = (id) => {
    ar[id - 1].type = 'empty';
    ar[id - 1].content = {
      note: '',
      url: '',
      quantity: 0,
      ttl: 6000000,
    };
    ar[id - 1].title = '';
    ar[id - 1].hidden = true;
    setAr([...ar]);
  };

  const addElem = (id, type) => {
    ar[id - 1].type = type;
    ar[id - 1].content.note = 'empty';
    ar[id - 1].title = `${type} ${id}`;
    ar[id - 1].hidden = false;
    ar[id - 1].content.quantity = 1;
    if (type === 'reddit') {
      ar[id - 1].content.url = `https://www.reddit.com/r/${ar[id - 1].title}.json?sort=top&t=month`;
      ar[id - 1].content.quantity = 3;
    } else if (type === 'note') {
      ar[id - 1].content.url = '';
    } else if (type === 'weather') {
      ar[id - 1].content.url = `https://api.openweathermap.org/data/2.5/weather?q=${ar[id - 1].title}&units=metric&appid=27e07daa0e505f99430d85fcab92fbae`;
    } else if (type === 'PokemonInfo') {
      ar[id - 1].content.url = `https://pokeapi.co/api/v2/pokemon/${ar[id - 1].title}`;
    } else if (type === 'AirQuality') {
      ar[id - 1].content.url = `https://api.openaq.org/v1/measurements/?city=${ar[id - 1].title}`;
    } else if (ar[id - 1].type === 'PdfConverter') {
      ar[id - 1].content.url = `http://api.pdflayer.com/api/convert?access_key=11ceb08bc1f1c4e11db6acc51d01e190&document_url=${ar[id - 1].title}`;
    }
    setAr([...ar]);
  };

  const editElemTitle = (id, text) => {
    ar[id - 1].title = text;
    if (ar[id - 1].type === 'reddit') {
      ar[id - 1].content.url = `https://www.reddit.com/r/${ar[id - 1].title}.json?sort=top&t=month`;
    } else if (ar[id - 1].type === 'weather') {
      ar[id - 1].content.url = `https://api.openweathermap.org/data/2.5/weather?q=${ar[id - 1].title}&units=metric&appid=27e07daa0e505f99430d85fcab92fbae`;
    } else if (ar[id - 1].type === 'PokemonInfo') {
      ar[id - 1].content.url = `https://pokeapi.co/api/v2/pokemon/${ar[id - 1].title}`;
    } else if (ar[id - 1].type === 'AirQuality') {
      ar[id - 1].content.url = `https://api.openaq.org/v1/measurements/?city=${ar[id - 1].title}`;
    } else if (ar[id - 1].type === 'PdfConverter') {
      ar[id - 1].content.url = `http://api.pdflayer.com/api/convert?access_key=11ceb08bc1f1c4e11db6acc51d01e190&document_url=${ar[id - 1].title}`;
    }
    setAr([...ar]);
  };

  const editElemContentNote = (id, note) => {
    ar[id - 1].content.note = note;
    setAr([...ar]);
  };

  const editElemContentQuantity = (id, n) => {
    ar[id - 1].content.quantity = n;
    setAr([...ar]);
  };

  const editElemContentTTL = (id, ttl) => {
    ar[id - 1].content.ttl = ttl;
    setAr([...ar]);
  };

  const switchMode = () => {
    setEditionMode(!editionMode);
    if (editionMode && !example) {
      const finalGrid = (ar.filter((elem) => (editionMode) || (!editionMode && !elem.hidden)));
      saveToDatabase(finalGrid);
    }
  };

  const finalGrid = (ar.filter((elem) => (editionMode) || (!editionMode && !elem.hidden)));

  const gridStyle = {
    gutter: 16,
    xs: 1,
    sm: ((ar.filter((elem) => editionMode || (!editionMode && !elem.hidden)).length > 1)
      ? 1
      : ar.filter((elem) => editionMode || (!editionMode && !elem.hidden)).length),
    md: ((ar.filter((elem) => editionMode || (!editionMode && !elem.hidden)).length > 2)
      ? 2
      : ar.filter((elem) => editionMode || (!editionMode && !elem.hidden)).length),
    lg: ((ar.filter((elem) => editionMode || (!editionMode && !elem.hidden)).length > 2)
      ? 2
      : ar.filter((elem) => editionMode || (!editionMode && !elem.hidden)).length),
    xl: ((ar.filter((elem) => editionMode || (!editionMode && !elem.hidden)).length > 4)
      ? 4
      : ar.filter((elem) => editionMode || (!editionMode && !elem.hidden)).length),
    xxl: ((ar.filter((elem) => editionMode || (!editionMode && !elem.hidden)).length > 6)
      ? 6
      : ar.filter((elem) => editionMode || (!editionMode && !elem.hidden)).length),
  };

  return (
    <>
      <EditionSwitcher editionMode={editionMode} switchMode={switchMode} />
      <List
        grid={gridStyle}
        dataSource={finalGrid}
        rowKey="id"
        renderItem={(params) => (
          <List.Item>
            <Widget
              type={params.type}
              title={params.title}
              content={params.content}
              id={params.id}
              deleteFunc={delElem}
              addFunc={addElem}
              editFunc={editElemTitle}
              editionMode={editionMode}
              editElemContentNote={editElemContentNote}
              editElemContentQuantity={editElemContentQuantity}
              editElemContentTTL={editElemContentTTL}
            />
          </List.Item>
        )}
      />
      <Divider />
    </>
  );
};

Section.propTypes = {
  elements: PropTypes.arrayOf(
    PropTypes.shape({
      type: PropTypes.string.isRequired,
      title: PropTypes.string.isRequired,
      content: PropTypes.shape({
        note: PropTypes.string.isRequired,
        quantity: PropTypes.number.isRequired,
        url: PropTypes.string.isRequired,
        ttl: PropTypes.number.isRequired,
      }).isRequired,
    }).isRequired,
  ).isRequired,
  saveToDatabase: PropTypes.func,
  example: PropTypes.bool.isRequired,
};

export default Section;
