import React from 'react';

import { Layout } from 'antd';

const { Footer } = Layout;

const BasePageFooter = () => (
  <Footer style={{ textAlign: 'center' }}>EpiBoard ©2019 Created by Pierre MARTY and Aymeric ASTAING</Footer>
);

export default BasePageFooter;
