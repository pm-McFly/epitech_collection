import React from 'react';
import { useHistory } from 'react-router-dom';
import { Breadcrumb, Button } from 'antd';

import firebase from 'firebase';

const BasePageBreadcrumb = () => {
  const history = useHistory();

  const onClick = (location) => () => {
    history.push(location);
  };

  if (firebase.auth().currentUser) {
    return (
      <Breadcrumb style={{ margin: '16px 0' }}>
        <Breadcrumb.Item>User</Breadcrumb.Item>
        <Breadcrumb.Item>{firebase.auth().currentUser.displayName}</Breadcrumb.Item>
        <Breadcrumb.Item><Button onClick={onClick('/')}>Dashboard</Button></Breadcrumb.Item>
      </Breadcrumb>
    );
  }
  return (
    <Breadcrumb style={{ margin: '16px 0' }}>
      <Breadcrumb.Item>Not Logged In</Breadcrumb.Item>
    </Breadcrumb>
  );
};
export default BasePageBreadcrumb;
