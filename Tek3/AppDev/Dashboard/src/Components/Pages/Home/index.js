import React, { useState, useEffect } from 'react';

import { notification, Spin } from 'antd';

import firebase from 'firebase';

import Dashboard from '../../Elements/Dashboard/Dashboard';
import { database } from '../../../credentials';


export default () => {
  const [userId] = useState(firebase.auth().currentUser.uid);
  const [loaded, setLoaded] = useState(false);
  const [settings, setSettings] = useState(null);
  const tmpSetting = settings;

  const openNotification = () => {
    notification.success({
      message: 'Dashboard Configuration correctly saved to the database !',
      description: '',
    });
  };

  const saveToDatabase = (section) => {
    tmpSetting[section.level] = section;

    database.ref(`users/${userId}`)
      .update({
        dashboardSettings: tmpSetting,
      })
      .then(() => openNotification())
      .catch((e) => console.error('Create, Error: ', e));
  };

  useEffect(() => {
    database.ref(`users/${userId}`)
      .once('value')
      .then((r) => {
        setSettings(r.val().dashboardSettings);
        setLoaded(true);
      })
      .catch((e) => console.error('Home, Error:', e));
  }, []);

  if (!loaded) {
    return (
      <Spin size="large" />
    );
  }

  return (
    <Dashboard settings={settings} example={false} saveToDatabase={saveToDatabase} />
  );
};
