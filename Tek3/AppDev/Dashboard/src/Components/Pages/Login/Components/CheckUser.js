import { useHistory } from 'react-router-dom';

import firebase from 'firebase';
import { useEffect } from 'react';
import { database } from '../../../../credentials';

const CheckUser = () => {
  const history = useHistory();
  const userId = firebase.auth().currentUser.uid;

  useEffect(() => {
    database.ref(`users/${userId}`)
      .once('value')
      .then((r) => {
        const user = (r.val() && r.val().userId) || 'Anonymous';
        if (user !== 'Anonymous') {
          window.location.href = '/';
        } else {
          history.push('/createuser');
        }
      })
      .catch((e) => console.error('Check, Error: ', e));
  }, []);
  return null;
};

export default CheckUser;
