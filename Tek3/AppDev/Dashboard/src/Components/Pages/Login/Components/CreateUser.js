import { useHistory } from 'react-router-dom';

import firebase from 'firebase';
import { database } from '../../../../credentials';
import { emptyData } from '../../../Elements/Dashboard/Elements/EmptyDataSet';

const CreateUser = () => {
  const history = useHistory();
  const userId = firebase.auth().currentUser.uid;

  database.ref(`users/${userId}`)
    .set({
      userId,
      dashboardSettings: emptyData,
    })
    .then(() => history.push('/'))
    .catch((e) => console.error('Create, Error: ', e));
  return null;
};

export default CreateUser;
