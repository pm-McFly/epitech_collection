import React from 'react';
import { useHistory } from 'react-router-dom';

import { Typography, Divider, Icon } from 'antd';

const { Title, Paragraph, Text } = Typography;

export default () => {
  const history = useHistory();

  const onClick = (location) => () => {
    history.push(location);
  };

  return (
    <Typography>

      <div className="Summary">
        <Title>Welcome in EpiBoard</Title>
        <Paragraph>
          <Text strong>
            In this short page you&#39;ll find everything you need to know how to use the plateform
            and
            create your fisrt personnalized dashboard.
          </Text>
        </Paragraph>
      </div>

      <Divider />

      <div className="Authentication">
        <Title level={2}>Authentication</Title>
        <Paragraph>
          In order to use this app you have to authenticate yourself first.
          <br />
          To do so, use one of the methods available here:
          <Paragraph>
            <Text type="secondary">
              <ul>
                <li>
                  <Icon type="mail" theme="filled" />
                  {' '}
                  Email and password,
                </li>
                <li>
                  <Icon type="google" />
                  {' '}
                  Google,
                </li>
                <li>
                  <Icon type="facebook" theme="filled" />
                  {' '}
                  Facebook,
                </li>
                <li>
                  <Icon type="twitter" />
                  {' '}
                  Twitter,
                </li>
                <li>
                  <Icon type="github" />
                  {' '}
                  GitHub,
                </li>
              </ul>
            </Text>
          </Paragraph>
        </Paragraph>
      </div>

      <Divider />

      <div className="UI">
        <Title level={2}>The UI</Title>
        <Paragraph>
          <Text strong>
            This plateform want to be easier as possible.
            <br />
            So there is only 2 areas you have to know.
          </Text>
        </Paragraph>
        <Title level={3}>The sider</Title>
        <Paragraph>
          Let&#39;s start with the static one !
          <br />
          The sider is the vertical collapsible bar on the left.
          <br />
          The only reason this bar exists is to give you information about what widget you can add
          to your dashboard.
          <br />
          You can also find at the top the &#34;logout&#34; (
          <Icon type="logout" />
          ) button
        </Paragraph>
        <Title level={3}>The big white center space</Title>
        <Paragraph>
          The big central zone is where everything happen.
          <br />
          This is where your dashboard will be. You can find out an example in the example page.
        </Paragraph>
      </div>

      <Divider />

      <div className="Widgets">
        <Title level={2}>Widgets</Title>
        <Paragraph>
          Firstly, you can find in the sider the list of available widgets for this dashboard.
          To edit your dashboard enter in &#39;Edition Mode&#39; by clicking the slider at the top
          of each sections.
          <Title level={3}>Edition Mode</Title>
          When this mode is active you can edit, add or delete a widget.
          The red trash button will delete the current widget by earasing any sort of content in it.
          Be sure of your will before using it!
          The gear button let you access to the settings of the widget. Here you can change the note
          field, the number of results you want for Reddit or AirQuality widget or the ttl of the
          widget for Reddit, AirQuality, PokeStats or OpenWeather Widget.
          You can also change the title of the widget with the side effect linked to.
          <ul>
            <li>Reddit: Change the subReddit you want to get the info</li>
            <li>Pokemon: Change the pokemon you want to get the info</li>
            <li>AirQuality and OpenWeather: Change the city you want to get the info</li>
            <li>URL to PDF: Change the website you want to grab the content</li>
          </ul>
          When you're happy just click back the slider and all of your changes will be saved.
          You can edit and save each sections in the same time.
        </Paragraph>
      </div>

      <Divider />

      <div className="More">
        <Title level={2}>More</Title>
        <Paragraph>
          In the sider there is also some links that will be useful:
          {' '}
          <Text strong>
            Example page (
            <Icon type="project" onClick={onClick('/exmaple')} />
            ),
            Privacy Policy (
            <Icon type="security-scan" onClick={onClick('/privacy')} />
            ),
            {' '}
            and the Help page (
            <Icon type="question" onClick={onClick('/help')} />
            )
          </Text>
          .
        </Paragraph>
      </div>

    </Typography>
  );
};
