import React from 'react';

import { testData } from '../../Elements/Dashboard/Elements/SampleDataSet';
import Dashboard from '../../Elements/Dashboard/Dashboard';

export default () => (
  <Dashboard settings={testData} example saveToDatabase={null} />
);
