from sys import *


class Settings:
    def __init__(self):
        self.timeBank = 0
        self.timePerMove = 0
        self.playerNames = list()
        self.yourBot = ""
        self.candleInterval = 0
        self.candlesTotal = 0
        self.candlesGiven = 0
        self.initialStack = 0
        self.transactionFeePercent = 0
        self.candleFormat = dict()

    def set_time_bank(self, value):
        self.timeBank = int(value)

    def set_time_per_move(self, value):
        self.timePerMove = int(value)

    def set_player_names(self, value):
        names = value.split(',')

        for i in range(0, len(names)):
            self.playerNames.append(names[i])

    def set_your_bot(self, value):
        self.yourBot = value

    def set_candle_interval(self, value):
        self.candleInterval = int(value)

    def set_candles_total(self, value):
        self.candlesTotal = int(value)

    def set_candles_given(self, value):
        self.candlesGiven = int(value)

    def set_initial_stack(self, value):
        self.initialStack = int(value)

    def set_transaction_fee_percent(self, value):
        self.transactionFeePercent = float(value)

    def set_candle_format(self, value):
        fmt = value.split(',')

        for i in range(0, len(fmt)):
            self.candleFormat[fmt[i]] = i

    def show(self):
        print(self.timeBank,
              self.timePerMove,
              self.playerNames,
              self.yourBot,
              self.candleInterval,
              self.candlesTotal,
              self.candlesGiven,
              self.initialStack,
              self.transactionFeePercent,
              self.candleFormat,
              sep='\n', file=stderr)

    def get_time_bank(self):
        return self.timeBank

    def get_time_per_move(self):
        return self.timePerMove

    def get_player_names(self):
        return self.playerNames

    def get_your_bot(self):
        return self.yourBot

    def get_candle_interval(self):
        return self.candleInterval

    def get_candles_total(self):
        return self.candlesTotal

    def get_candles_given(self):
        return self.candlesGiven

    def get_initial_stack(self):
        return self.initialStack

    def get_transaction_fee_percent(self):
        return self.transactionFeePercent

    def get_candle_format(self):
        return self.candleFormat
