#!/usr//bin/env python3

from bot_tools import *
import bot_assets


def settings(op_code, value, bot):
    lexer = {
        "timebank": bot.get_rules().set_time_bank,
        "time_per_move": bot.get_rules().set_time_per_move,
        "player_names": bot.get_rules().set_player_names,
        "your_bot": bot.get_rules().set_your_bot,
        "candle_interval": bot.get_rules().set_candle_interval,
        "candles_total": bot.get_rules().set_candles_total,
        "candles_given": bot.get_rules().set_candles_given,
        "initial_stack": bot.get_rules().set_initial_stack,
        "transaction_fee_percent": bot.get_rules().set_transaction_fee_percent,
        "candle_format": bot.get_rules().set_candle_format,
    }
    lexer.get(op_code, lambda x: log_it("Invalid command op_code %s !" % op_code))(value)


def update(player, value, bot):
    tokens = value.split()
    op_code = tokens[0]
    data = tokens[1]

    if player == "game":
        if op_code == "next_candles":
            bot.update_candles(data.split(';'))
        elif op_code == "stacks":
            bot.update_stacks(data)
        else:
            log_it("Unknown item %s !" % op_code)

    return


def action(op_code, value, bot):
    if op_code != "order":
        log_it("Bad op_code %s !" % op_code)
        return

    bot_init_stack = bot.get_rules().get_initial_stack()
    bot_stacks = bot.get_stacks()
    bot_candles_dict = bot.get_candles()
    bot_last_candle = len(bot_candles_dict["USDT_ETH"]) - 1
    bot_close_price = bot_candles_dict["USDT_ETH"][bot_last_candle].get_close()

    usd_mini = bot_init_stack * bot.get_mini_percent()
    current_usd = bot_stacks['USDT'] - usd_mini
    current_eth = bot_stacks['ETH']

    if bot.get_switch() == 1:
        log_it("SWITCH POS: " + str(bot_close_price))
        if current_usd <= 0:
            api_send("pass")
        else:
            bot.set_buy_price(bot_close_price)
            api_send("buy USDT_ETH " + str(current_usd / bot_close_price))
    elif bot.get_switch() == -1:
        log_it("SWITCH NEG: " + str(bot_close_price))
        if current_eth > 0:
            if bot_close_price >= bot.get_buy_price():
                api_send("sell USDT_ETH " + str(current_eth))
            else:
                api_send("pass")
        else:
            api_send("pass")
    else:
        api_send("pass")


def parser(buff, bot):
    tokens = buff.split(' ', 2)
    lexer = {
        "settings": settings,
        "update": update,
        "action": action
    }
    lexer.get(tokens[0], lambda x, y, z: log_it("Invalid command %s !" % tokens[0]))(tokens[1], tokens[2], bot)


if __name__ == '__main__':
    cli_de = bot_assets.Game()

    line = input()
    while line != "end":

        if len(line) > 0:
            parser(line, cli_de)
        else:
            break

        line = input()