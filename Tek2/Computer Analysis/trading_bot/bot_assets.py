import bot_settings
import bot_candles
import bot_tools
import statistics


class Game:
    def __init__(self):
        self.rules = bot_settings.Settings()
        self.candles = {'BTC_ETH': [bot_candles.Candle()], 'USDT_ETH': [bot_candles.Candle()], 'USDT_BTC': [bot_candles.Candle()]}
        self.stacks = dict()
        self.close_price = list()
        self.close_mean = list()
        self.relative_evolution = 0
        self.period = 95
        self.last_relative_evolution = 0
        self.switch = 0
        self.mini_percent = 0.05
        self.buy_price = 0

    def get_rules(self):
        return self.rules

    def get_switch(self):
        return self.switch

    def get_mini_percent(self):
        return self.mini_percent

    def get_stacks(self):
        return self.stacks

    def set_buy_price(self, value):
        self.buy_price = value

    def get_buy_price(self):
        return self.buy_price

    def update_stacks(self, lst):
        currencies = lst.split(',')

        for i in range(0, len(currencies)):
            name, value = currencies[i].split(':')
            self.stacks[name] = float(value)

    def get_candles(self):
        return self.candles

    def update_candles(self, data):

        for i in range(0, len(data)):
            candle = bot_candles.Candle()
            s_data = data[i].split(',')
            fmt = self.rules.candleFormat
            pair = s_data[fmt['pair']]
            date = s_data[fmt['date']]
            high = s_data[fmt['high']]
            low = s_data[fmt['low']]
            s_open = s_data[fmt['open']]
            s_close = s_data[fmt['close']]
            volume = s_data[fmt['volume']]

            if len(self.candles[pair]) > self.period:
                del self.candles[pair][0]

            candle.set_date(date)
            candle.set_high(high)
            candle.set_low(low)
            candle.set_open(s_open)
            candle.set_close(s_close)
            candle.set_volume(volume)

            if pair == "USDT_ETH":
                self.close_price.append(float(s_close))
                self.close_mean.append(statistics.mean(self.close_price))
                if len(self.close_price) > self.period:
                    self.last_relative_evolution = self.relative_evolution
                    self.relative_evolution = self.relative_calculus(self.close_mean)
                    if self.relative_evolution > 0 >= self.last_relative_evolution:
                        self.switch = -1
                    elif self.relative_evolution < 0 <= self.last_relative_evolution:
                        self.switch = 1
                    else:
                        self.switch = 0

            self.candles[pair].append(candle)

    def show_candles(self):
        for i in self.candles:
            bot_tools.log_it(i + ':\n')
            for j in self.candles[i]:
                j.show()
                bot_tools.log_it("")

    def relative_calculus(self, lst):
        return ((lst[len(lst) - 1] - lst[len(lst) - (1 + self.period)]) / lst[len(lst) - (1 + self.period)]) * 100
