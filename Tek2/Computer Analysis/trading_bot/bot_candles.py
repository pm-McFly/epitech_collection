import bot_tools


class Candle:
    def __init__(self):
        self.date = 0
        self.high = 0.0
        self.low = 0.0
        self.open = 0.0
        self.close = 0.0
        self.volume = 0.0

    def get_date(self):
        return self.date

    def get_high(self):
        return self.high

    def get_low(self):
        return self.low

    def get_open(self):
        return self.open

    def get_close(self):
        return self.close

    def get_volume(self):
        return self.volume

    def set_date(self, value):
        self.date = int(value)

    def set_high(self, value):
        self.high = float(value)

    def set_low(self, value):
        self.low = float(value)

    def set_open(self, value):
        self.open = float(value)

    def set_close(self, value):
        self.close = float(value)

    def set_volume(self, value):
        self.volume = float(value)

    def show(self):
        bot_tools.log_it("date: " + str(self.date))
        bot_tools.log_it("high: " + str(self.high))
        bot_tools.log_it("low: " + str(self.low))
        bot_tools.log_it("open: " + str(self.open))
        bot_tools.log_it("close: " + str(self.close))
        bot_tools.log_it("volume: " + str(self.volume))
