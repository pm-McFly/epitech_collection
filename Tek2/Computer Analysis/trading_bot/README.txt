How to run the bot:
1. Install Python3 and make
2. Run make
3. Install or Launch AI_Bot_Workspace (https://github.com/jmerle/ai-bot-workspace)
4. Set the path of the bot in the settings of the workspace (File->Settings->Command (use absolute paths without spaces)) at:
	"/path/to/the/bot/folder/trade"
5. Click "run" in the workspace