module Main where

import Lib
import System.Exit
import System.Random
import System.IO
import Control.Monad
import System.Environment
import Text.Printf

data Color = Color
    { coId ::Int
      ,x :: Int
      ,y :: Int
      ,r :: Double
      ,g :: Double
      ,b :: Double }

data Cluster = Cluster
    { clId :: Int
        ,prevR :: Double
        ,prevG :: Double
        ,prevB :: Double
        ,clR :: Double
        ,clG :: Double
        ,clB :: Double }

initColor :: (Int, Int, Int, Double, Double, Double) -> Color
initColor (id, x1, y1, r1, g1, b1) = Color
        {
        coId = id
        , x = x1
        , y = y1
        , r = r1
        , g = g1
        , b = b1 }

initCluster :: (Int, Double, Double, Double) -> Cluster
initCluster (id, r, g, b) = Cluster
        {
        clId = id
        , prevR = 0
        , prevG = 0
        , prevB = 0
        , clR = r
        , clG = g
        , clB = b }


rgbUpdate :: Cluster -> (Double, Double, Double) -> Cluster
rgbUpdate x (r, g, b) = x  {clR = r, clG = g, clB = b, prevR = clR x, prevG = clG x, prevB = clB x}

rdmCluster :: Int -> [Double] -> [Double] -> [Double] -> Int -> Cluster
rdmCluster v w x y z = initCluster (v, w !! z, x !! z, y !! z)

isEnd :: Int -> Int -> Bool
isEnd y z = (y + 1) == z

createClusters ::  [Double] -> [Double] -> [Double] -> Int -> Int ->  Bool -> [Cluster]
createClusters u v w x y True = []
createClusters u v w x y stop = rdmCluster y u v w y : createClusters u v w x (y + 1) (isEnd y x)

grabClose :: Color -> [Cluster] -> Int
grabClose y z = clId (whoIsNear (r y, g y, b y) z (head z))

colorLoop :: [Color] -> [Cluster] -> [(Int, Int)]
colorLoop xs z = map (\ x -> (grabClose x z, coId x)) xs

addPosition :: (Double, Double, Double) -> (Int, Int) -> Int -> [Color] -> (Double, Double, Double)
addPosition (r, g, b) x y z = if fst x == y then
        addMean (r, g, b) (z !! snd x) else
            (r, g, b)

linkedPoints :: Double -> Int -> [(Int, Int)] -> Double
linkedPoints x y [] = x
linkedPoints x y (z:zs) = if fst z == y then
        linkedPoints (x + 1) y zs else
            linkedPoints x y zs

divide :: (Double, Double, Double) -> Int ->  [(Int, Int)] -> (Double, Double, Double)
divide (r, g, b) y z = (r / linkedPoints 0 y z, g / linkedPoints 0 y z, b / linkedPoints 0 y z)

cPosition :: [Cluster] -> [Color] -> Int -> [(Int, Int)] -> (Double, Double, Double) -> (Double, Double, Double)
cPosition w x y [] (r, g, b) = (r, g, b)
cPosition w x y (z:zs) (r, g, b) = cPosition w x y zs (addPosition (r, g, b) z y x)

cMean :: Cluster -> [Cluster] -> [Color] -> [(Int, Int)] -> Cluster
cMean  w x y z = rgbUpdate w (divide (cPosition x y (clId w) z (0, 0, 0)) (clId w) z)

updateMeans :: [Cluster] -> [Color] -> [(Int, Int)] -> [Cluster]
updateMeans [] y z = []
updateMeans (x:xs) y  z = cMean x xs y z : updateMeans xs y z


printColor :: Color -> IO ()
printColor a = printf "(%d,%d) (%.f, %.f, %.f)\n"  (x a) (y a) (r a) (g a) (b a)

printPts :: Int -> [Color] -> [(Int, Int)] -> IO ()
printPts x y [] = return ()
printPts x y (z:zs) = do
    Control.Monad.when (fst z == x) (printColor (y !! snd z))
    printPts x y zs

finalPrint :: [Cluster] -> [Color] -> [(Int, Int)] -> IO ()
finalPrint [] y z = return ()
finalPrint (x:xs) y z = do
    printf "--\n(%.2f, %.2f, %.2f)\n-\n" (clR x) (clG x) (clB x)
    printPts (clId x) y z
    finalPrint xs y z

nFind str ch = [ y | (x, y) <- zip str [0..], x == ch ]

cEdistance (r1, g1, b1) (r2, g2 , b2) = sqrt (((r1 - r2) ^ 2) + ((g1 - g2) ^ 2) + ((b1 - b2) ^ 2))

cEdistanceColor :: Color -> (Double, Double, Double) -> Double
cEdistanceColor item (r2, g2, b2) = sqrt (((r item - r2) ^ 2) + ((g item - g2) ^ 2) + ((b item - b2) ^ 2))

cEdistanceCluster :: Cluster -> (Double, Double, Double) -> Double
cEdistanceCluster item (r3, g3, b3) = sqrt (((clR item - r3) ^ 2) + ((clG item - g3) ^ 2) + ((clB item - b3) ^ 2))

cEdistanceClusterColor :: Cluster -> Color -> Double
cEdistanceClusterColor cluster color = sqrt (((clR cluster - r color) ^ 2) + ((clG cluster - g color) ^ 2) + ((clB cluster - b color) ^ 2))

rMean :: [Color] -> Int -> Int -> Int
rMean [] mean index = div mean index
rMean (x:xs) mean index = rMean xs (mean + (round (r x) :: Int)) (index + 1)

gMean :: [Color] -> Int -> Int -> Int
gMean [] mean index = div mean index
gMean (x:xs) mean index = gMean xs (mean + (round (g x) :: Int)) (index + 1)

bMean :: [Color] -> Int -> Int -> Int
bMean [] mean index = div mean index
bMean (x:xs) mean index = bMean xs (mean + (round (b x) :: Int)) (index + 1)

subStr :: Int -> Int -> String -> String
subStr start end text = take (end - start) (drop start text)

addMean :: (Double, Double, Double) -> Color -> (Double, Double, Double)
addMean (r_cl, g_cl, b_cl) color  = (r_cl + r color, g_cl + g color, b_cl + b color)

toDouble :: String -> Double
toDouble str = read str :: Double

toInt :: String -> Int
toInt str = read str :: Int

keepIt :: Cluster -> Double -> Bool
keepIt c z = cEdistance (clR c, clG c, clB c) (prevR c, prevG c, prevB c) <= z

finalCluster :: [Cluster] -> Double -> Bool
finalCluster ys z = foldr (\ y -> (&&) (keepIt y z)) True ys

str2Tab :: String -> Char -> [String]
str2Tab str delim = let (start, end) = break (== delim) str
    in start : if null end then [] else str2Tab (tail end) delim

isLower :: Double -> Color -> Color -> (Double, Double, Double) -> Color
isLower distance_to_test tmp_lowest actual_item (r, g, b) = if cEdistanceColor tmp_lowest (r, g, b) > distance_to_test then
    actual_item else tmp_lowest

miniCluster :: Double -> Cluster -> Cluster -> (Double, Double, Double) -> Cluster
miniCluster distance_to_test tmp_lowest actual_item (r, g, b) = if cEdistanceCluster tmp_lowest (r, g, b) > distance_to_test then
    actual_item else tmp_lowest

whoIsNear :: (Double, Double, Double) -> [Cluster] -> Cluster -> Cluster
whoIsNear (r1, g1, b1) [] tmp_lower = tmp_lower
whoIsNear (r, g, b) (x:xs) tmp_lower_dist = whoIsNear (r, g, b) xs (miniCluster (cEdistanceCluster x (r, g, b)) tmp_lower_dist x (r, g, b))

chkArgs x = (length x == 3) || ((length x == 1) && (head x == "-h"))

colorFromString :: String -> Int -> Color
colorFromString x z = do
    let commas = nFind x ','
    let left_par = nFind x '('
    let right_par = nFind x ')'
    initColor (z, toInt (subStr 1 (head commas) x), toInt (subStr (head commas + 1) (head right_par)x), toDouble (subStr ((left_par !! 1) + 1) (commas !! 1)x), toDouble (subStr ((commas !! 1) + 1) (commas !! 2)x), toDouble (subStr ((commas !! 2) + 1) (right_par !! 1)x))

parseImage :: [String] -> Int -> [Color]
parseImage [] z = []
parseImage (y:ys) z = colorFromString y z : parseImage ys (z + 1)

failUsage :: IO ()
failUsage = do
    printUsage
    exitWith (ExitFailure 84)

askHelp x = (length x == 1) && head x == "-h"

printUsage :: IO()
printUsage = do
    x <- readFile "help"
    putStr x

exitUsage :: IO()
exitUsage = do
    printUsage
    exitSuccess

loop :: [Color] -> [Cluster] -> Double -> IO ()
loop x y z = do
    let ref = colorLoop x y
    let new = updateMeans y x ref
    if finalCluster new z then
        finalPrint new x ref else
        loop x new z


spli :: Eq a => a -> [a] -> [[a]]
spli x y = func x y [[]]
    where
        func x [] z = reverse $ map reverse z
        func x (y:ys) (z:zs) = if y == x then
            func x ys ([]:(z:zs))
        else
            func x ys ((y:z):zs)

main :: IO ()
main = do
    args <- getArgs
    unless (chkArgs args) failUsage
    when (askHelp args) exitUsage
    s <- readFile (args !! 2)
    let pixel_list = spli '\n' s
    let final = if null (pixel_list !! (length pixel_list - 1)) then init pixel_list else pixel_list
    let color_list = parseImage final 0
    let mean_r = fromIntegral (rMean color_list 0 0) :: Double
    let mean_g = fromIntegral (gMean color_list 0 0) :: Double
    let mean_b = fromIntegral (bMean color_list 0 0) :: Double
    let max_r = if (mean_r + 10) > 255 then 255 else mean_r + 10
    let min_r = if (mean_r - 10) < 0 then 0 else mean_r - 10
    let max_g = if (mean_g + 10) > 255 then 255 else mean_g + 10
    let min_g = if (mean_g - 10) < 0 then 0 else mean_g - 10
    let max_b = if (mean_b + 10) > 255 then 255 else mean_b + 10
    let min_b = if (mean_b - 10) < 0 then 0 else mean_b - 10
    rndmClR <- replicateM (toInt (head args)) (randomRIO (min_r,max_r::Double))
    rndmClG <- replicateM (toInt (head args)) (randomRIO (min_g,max_g::Double))
    rndmClB <- replicateM (toInt (head args)) (randomRIO (min_b,max_b::Double))
    let clusters = createClusters rndmClR rndmClG rndmClB (toInt(head args)) 0 False
    loop color_list clusters (toDouble (args !! 1))
