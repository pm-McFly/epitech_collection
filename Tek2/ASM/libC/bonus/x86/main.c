#include <stdio.h>
#include <stdlib.h>
#include <time.h>

size_t	strlen(char const *);
char *	strchr(const char *, int);

int	main(int argc __attribute__((unused)), char *argv[])
{
	time_t t;
	srand((unsigned) time(&t));

	int c = argv[2][rand() % strlen(argv[2])];

	printf("str: %s, size = %d\n", argv[1], strlen(argv[1]));
	printf("result: %s, character = %c\n", strchr(argv[1], c), c);
	return (0);
}
