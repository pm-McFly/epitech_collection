#include <stdio.h>
#include <stdlib.h>
#include <time.h>
#include <string.h>

int	main(int argc __attribute__((unused)), char *argv[])
{
	time_t t;
	srand((unsigned) time(&t));

	int c = argv[2][rand() % strlen(argv[2])];
	char *s1 = "ASASAP";
	char *s2 = "Hola Que Tal ?H";
	char *s3 = "";
	//char *null = NULL;
	char *s4 = "ASAP";
	char *s5 = "Hello";
	char s6[] = "abcdefghijklmn";
	char *s7 = s6;
	int n = 3;

	printf("strlen: %s, size = %ld\n", argv[1], strlen(argv[1]));
	printf("strchr: %s, character = %c\n", strchr(argv[1], c), c);
	printf("s1: %s, s2: %s, strcmp = %d\n", s1, s2, strcmp(s1, s2));
	printf("s1: %s, s2: %s, strncmp = %d, n = %d\n", s1, s2, strncmp(s1, s2, n), n);
	printf("s1: %s, s: %s, strcmp = %d\n", s1, strchr(argv[2], c), strcmp(s1, strchr(argv[2], c)));
	printf("s1: %s, s: %s, strncmp = %d, n = %d\n", s1, strchr(argv[2], n), strncmp(s1, strchr(argv[2], c), n), n);
	printf("strstr: %s, s1: %s, s3: %s\n", strstr(s1, s3), s1, s3);
	//printf("strstr: %s, s1: %s, null: %s\n", strstr(s1, null), s1, null);
	printf("strstr: %s, s1: %s, s4: %s\n", strstr(s1, s4), s1, s4);
	printf("strstr: %s, s1: %s, s5: %s\n", strstr(s1, s5), s1, s5);
	printf("strstr: %s, s3: %s, s5: %s\n", strstr(s3, s5), s3, s5);
	printf("strstr: %s, s3: %s, s3: %s\n", strstr(s3, s3), s3, s3);
	printf("s6: %s, memset: %s\n", s7, (char *)memset(s6, 'Q', 6));

	const char src[50] = "http://www.tutorialspoint.com";
	char dest[50];
	strcpy(dest,"Heloooo!!");
	printf("Before memcpy dest = %s\n", dest);
	memcpy(dest, src, strlen(src)+1);
	printf("After memcpy dest = %s\n", dest);

	char dest1[] = "oldstring";
	const char src1[]  = "newstring";
	printf("Before memmove dest1 = %s, src1 = %s\n", dest1, src1);
	memmove(dest1, src1, 9);
	printf("After memmove dest1 = %s, src1 = %s\n", dest1, src1);

	const char str[] = "http://www.tutorialspoint.com";
	const char ch = '.';
	char *ret;
	ret = strrchr(str, ch);
	printf("String after |%c| is - |%s|\n", ch, ret);

	const char str1[] = "";
	const char str2[] = "QQEW";
	const char str3[] = "JKAL";
	const char str4[] = "abcd eF";
	const char st[] = "ABCDEF";
	printf("First matched character is at %ld\n", strcspn(str1, st));
	printf("First matched character is at %ld\n", strcspn(str2, st));
	printf("First matched character is at %ld\n", strcspn(str3, st));
	printf("First matched character is at %ld\n", strcspn(str4, st));
	printf("First matched character is at %ld\n", strcspn(str1, str1));
	printf("First matched character is at %ld\n", strcspn(str2, str1));
	printf("First matched character is at %ld\n", strcspn(str3, str1));
	printf("First matched character is at %ld\n", strcspn(str4, str1));

	int result;
	result = strcasecmp(str2, str4);
	if (result == 0)
		printf("Strings compared equal.\n");
	else if (result < 0)
		printf("\"%s\" is less than \"%s\".\n", str2, str4);
	else
		printf("\"%s\" is greater than \"%s\".\n", str2, str4);

	const char stri1[] = "abcde2fghi3jk4l";
   const char stri2[] = "34";
   char *reti;

   reti = strpbrk(stri1, stri2);
   if(reti) {
      printf("First matching character: %c\n", *reti);
   } else {
      printf("Character not found");
   }

	return (0);
}
