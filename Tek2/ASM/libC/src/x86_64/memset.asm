BITS	64

GLOBAL memset			;	void	*memset(void *, int, size_t)

SECTION .TEXT

memset:
	PUSH	RBP		; -|
	MOV	RBP, RSP	; -|->	ASM PROLOGUE

	XOR	RCX, RCX	;	int i = 0
	MOV	RAX, RDI
loop:
	CMP	RDX, RCX	; -|
	JE	end		;  |->	while (i < n) {
	MOV	[RAX], SIL	;  |		ptr[i++] = c
	INC	RCX		;  |	}
	INC	RAX		;  |
	JNE	loop		; -|

end:
	MOV	RAX, RDI

	LEAVE			;	ASM EPILOGUE
	RET			;	return (ptr)
