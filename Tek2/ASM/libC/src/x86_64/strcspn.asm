BITS	64

GLOBAL	strcspn					;	size_t strcspn(const char *, const char *)

strcspn:
		PUSH	RBP			; -|
		MOV	RBP, RSP		; -|->	ASM PROLOGUE

		XOR	RAX, RAX		;	int len = 0
		CMP	BYTE	[RDI], 0x0
		JE	end
		MOV	R8, RSI

loop:
		MOV	R10B, [R8]
		MOV	R11B, [RDI]
inside:
		CMP	R10B, 0x0
		JE	next
		CMP	R10B, R11B
		JE	end
		INC	R8
		JNE	loop
next:
		MOV	R8, RSI
		CMP	R11B, 0x0
		JE	end
		INC	RDI
		INC	RAX
		JNE	loop
end:
		LEAVE				;	ASM EPILOGUE
		RET				;	return (len)
