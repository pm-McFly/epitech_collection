BITS	64

GLOBAL	strlen					;	size_t strlen(char const *s)

SECTION	.TEXT

strlen:
	PUSH	RBP				; -|
	MOV	RBP, RSP			; -|->	ASM PROLOGUE

	XOR	RAX, RAX			;	int i = 0

loop:
	CMP	BYTE	[RDI, RAX], 0x0		;	while (s + i != 0)
	JZ	end
	INC	RAX				;	++i
	JNZ	loop

end:
	LEAVE					;	ASM EPILOGUE
	RET					;	return (i)
