BITS	64

GLOBAL	strstr				;	char	*strstr(const char *, const char *)

SECTION	.TEXT

strstr:
		PUSH	RBP			; -|
		MOV	RBP, RSP		; -|->	ASM PROLOGUE

		CMP	BYTE	[RSI], 0x0	; -|->	if (*s2 == 0)
		JE	end			;  |		return (s1)
		CMP	BYTE	[RDI], 0x0	;  |	if (*s1 == 0)
		JE	fail			; -|		return (NULL)

loop:
		MOV	AL, [RDI]		; -|
		CMP	AL, [RSI]		;  |->	while (*s1 != *s2 && *s1 != 0) {
		JE	precheck		;  |		if (*s1 == *s2)
inside:						;  |			check(s1, s2)
		CMP	AL, 0x0 		;  |		++s1
		JE	fail			;  |	}
		INC	RDI			;  |	return (NULL)
		JNE	loop			; -|

precheck:
		MOV	R8, RDI 		; -|
		MOV	R9, RSI 		;  |->	while (*s1++ == *s2++ && *s2 != 0)
check:						;  |		;
		INC	R8			;  |	if (*s2 == 0)
		INC	R9			;  |		return (s1)
		MOV	R10B, [R8]		;  |	else
		MOV	R11B, [R9]		;  |		loop()
		CMP	R11B, 0x0		;  |
		JE	end			;  |
		CMP	R10B, R11B		;  |
		JE	check			;  |
		JNE	inside			; -|
fail:
		MOV	RAX, 0x0
		LEAVE				;	ASM EPILOGUE
		RET				;	return (null)

end:
		MOV	RAX, RDI
		LEAVE				;	ASM EPILOGUE
		RET				;	return (s1)
