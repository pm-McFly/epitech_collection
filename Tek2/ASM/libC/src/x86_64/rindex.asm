BITS	64

GLOBAL	rindex					; char *rindex(char const *, int)
GLOBAL	strrchr					; char *strrchr(char const *, int)

SECTION	.TEXT

rindex:
strrchr:
		PUSH	RBP
		MOV	RBP, RSP

		XOR	RCX, RCX

move:
		CMP	BYTE	[RDI + RCX], 0
		JZ	loop
		INC	RCX
		JNZ	move

loop:
		CMP	[RDI + RCX], SIL
		JZ	found
		CMP	RCX, 0
		JZ	fail
		DEC	RCX
		JNZ	loop

fail:
		MOV	RAX, 0
		JMP	end

found:
		MOV	RAX, RDI
		ADD	RAX, RCX

end:
		LEAVE
		RET
