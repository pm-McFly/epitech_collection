BITS	64

GLOBAL	strspn

SECTION	.TEXT

strspn:
		PUSH	RBP
		MOV	RBP, RSP

		XOR	RAX, RAX
		MOV	RDX, RDI
		JMP	loop

continue:
		INC	RAX
		INC	RDX
loop:
		CMP	BYTE	[RDX], 0
		JE	end
		MOV	R8, RSI
loop2:
		CMP	BYTE	[R8], 0
		JE	end
		MOV	AL, [RDX]
		CMP	[R8], AL
		JE	continue
		INC	R8
		JNE	loop2

end:
		LEAVE
		RET
