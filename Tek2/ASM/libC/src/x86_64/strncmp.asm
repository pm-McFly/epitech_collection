BITS	64

GLOBAL	strncmp					;	int strncmp(const char *s1, const char *s2, size_t n)

SECTION	.TEXT

strncmp:
	PUSH	RBP
	MOV	RBP, RSP

	XOR	RCX, RCX
loop:
	MOV	AL, [RDI]
	MOV	R8B, [RSI]
	CMP	AL, 0
	JE	end
	CMP	R8B, 0
	JE	end
	CMP	AL, R8B
	JNE	end
	DEC	RDX
	CMP	RDX, 0X0
	JE	end
	JMP	loop

end:
	SUB	AL, R8B
	MOVSX	RAX, AL

	LEAVE
	RET
