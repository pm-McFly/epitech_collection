BITS	64

GLOBAL	strcasecmp

SECTION	.TEXT

strcasecmp:
		PUSH	RBP
		MOV	RBP, RSP

		JMP	loop

t_lower1:
		ADD	R8B, 32
		JMP	loop2

t_lower2:
		ADD	R9B, 32
compare:
		CMP	R8B, R9B
		JNE	end
		CMP	R8B, 0
		JE	end
		CMP	R9B, 0
		JE	end
		INC	RSI
		INC	RDI
loop:
		MOV	R8B, [RDI]
		CMP	R8B, 65
		JL	loop2
		CMP	R8B, 90
		JL	t_lower1

loop2:
		MOV R9B, [RSI]
		CMP R9B, 65
		JL compare
		CMP R9B, 90
		JL t_lower2

end:
		SUB R8B, R9B
		MOVSX RAX, R8B
		LEAVE
		RET
