BITS	64

GLOBAL	strpbrk

SECTION	.TEXT

strpbrk:
		PUSH	RBP
		MOV	RBP, RSP

		JMP	loop
continue:
		INC	RDI
loop:
		CMP	BYTE	[RDI], 0X0
		JE	fail
		MOV	R8, RSI
loop2:
		CMP	BYTE	[R8], 0X0
		JE	continue
		MOV	AL, [RDI]
		CMP	[R8], AL
		JE	end
		INC	R8
		JNE	loop2

fail:
		MOV	RDI, 0x0
end:
		MOV	RAX, RDI
		LEAVE
		RET
