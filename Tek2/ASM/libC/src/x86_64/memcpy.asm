BITS	64

GLOBAL memcpy 

SECTION .text

memcpy:
	PUSH	RBP			; -|
	MOV	RBP, RSP		; -|->	ASM PROLOGUE
	
	XOR	RCX, RCX		;	int i = 0
loop:
	CMP	RCX, RDX		; -|
	JE	end			;  |->	while (i < n) {
	MOV	RAX, [RSI + RCX]	;  |		dest[i] = src[i];
	MOV	BYTE [RDI + RCX], AL	;  |		++i;		  //AL => [8 BITS] RAX -> [RAX]
	INC	RCX			;  |	}
	JNE	loop			; -|
end:
	MOV	RAX, RDI		;
	LEAVE				;	ASM EPILOGUE
	RET				;
