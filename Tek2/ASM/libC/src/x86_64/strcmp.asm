BITS	64

GLOBAL	strcmp					;	int strcmp(const char *s1, const char *s2)

SECTION	.TEXT

strcmp:
	PUSH	RBP				; -|
	MOV	RBP, RSP			; -|->	ASM PROLOGUE

loop:
	MOV	AL, [RDI]			; -|
	MOV	R8B, [RSI]			;  |
	CMP	AL, 0				;  |->	while (*s1++ && *s2++) {
	JE	end				;  |		if (*s1 != *s2)
	CMP	R8B, 0				;  |			=> end
	JE	end				;  |	}
	CMP	AL, R8B				;  |
	JNE	end				;  |
	INC	RDI				;  |
	INC	RSI				;  |
	JMP	loop				; -|

end:
	SUB	AL, R8B				; -|
	MOVSX	RAX, AL				;  |
						;  |->	return ((int) ((unsigned char)* s1 - (unsigned char)* s2))
	LEAVE					;  |
	RET					; -|
