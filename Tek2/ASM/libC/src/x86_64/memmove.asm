BITS	64

GLOBAL	memmove					;	void *memmove(void *, void const *, size_t)

SECTION	.TEXT

memmove:
		PUSH	RBP
		MOV	RBP, RSP

		MOV	RAX, RDI
		MOV	R8, RDI
		SUB	R8, RSI
		CMP	R8, 0
		JGE	pre_loop2

pre_loop:
		XOR	RCX, RCX
loop:
		CMP	RCX, RDX
		JE	end
		MOV	R8B, [RSI + RCX]
		MOV	[RDI + RCX], R8B
		INC	RCX
		JMP	loop

pre_loop2:
		DEC	RDX
loop2:
		CMP	RDX, 0
		JL	end
		MOV	R8B, [RSI + RDX]
		MOV	[RDI + RDX], R8B
		JMP	pre_loop2

end:
		LEAVE
		RET
