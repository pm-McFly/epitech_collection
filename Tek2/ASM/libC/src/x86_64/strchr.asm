BITS	64

GLOBAL	strchr					;	char *strchr(const char *s, int c)
GLOBAL	index					;	char *index(const char *, int c)

SECTION	.TEXT

index:
strchr:
	PUSH	RBP				; -|
	MOV	RBP, RSP			; -|->	ASM PROLOGUE

loop:
	CMP	BYTE	[RDI], SIL		; -|->	while (*s != c) {
	JZ	found				;  |
	CMP	BYTE	[RDI], 0x0		;  |		if (*s == NULL)
	JZ	end				;  |			return (NULL)
	INC	RDI				;  |		++s
	JNZ	loop				; -|	}

end:
	MOV	RAX, 0x0			; -|
	JMP	exit				; -|	s = NULL

found:	MOV	RAX, RDI			;	else return (*s)

exit:
	LEAVE					;	ASM EPILOGUE
	RET					;	return (s)
