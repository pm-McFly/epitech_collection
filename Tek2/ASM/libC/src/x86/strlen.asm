BITS	32

GLOBAL	strlen					;	size_t strlen(char const *s)

SECTION	.TEXT

strlen:
;	PUSH	EBP				; -|
;	MOV	EBP, ESP			; -|->	ASM PROLOGUE
;
;	XOR	EAX, EAX			;	int i = 0
;
;loop:
;	CMP	BYTE	[EBP, EAX], 0x0		;	while (s + i != 0)
;	JZ	end
;	INC	EAX				;	++i
;	JNZ	loop
;
;end:
;	LEAVE					;	ASM EPILOGUE
;	RET					;	return (i)
push ebx        ; sauvegarde de ebx
; Prologue:
        push ebp
        mov ebp, esp
        mov ebx, [ebp + 12] ; r�cup�ration du pointeur pass� en argument
        mov eax, 0         ; initialisation de eax � 0
        boucle:
          cmp byte [ebx + eax], 0
          je short finboucle
          inc eax
          jmp short boucle
        finboucle:
; Epilogue:
        mov esp, ebp
        pop ebp
        pop ebx
        ret 
